<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic',
        'vendor/bootstrap/css/bootstrap.min.css',
        'vendor/fontawesome/css/font-awesome.min.css',
        'vendor/themify-icons/themify-icons.min.css',
        'vendor/animate.css/animate.min.css',
        'vendor/perfect-scrollbar/perfect-scrollbar.min.css',
        'vendor/switchery/switchery.min.css',
        'css/styles.css',
        'css/plugins.css',
       // 'css/themes/theme-1.css',
        'vendor/kendoui/styles/kendo.common.min.css',
        'vendor/kendoui/styles/kendo.fiori.min.css',
        'vendor/kendoui/styles/kendo.fiori.mobile.min.css'
        //'css/site.css',

    ];
    public $js = [
        'vendor/jquery/jquery.min.js',
        'vendor/bootstrap/js/bootstrap.min.js',
        'vendor/modernizr/modernizr.js',
        'vendor/jquery-cookie/jquery.cookie.js',
        'vendor/perfect-scrollbar/perfect-scrollbar.min.js',
        'vendor/switchery/switchery.min.js',
        'js/main.js',
        'vendor/kendoui/js/kendo.all.min.js',
        'js/bundle.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
