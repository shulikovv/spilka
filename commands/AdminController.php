<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\User;

/**
 * Allows you to do samething admin functions.
 *
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminController extends Controller
{
    /**
     * Add new user.
     * @param string $username the username .
     * @param string $email the email.
     * @param string $password the password.
     */
    public function actionAddUser($username = 'user01', $email='vasja@email.com', $password='DumSp1r0Sper0')
    {
        $user = new User();
        $user->username = $username;
        $user->email = $email;
        $user->setPassword($password);
        $user->generateAuthKey();
        
        if ($user->save()) 
        {
            echo "User has created\n";
        }
        else
        {
            echo "User has not created\n";
        }
        
    }
}
