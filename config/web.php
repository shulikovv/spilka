<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            'dsn' => 'mongodb://localhost:27017/mydatabase',

        ],
        'fs' => [
            'class' => 'creocoder\flysystem\LocalFilesystem',
            //'path' => '@webroot/files',
            'path' => '/home/vlad/ftest',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'magellan',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            //'errorAction' => 'site/error',
            'errorAction' => 'site/index',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'trace'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'site/test'=> 'site/test',
                'site/test2'=> 'site/test2',
                'api/jwt' => 'site/jwt',
                'api/jwt2' => 'site/jwt2',
                'api/test' => 'site/test',
                'api/login' => 'site/login',
                'POST api/user' => 'user/add',
                'PUT api/user/<id:\d+>' => 'user/update',
                'DELETE api/user/<id:\d+>' => 'user/delete',
                'GET api/user/<id:\d+>' => 'user/view',
                'api/user/<action:\w+>' => 'user/<action>',
                'api/thesauri' => 'thesauri/list',
                'api/thesaurus/<id:\d+>' => 'thesauri/view',
                'POST api/organization' => 'organization/add',
                'api/organization/list' => 'organization/list',
                'api/organization/tree' => 'organization/tree',
                'GET api/organization/<id:\d+>' => 'organization/view',
                //'GET api/organization/children-list/<id:\d+>' => 'organization/view',
                'GET api/organization/<action:[\w\-]+>/<id:\d+>' => 'organization/<action>',    
                'GET api/organization/<action:[\w\-]+>' => 'organization/<action>',    
                'PUT api/organization/<id:\d+>' => 'organization/edit',
                'GET api/person/list' => 'person/list',   
                //'GET api/person/<action:[\w\-]+>/<id:\d+>' => 'person/<action>',    
                'DELETE api/person/biodetail/<id:\d+>' => 'person/biodetaildelete',   
                'DELETE api/person/address/<id:\d+>' => 'person/addressdelete',   
                'api/person/<action:[\w\-]+>/<id:\d+>' => 'person/<action>',    
                'GET api/person/<action:[\w\-]+>' => 'person/<action>', 
                'POST api/person' => 'person/add',   
                'PUT api/person/<id:\d+>' => 'person/edit',   
                'DELETE api/person/<id:\d+>' => 'person/delete',   
                'GET api/event/<action:[\w\-]+>/<id:\d+>' => 'event/<action>',    
                'GET api/event/<action:[\w\-]+>' => 'event/<action>', 
                'GET api/document/<action:[\w\-]+>'=>'document/<action>',
                'GET api/document/<action:[\w\-]+>/<id:\d+>'=>'document/<action>',
                'POST api/document/<action:[\w\-]+>'=>'document/<action>',
                '<controller:[\w-]+>' => 'site/index',
                '<controller:\w+>/<action:\w+>' => 'site/index',
                'catchAll' => 'site/index',
            ],
        ],
        'jwt' => [
            'class' => 'sizeg\jwt\Jwt',
            'key' => 'secret',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'mycomponent' => [
            'class' => 'app\components\MyComponent'
        ],
        'form' => [
            'class' => 'app\components\FormComponent'
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
