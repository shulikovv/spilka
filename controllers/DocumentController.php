<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use app\models\User;
use app\models\Organization;
use app\models\Thesauri;
use app\models\Thesaurus;
use app\models\Fields;
use app\models\Person;
use app\models\Document;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use yii\web\UploadedFile;

class DocumentController extends Controller
{
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                },
                'rules' => [
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
            'authenticator' => [
                'class' => CompositeAuth::className(),
                //'except' => ['add2'],
                'authMethods' => [
                    HttpBearerAuth::className(),
                ],
            ],

        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionFindDocuments()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $criteria =  Yii::$app->request->get();
        $where = ['AND'];
        if (array_key_exists('title',$criteria)) {
            $where[]=['like','title',$criteria['title']];
        }
        if (array_key_exists('datecreated',$criteria) && is_array($criteria['datecreated']) && count($criteria['datecreated'])==2) {
            $where[]=['between','datecreated',$criteria['datecreated'][0],$criteria['datecreated'][1]];
        }
        if (array_key_exists('type',$criteria)) {
            $where[]=['type'=>$criteria['type']];
        }
        if (array_key_exists('format',$criteria)) {
            $where[]=['like','format',$criteria['format']];
        }
        $documents = Document::find()
            ->where($where)
            ->all();
        return $documents;
    }

    public function actionStoreDocument()
    {
/*
baseName
:
"loader"
creator
:
"assa"
datecreated
:
"Wed Oct 04 2017 10:20:47 GMT+0300"
datesubmitted
:
"Thu Oct 05 2017 10:23:07 GMT+0300"
description
:
"cxzc"
ext
:
"less"
file_name
:
"loader.less"
real_name
:
"68/62/66029ED17284731221CBF503CF22.less"
title
:
"asada"
type
:
"01161605010116"
*/

        if (Yii::$app->request->isPost) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $file = UploadedFile::getInstanceByName('file');
            if (!$file) {
              return ['ststus'=>'error', 'message'=>'Вы не отправили файл'];
            }

            $s = strtoupper(md5(uniqid(rand(),true)));
            $document = new Document();
            $toUpdate = Yii::$app->request->post();
            $toUpdate['ext']=$file->extension;
            $toUpdate['file_name']=$file->name;
            $toUpdate['real_name']=substr($s,0,2) . '/' .substr($s,2,2). '/' .substr($s,4).'.'.$file->extension;
            //$params = Yii::$app->request->bodyParams;
            $document->load($toUpdate);
            $stream = fopen($file->tempName, 'r+');
            Yii::$app->fs->writeStream(substr($s,0,2) . '/' .substr($s,2,2). '/' .substr($s,4).'.'.$file->extension, $stream);

            if ($document->save()) {
                return ['status'=>'ok','success'=>true, 'errors'=>$document->getErrors()];
            } else {
                return ['status'=>'error','success'=>false, 'errors'=>$document->getErrors()];
            }
        }
    }
    public function actionStoreDocument2()
    {
        if (Yii::$app->request->isPost) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $file = UploadedFile::getInstanceByName('file2');
            if (!$file) {
                return 'nea';
            } else {
                $s = strtoupper(md5(uniqid(rand(),true)));
                $stream = fopen($file->tempName, 'r+');
                Yii::$app->fs->writeStream(substr($s,0,2) . '/' .substr($s,2,2). '/' .substr($s,4).'.'.$file->extension, $stream);
                return 'ok';
            }
            // getExtension()
            //saveAs()
            //$s = strtoupper(md5(uniqid(rand(),true)));
            //$file->saveAs('/home/vlad/ftest/'.substr($s,0,2) . '/' .substr($s,2,2). '/' .substr($s,4));
            //$stream = fopen($file->tempName, 'r+');
            //Yii::$app->fs->writeStream(substr($s,0,2) . '/' .substr($s,2,2). '/' .substr($s,4).'.'.$file->extension, $stream);
            //Yii::$app->fs->write(substr($s,0,2) . '/' .substr($s,2,2). '/' .substr($s,4), 'contents');
            return ['status'=>'ok','data'=>Yii::$app->request->post()];
        }
    }

    public function actionGetDocumentForm()
    {
      $out = \Yii::$app->form->getFormSchema('document');
      $data = [];
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      return [
          'schema' => $out,
          'data' => $data
      ];
    }
      
    public function actionGetDocumentsList($id=null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($id) {
            $person = Person::find()
                ->where(['person_id'=>$id])
                ->one();
            if ($person) {
                return $person->document;
            } else {
                return [];
            }
        } else {
            $documents = Document::find()->all();
            return $documents;
        }
    }

    public function actionGetFile($file='6B/3D/E0BAA717A3CAE1FC39680290F783.less') {
        if ($exists = Yii::$app->fs->has($file)) {
            if (ob_get_level()) {
                ob_end_clean();
              }
            $stream = Yii::$app->fs->readStream($file);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' .Yii::$app->fs->getSize($file));
            echo stream_get_contents($stream);
            fclose($stream);

        }
    }

    public function actionGetFileTmp($file='') {


        if (file_exists($file)) {
          // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
          // если этого не сделать файл будет читаться в память полностью!
          if (ob_get_level()) {
            ob_end_clean();
          }
          // заставляем браузер показать окно сохранения файла
          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename=' . basename($file));
          header('Content-Transfer-Encoding: binary');
          header('Expires: 0');
          header('Cache-Control: must-revalidate');
          header('Pragma: public');
          header('Content-Length: ' . filesize($file));
          // читаем файл и отправляем его пользователю
          readfile($file);
          exit;
        }
      }

}
