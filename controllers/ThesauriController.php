<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\User;
use app\models\Thesauri;
use app\models\Thesaurus;
use app\models\ContactForm;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use app\actions\test\TestAction;


class ThesauriController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                },
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'authenticator' => [
                'class' => CompositeAuth::className(),
                'except' => ['add2'],
                'authMethods' => [
                    HttpBearerAuth::className(),
                ],
            ],

        ];
    }

     public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Return users list.
     *
     * @return array
     */
     public function actionList()
     {
        $thesauri = Thesauri::find()->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $thesauri;
     }

     /**
     * Retur single user iformation
     *
     * @return array
     */
     public function actionView($id)
     {
        $thesaurus = Thesaurus::find()
            ->where(['mt_index_no'=>$id])
            ->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $thesaurus;
     }

     

}