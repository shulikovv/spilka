<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use app\models\User;
use app\models\Organization;
use app\models\Thesauri;
use app\models\Thesaurus;
use app\models\Fields;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use Lcobucci\JWT\Signer\Hmac\Sha256;


class OrganizationController extends Controller
{
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                },
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'authenticator' => [
                'class' => CompositeAuth::className(),
                //'except' => ['add2'],
                'authMethods' => [
                    HttpBearerAuth::className(),
                ],
            ],

        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionList()
    {
        $org = Organization::find()
            ->where(['organization_parent'=>0])
            ->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $org;
    }

    private function search($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, search($subarray, $key, $value));
            }
        }

        return $results;
    }

    private function getOrgsTree($list, $select = '*')
    {
        $out = array();
        foreach ($list as $item) {
            $children = $this->getChildren($item['organization_id'], $select);
            if (is_array($children) && count($children)>0) {
                $item['children']= $this->getOrgsTree($children, $select);
            }
            $out[]=$item;
        }
        return $out;
    }

    private function getChildren($id, $select = '*')
    {
        $children = Organization::find()
            ->select($select)
            ->where(['organization_parent'=>$id])
            ->asArray()
            ->all();
        return $children;
    }

    public function actionTree()
    {
        $org = Organization::find()
            ->where(['organization_parent'=>0])
            ->asArray()
            ->all();
        $out = $this->getOrgsTree($org);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;

    }

    public function actionShortTree()
    {
        $select = ['organization_id', 'organization_title'];
        $org = Organization::find()
            ->select($select)
            ->where(['organization_parent'=>0])
            ->asArray()
            ->all();
        $out = $this->getOrgsTree($org, $select);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;

    }


    public function actionChildren($id)
    {
        $org = Organization::find()
            ->where(['organization_parent'=>$id])
            ->asArray()
            ->all();
        $out = $this->getOrgsTree($org);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;
    }


    public function actionChildrenList($id)
    {
        $org = Organization::find()
            ->where(['organization_parent'=>$id])
            ->asArray()
            ->all();
        //$out = $this->getOrgsTree($org);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $org;
    }

    public function actionChildrenTree($id)
    {
        $org = Organization::find()
            ->where(['organization_parent'=>$id])
            ->asArray()
            ->all();
        $out = $this->getOrgsTree($org);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;
    }


    public function actionView($id)
    {
        $org = Organization::find()
            ->where(['organization_id'=>$id])
            ->asArray()
            ->one();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $org;
    }

    public function actionGetOrganization($id)
    {
        $info = Organization::find()
            ->where(['organization_id'=>$id])
            ->asArray()
            ->one();
        
        $org = Organization::find()
            ->where(['organization_parent'=>$id])
            ->asArray()
            ->all();
        $dep = $this->getOrgsTree($org);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'information' => $info,
            'departments' => $dep,
            'persnal' => [],
            'vechicles' => []
        ];
    }

    public function actionAdd()
    {

        $organization = new Organization();
        $organization->organization_title = Yii::$app->request->post('organization_title');
        $organization->organization_abb = Yii::$app->request->post('organization_abb');
        $organization->organization_is_sub = Yii::$app->request->post('organization_is_sub');
        $organization->organization_incl_sub = Yii::$app->request->post('organization_incl_sub');
        $organization->organization_recognition = Yii::$app->request->post('organization_recognition');
        $organization->organization_is_state = Yii::$app->request->post('organization_is_state');
        $organization->organization_description = Yii::$app->request->post('organization_description');
        $organization->organization_features = Yii::$app->request->post('organization_features');
        $organization->organization_hierarchy_level = Yii::$app->request->post('organization_hierarchy_level');
        $organization->organization_parent = (int) Yii::$app->request->post('organization_parent');
        $organization->organization_children = Yii::$app->request->post('organization_children');
        $organization->organization_has_children = 0;
        //$user = Yii::$app->user->identity;
        //$user->username
        //$organization->save();
        //$date = new DateTime();
        //echo $date->format('Y-m-d H:i:s');

         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!$organization->save()) {
        //if (!$organization->validate()) {
            $ret = [
                'status' => 'error',
                'errors' => $organization->getErrors(),
            ];
        } else {
            $ret = [
                'status' => 'ok',
            ];
        }
        return $ret;
        //return Yii::$app->request->post();        

    }

    public function actionEdit($id)
    {
        $organization = Organization::find()
            ->where(['organization_id'=>$id])
            ->one();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($organization) {
            $organization->organization_title = Yii::$app->request->post('organization_title');
            $organization->organization_abb = Yii::$app->request->post('organization_abb');
            $organization->organization_is_sub = Yii::$app->request->post('organization_is_sub');
            $organization->organization_incl_sub = Yii::$app->request->post('organization_incl_sub');
            $organization->organization_recognition = Yii::$app->request->post('organization_recognition');
            $organization->organization_is_state = Yii::$app->request->post('organization_is_state');
            $organization->organization_description = Yii::$app->request->post('organization_description');
            $organization->organization_features = Yii::$app->request->post('organization_features');
            $organization->organization_hierarchy_level = Yii::$app->request->post('organization_hierarchy_level');
            $organization->organization_parent = (int) Yii::$app->request->post('organization_parent');
            $organization->organization_children = Yii::$app->request->post('organization_children');
            $organization->organization_has_children = 0;
            if (!$organization->save()) {
                $ret = [
                    'status' => 'error',
                    'errors' => $organization->getErrors(),
                ];
            } else {
                $ret = [
                    'status' => 'ok',
                ];
            }
            return $ret;
        }
        return [
            'status' => 'error',
        ];  
    }

    public function actionForm($id) {
        $org = Organization::find()
            ->where(['organization_id'=>$id])
            ->asArray()
            ->one();


        $departments = $this->getOrgsTree($org);      
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $org;
    }

    function getMtAsOptions($mt) {
        $rows = Thesaurus::find()
            ->where(['mt_index_no'=>$mt])
            ->all();
        return $rows;
    }

    function getOrgsAsOptions() {
        $select = ['organization_id', 'organization_title'];
        $org = Organization::find()
            ->select($select)
            ->where(['organization_parent'=>0])
            ->asArray()
            ->all();
        $out = $this->getOrgsTree($org, $select);
        return $out;
    }

    function getFormSchema($entity) {
        $fields = \app\models\Fields::find()
        ->where(['entity'=>$entity])
        ->all();
        $schema = [
            'type'=> 'object',
            'properties' => []
        ];
        foreach ($fields as $field) {
            switch ($field->field_type) {
                case 'number':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'number'
                    ];
                break;
                case 'string':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'string'
                    ];
                break;
                case 'mt_tree':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'mt_tree',
                        'options' => $this->getMtAsOptions($field->list_code)
                    ];
                break;
                case 'org_tree':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'org_tree',
                        'options' => $this->getOrgsAsOptions()
                    ];
                break;
                case 'date':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'date'
                    ];
                break;
                case 'checkbox':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'checkbox'
                    ];
                break;
                case 'text':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'text'
                    ];
                break;
                default:
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'string'//$field->field_type
                    ];

            }
        }
        return $schema;
    }

    protected function getInfo($id) {
        $org = Organization::find()
            ->where(['organization_id'=>$id])
            ->asArray()
            ->one();
        $out = [];
        foreach ($org as $key=>$val) {
            if ($key == 'organization_is_state') {
                if ($val=='true') {
                    $out[$key]=true;
                } else {
                    $out[$key]=false;
                }
            } else {
                $out[$key]=$val;
            }
        }
        return $out;
    }

    public function actionFormSchema($id=null) {
        $formData = [];
        $departmentsData = null;
        if ($id) {
            $formData = $this->getInfo($id);
            $departmentsData = $this->actionChildrenTree($id);
        }
        $out = [
            'title' => '',
            'type' => 'tabs',
            'tid'=> $id,
            'tabs' => [
                [
                    'title'=> 'Information',
                    'id' => 'info',
                    'type' => 'form',
                    'form' => [
                        'schema' =>         $out = \Yii::$app->form->getFormSchema('organization'), //$this->getFormSchema('organization'),
                        'data' => $formData,
                    ]
                ],
                [
                    'title' => 'Departments',
                    'id'=>'departments',
                    'type' => 'table',
                    'form' => [
                        'key' => 'id',
                        'columns'=> [
                            [
                                'title' => 'Name',
                                'dataIndex' => 'organization_title',
                                'key' => 'organization_title'
                            ]   
                        ],
                        'data' => $departmentsData,
                    ]
                ]
            ]
        ];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;
    }

    public function actionAddFormSchema() {
        //$out = $this->getFormSchema('organization');
        $out = \Yii::$app->form->getFormSchema('organization');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;
    }

    public function actionEditFormSchema() {
        //$out = $this->getFormSchema('organization');
        $out = \Yii::$app->form->getFormSchema('organization');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;
    }


    public function actionGetUser() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return Yii::$app->user->identity;
    }

    public function actionEditFormData($id) {
        $org = Organization::find()
        ->where(['organization_id'=>$id])
        ->asArray()
        ->one();
        $out = [];
        foreach ($org as $key=>$val) {
            if ($key == 'organization_is_state') {
                if ($val=='true') {
                    $out[$key]=true;
                } else {
                    $out[$key]=false;
                }
            } else {
                $out[$key]=$val;
            }
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;
    }

}