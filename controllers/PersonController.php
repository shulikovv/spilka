<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use app\models\User;
use app\models\Organization;
use app\models\Thesauri;
use app\models\Thesaurus;
use app\models\Fields;
use app\models\Person;
use app\models\Address;
use app\models\BiographicDetails;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use Lcobucci\JWT\Signer\Hmac\Sha256;


class PersonController extends Controller
{
/**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                },
                'rules' => [
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
            'authenticator' => [
                'class' => CompositeAuth::className(),
                //'except' => ['add2'],
                'authMethods' => [
                    HttpBearerAuth::className(),
                ],
            ],

        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionGetPersonListForAct()
    {
        $out = [];
        $persons = Person::find()
        ->joinWith('counting')
        ->all();
        foreach ($persons as $person){
            $out[]= [
                'person_id' => $person->person_id,
                'counting_unit' => $person->counting->english,
                'person_name'=> $person->person_name,
                'date_of_birth'=> $person->date_of_birth,
                'sex' => $person->sexstring->english,
            ];
        };
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;            

    }

    public function actionGetPersonForm($id=null) {
        $out = \Yii::$app->form->getFormSchema('person');
        $data = [];
        if ($id) {
            $person = Person::find()
            ->where(['person_id'=> $id])
            ->asArray()
            ->one();
            if ($person) {
                $data = $person;
                if (is_string($data['org_id'])) {
                    //$data['org_id'] = (int) $data['org_id'];
                }
                if ($data['physical_description'][0]!='[') {
                    $data['physical_description'] = '["'.$data['physical_description'].'"]';
                } 
                if ($data['confidentiality']==1) {
                    $data['confidentiality']=true;
                } else {
                    $data['confidentiality']=false;
                }
                if ($data['deceased']==1) {
                    $data['deceased']=true;
                } else {
                    $data['deceased']=false;
                }
            }
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'schema' => $out,
            'data' => $data
        ];
    }

    public function actionBiodetaildelete($id=null) {
        $result =Yii::$app->db->createCommand()->update('biographic_details', ['is_deleted' => 1], ['biographic_details_id'=> (int) $id])->execute();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($result > 0) {
            $ret = [
                'status' => 'ok',
            ];
        } else {
            $ret = [
                'status' => 'error',
            ];
        }
        return $ret;
    }
    public function actionAddressdelete($id=null) {
        $result =Yii::$app->db->createCommand()->update('address', ['is_deleted' => 1], ['address_id'=> (int) $id])->execute();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($result > 0) {
            $ret = [
                'status' => 'ok',
            ];
        } else {
            $ret = [
                'status' => 'error',
            ];
        }
        return $ret;
    }

    public function actionGetPersonAform($id=null) {
        $out = \Yii::$app->form->getFormSchema('person_address');
        $data = [];
        /*
        if ($id) {
            $person = Person::find()
            ->where(['person_id'=> $id])
            ->asArray()
            ->one();
            if ($person) {
                $data = $person;
                if (is_string($data['org_id'])) {
                    //$data['org_id'] = (int) $data['org_id'];
                }
                if ($data['confidentiality']==1) {
                    $data['confidentiality']=true;
                } else {
                    $data['confidentiality']=false;
                }
                if ($data['deceased']==1) {
                    $data['deceased']=true;
                } else {
                    $data['deceased']=false;
                }
            }
        }
        */
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'schema' => $out,
            'data' => $data
        ];
    }

    public function actionGetPersonBioform($id=null) {
        $out = \Yii::$app->form->getFormSchema('person_biographic_details');
        $data = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'schema' => $out,
            'data' => $data
        ];
    }

    public function actionEdit($id=null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $person = Person::find()
        ->where(['person_id'=>$id])
        ->one();
        $toUpdate = Yii::$app->request->post();
        $mlt = [];
        foreach ($toUpdate as $key=>$val) {
            if ($key == 'physical_description') {
                $val_ = [];
                if (is_array($val)) {
                    $val_ = $val;
                } else {
                    $val_ = json_decode($val,true);
                }
                $mlt[] = array(
                    'table' => 'mlt_person_physical_description',
                    'field' => 'physical_description',
                    'values' => $val_
                );
            }
            if (($key == 'confidentiality') || ($key == 'deceased')) {
                if ($val == 'true') {
                    $person->$key = 1;   
                } else {
                    $person->$key = 1;
                }
            } else {
                $person->$key = $val;
            }
        }

        if (!$person->save()) {
        //if (!$organization->validate()) {
            $ret = [
                'status' => 'error',
                'errors' => $person->getErrors(),
            ];
        } else {
            foreach($mlt as $rows) {
                Yii::$app->db->createCommand()->delete($rows['table'], 'record_number = '.$person->person_id)->execute();
                foreach ($rows['values'] as $val) {
                    Yii::$app->db->createCommand()->insert($rows['table'], [
                        'vocab_number' => (string) $val,
                        'record_number' => $person->person_id,
                    ])->execute();
                }
            }
            $ret = [
                'status' => 'ok',
            ];
        }
        return $ret;
    }

    public function actionAdd()
    {
        /*

        {
        "counting_unit":"010000000000",
        "person_name":"Name",
        "other_names":"O Name",
        "org_id":"5",
        "confidentiality":"true",
        "date_of_birth":"    2017-08-16",
        "place_of_birth":"010125000000",
        "sex":"010000000000",
        "sexual_orientation":"010000000000",
        "identification_documents":"sfsdfsdfsdf",
        "civil_status":"020000000000",
        "depedants":"dfdfdfdf",
        "formal_education":"010000000000",
        "other_training":"fdfd",
        "helth":"dfdf",
        "medical_records":"dffdf",
        "physical_description":"010500000000",
        "deceased":"true",
        "date_deceased":"2017-08-17",
        "citizenship":"030119000000",
        "remarks":"dfdfdfdfd"
        }

        */
        $mlt = [];
        $data = Yii::$app->request->post();
        $form = \Yii::$app->form->getFormSchema('person');
        try {
            $post_physical_description = Yii::$app->request->post('physical_description');
            if (!is_array($post_physical_description)) {
                $physical_description = json_decode($post_physical_description,true);
            } else {
                $physical_description = $post_physical_description;
            }
            
            //Yii::log('physical_description',print_r(Yii::$app->request->post('physical_description')),$physical_description);
            if (is_array($physical_description)) {
                foreach($physical_description as $val) {
                    $mlt[] = [
                        'table' => 'mlt_person_physical_description',
                        'field' => 'physical_description',
                        'value' => $val
                    ];
                }
            }

//            print_r($mlt);
        } catch (Error $e) {
        }
        
        $person = new Person();
        $person->counting_unit = Yii::$app->request->post('counting_unit');
        $person->person_name = Yii::$app->request->post('person_name');
        $person->other_names = Yii::$app->request->post('other_names');
        $person->org_id = Yii::$app->request->post('org_id');
        $tpmConfidentiality = Yii::$app->request->post('confidentiality');
        $person->confidentiality = $tpmConfidentiality=='true'?1:0;
        $person->date_of_birth = Yii::$app->request->post('date_of_birth');
        $person->place_of_birth = Yii::$app->request->post('place_of_birth');
        $person->sex = Yii::$app->request->post('sex');
        $person->sexual_orientation = Yii::$app->request->post('sexual_orientation');
        $person->identification_documents = Yii::$app->request->post('identification_documents');
        $person->civil_status = Yii::$app->request->post('civil_status');
        $person->depedants = Yii::$app->request->post('depedants');
        $person->formal_education = Yii::$app->request->post('formal_education');
        $person->other_training = Yii::$app->request->post('other_training');
        $person->helth = Yii::$app->request->post('helth');
        $person->medical_records = Yii::$app->request->post('medical_records');
        $person->physical_description = Yii::$app->request->post('physical_description');
        $tmpDeceased = Yii::$app->request->post('deceased');
        $person->deceased = $tmpDeceased=='true'?1:0;
        $person->date_deceased = Yii::$app->request->post('date_deceased');
        $person->citizenship = Yii::$app->request->post('citizenship');
        $person->remarks = Yii::$app->request->post('remarks');

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!$person->save()) {
        //if (!$organization->validate()) {
            $ret = [
                'status' => 'error',
                'errors' => $person->getErrors(),
            ];
        } else {
            foreach ($mlt as $row) {
                //echo($row['value']);
                Yii::$app->db->createCommand()->insert($row['table'], [
                    'vocab_number' => (string) $row['value'],
                    'record_number' => $person->person_id,
                ])->execute();
            }
            $ret = [
                'status' => 'ok',
            ];
        }
        return $ret;
    }

    public function actionDelete($id = null)
    {
        $result =Yii::$app->db->createCommand()->update('person', ['is_deleted' => 1], ['person_id'=> (int) $id])->execute();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($result > 0) {
            $ret = [
                'status' => 'ok',
            ];
        } else {
            $ret = [
                'status' => 'error',
            ];
        }
        return $ret;
    }

    public function actionList()
    {
        $persons = Person::find()
        ->select(['person_id','counting_unit', 'person_name', 'date_of_birth', 'sex'])
        ->where(['is_deleted'=>0])
        ->all();
        $out = [];

        foreach ($persons as $person) {
            $countingUnit = Thesaurus::find()
            ->where(['mt_index_no'=>7, 'huri_code' => $person->counting_unit])
            ->one();
            $sex = Thesaurus::find()
            ->where(['mt_index_no'=>39, 'huri_code' => $person->sex])
            ->one();
            $out[] = [
                'person_id' => $person->person_id,
                'counting_unit' => @$countingUnit->english,
                'person_name' => $person->person_name,
                'date_of_birth' => $person->date_of_birth,
                'sex' => ($sex)?$sex->english:$person->sex,
            ];
        }


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;                    
    }

    public function actionAddPersonAddress($personId = null)
    {
        $params = Yii::$app->request->bodyParams;
        $address = new Address();
        $address->load($params);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($address->save()) {
            return ['success'=>true, 'address_id'=>$address->address_id, 'country'=> $address->countrySt->english];                    
        } else {
            return ['success'=>false, 'errors'=>$address->getErrors()];                    
        }
    }

    public function actionEditPersonAddress($id)
    {
        $params = Yii::$app->request->bodyParams;
        $address = Address::find()
        ->where(['address_id'=>$params['address_id']])->one();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!$address) {
            return ['success'=>false, 'errors'=>['some errors']];                    
        }
        $address->load($params);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($address->save()) {
            return ['success'=>true, 'address_id'=>$address->address_id, 'country'=> $address->countrySt['english']];                    
        } else {
            return ['success'=>false, 'errors'=>$address->getErrors()];                    
        }
    }


    public function actionAddPersonBiodetail($personId = null)
    {
        $params = Yii::$app->request->bodyParams;
        $biodetail = new BiographicDetails();
        $biodetail->load($params);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($biodetail->save()) {
            return ['success'=>true, 'biographic_details_id'=>$biodetail->biographic_details_id];                    
        } else {
            return ['success'=>false, 'errors'=>$biodetail->getErrors()];                    
        }
    }
    public function actionEditPersonBiodetail($id)
    {
        $params = Yii::$app->request->bodyParams;
        $biodetail = BiographicDetails::find()
        ->where(['biographic_details_id'=>$params['biographic_details_id']])->one();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!$biodetail) {
            return ['success'=>false, 'errors'=>['some errors']];                    
        }
        $biodetail->load($params);
        if ($biodetail->save()) {
            return ['success'=>true, 'biographic_details_id'=>$biodetail->biographic_details_id];                    
        } else {
            return ['success'=>false, 'errors'=>$biodetail->getErrors()];                    
        }
    }

    public function actionGetPersonBio($id)
    {
        $out = [];
        $biodetails = BiographicDetails::find()
        ->where(['person'=> $id, 'is_deleted'=>0])->all();
        foreach ($biodetails as $biodetail) {
            $out[] = [
                'biographic_details_id' => $biodetail->biographic_details_id,
                'biographic_details_record_number' => $biodetail->biographic_details_record_number,
                'title' => $biodetail->title,
                'person' => $biodetail->person,
                'related_person' => $biodetail->related_person,
                'confidentiality' => $biodetail->confidentiality,
                'type_of_relationship' => $biodetail->type_of_relationship,
                'initial_date' => $biodetail->initial_date,
                'initial_date_type' => $biodetail->initial_date_type,
                'final_date' => $biodetail->final_date,
                'final_date_type' => $biodetail->final_date_type,
                'education_and_training' => $biodetail->education_and_training,
                'employment' => $biodetail->employment,
                'affiliation' => $biodetail->affiliation,
                'position_in_organisation' => $biodetail->position_in_organisation,
                'rank' => $biodetail->rank,
                'remarks' => $biodetail->remarks,
            ];
         }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success'=>true, 'data'=>$out]; 
    }

    public function actionGetPersonAddresses($id)
    {
        $out = [];
        $addresses = Address::find()
        ->where(['person_id'=> $id, 'is_deleted'=>0])->all();
        foreach ($addresses as $address) {
            $out[] = [
                'address_id' => $address->address_id,
                'person_id' => $address->person_id,
                'address_type' => $address->address_type,
                'address_type_text' => $address->addressType['english'],
                'address' => $address->address,
                'country' => $address->country,
                'country_text' => $address->countrySt['english'],
                'phone' => $address->phone,
                'cellular' => $address->cellular,
                'fax' => $address->fax,
                'email' => $address->email,
                'web' => $address->web,
                'start_date' => $address->start_date,
                'end_date' => $address->end_date,
            ];
         }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success'=>true, 'data'=>$out]; 
    }

    public function actionGetPersonAddress($id)
    {
        $out = [];
        $addresses = Address::find()
        ->where(['address_id'=> $id,'is_deleted'=>0])->one();
        if ($addresses) {
            $out = $addresses;
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success'=>true, 'data'=>$out]; 
    }

    public function actionGetPersonBio_($id)
    {
        $out = [];
        $addresses = BiographicDetails::find()
            ->where(['person'=> $id])->all();
        foreach ($addresses as $address) {
            $out[] = [
                'address_id'     => $address->address_id,
                'person_id' => $address->person_id,
                'address_type' => $address->addressType->english,
                'address' => $address->address,
                'country' => $address->countrySt->english,
                'phone' => $address->phone,
                'cellular' => $address->cellular,
                'fax' => $address->fax,
                'email' => $address->email,
                'web' => $address->web,
                'start_date' => $address->start_date,
                'end_date' => $address->end_date,
            ];
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success'=>true, 'data'=>$out];
    }

    public function actionGetPersonListForForm() {
        $out = [];
        $request = Yii::$app->request->get();
        $personsQuery = Person::find();
        if (is_array($request)&& count($request)>0) {
            if (count($request)>1) {
                $query = ['and'];
                foreach ($request as $key=>$val) {
                    switch ($key) {
                        case 'counting_unit':
                        $query[]=[$key=>$val];
                        break;
                        case 'person_name':
                        $query[]=['like','person_name',$val];
                        break;
                        case 'date_of_birth':
                        $query[]=['like','date_of_birth',$val];
                        break;
                        case 'sex':
                        $query[]=[$key=>$val];
                        break;
                    }
                }
            } else {
                foreach ($request as $key=>$val) {
                    switch ($key) {
                        case 'counting_unit':
                        $query=[$key=>$val];
                        break;
                        case 'person_name':
                        $query=['like','person_name',$val];
                        break;
                        case 'date_of_birth':
                        $query=['like','date_of_birth',$val];
                        break;
                        case 'sex':
                        $query=[$key=>$val];
                        break;
                    }
                }
            }
            $personsQuery->where($query);
        }
        $persons = $personsQuery
        ->all();
        foreach ($persons as $person){
            $out[]= [
                'person_id' => $person->person_id,
                'counting_unit' => $person->counting['english'],
                'person_name'=> $person->person_name,
                'date_of_birth'=> $person->date_of_birth,
                'sex' => $person->sexstring['english'],
            ];
        };
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;   
    }



}