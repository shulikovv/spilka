<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\User;
use app\models\ContactForm;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use app\actions\test\TestAction;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                },
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'authenticator' => [
                'class' => CompositeAuth::className(),
                'except' => ['index','jwt', 'jwt2', 'login', 'test', 'test2'],
                'authMethods' => [
                    HttpBearerAuth::className(),
                ],
            ],

        ];
    }


    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        parent::beforeAction($action);

        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {
            // End it, otherwise a 401 will be shown.
            Yii::$app->end();
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'test2' => [
                'class' => TestAction::class,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //$s = strtoupper(md5(uniqid(rand(),true))); 
        //Yii::$app->fs->write(substr($s,0,2) . '/' .substr($s,2,2). '/' .substr($s,4), 'contents');
        
        $token = Yii::$app->jwt->getBuilder()->setIssuer('http://example.com') // Configures the issuer (iss claim)
        ->setAudience('http://example.org') // Configures the audience (aud claim)
        ->setId('4f1g23a12aa', true) // Configures the id (jti claim), replicating as a header item
        ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
        //->setNotBefore(time() + 60) // Configures the time before which the token cannot be accepted (nbf claim)
        ->setExpiration(time() + 3600) // Configures the expiration time of the token (exp claim)
        ->set('uid', 1) // Configures a new claim, called "uid"
        ->getToken(); // Retrieves the generated token


        $token->getHeaders(); // Retrieves the token headers
        $token->getClaims(); // Retrieves the token claims
/*
        echo $token->getHeader('jti'); // will print "4f1g23a12aa"
        echo "<br>";
        echo $token->getClaim('iss'); // will print "http://example.com"
        echo "<br>";
        echo $token->getClaim('uid'); // will print "1"
        echo "<br>";
        echo $token; // The string representation of the object is a JWT string (pretty easy, right?)
        echo "<br>-<br>";


        $data = Yii::$app->jwt->getValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer('http://example.com');
        $data->setAudience('http://example.org');
        $data->setId('4f1g23a12aa');

        var_dump($token->validate($data)); // true, because validation information is equals to data contained on the token
        echo "<br>-<br>";

        $data->setCurrentTime(time() + 4000); // changing the validation time to future

        var_dump($token->validate($data)); // false, because token is expired since current time is greater than exp
        echo "<br>-<br>";

        exit();*/
        Yii::$app->response->setStatusCode(200);
        return $this->render('index', ['token'=>$token]);
    }

    public function actionJwt()
    {
        $signer = new Sha256();
        $token = Yii::$app->jwt->getBuilder()
        ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
        //->setNotBefore(time() + 60) // Configures the time before which the token cannot be accepted (nbf claim)
        ->setExpiration(time() + 3600) // Configures the expiration time of the token (exp claim)
        ->set('uid', 2  ) // Configures a new claim, called "uid"
        ->sign($signer, 'testing') // creates a signature using "testing" as key
        ->getToken(); // Retrieves the generated token

        return $token;
    }

    public function actionJwt2()
    {
        $signer = new Sha256();
        $token = Yii::$app->jwt->getParser()->parse((string) $_POST['token']);

        return var_dump($token->verify($signer, 'testing'));
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {


/*        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);*/

        $username = Yii::$app->request->post('username','');
        $password = Yii::$app->request->post('password','');
        $user = User::findByUsername($username);
        if (!$user || !$user->validatePassword($password)) {
            Yii::$app->response->setStatusCode(401);
            return;
        }
        if (Yii::$app->user->login($user,0)) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return (string) $user->getJwtToken();
//            return [
//                'user' => $user,
//                'token' => (string) $user->getJwtToken(),
//            ];
        }
        Yii::$app->response->setStatusCode(401);
        return;
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionTest()
    {
        Yii::$app->mycomponent->mySuperMethod();
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
