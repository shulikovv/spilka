<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\User;
use app\models\ContactForm;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use app\actions\test\TestAction;


class UserController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                },
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'authenticator' => [
                'class' => CompositeAuth::className(),
                'except' => ['add2'],
                'authMethods' => [
                    HttpBearerAuth::className(),
                ],
            ],

        ];
    }

     public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Return users list.
     *
     * @return array
     */
     public function actionList()
     {
        $users = User::find()->select('id, username, email, status')->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $users;
     }

     /**
     * Retur single user iformation
     *
     * @return array
     */
     public function actionView($id)
     {
        $users = User::find()
            ->select('id, username, email, status')
            ->where(['id'=>$id])
            ->one();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $users;
     }

     /**
     * Retur single user iformation
     *
     * @return array
     */
     public function actionUpdate($id)
     {
        $request = Yii::$app->request;

        $username = $request->post('username');
        $email = $request->post('email');
        $password = $request->post('password');

        $user = User::find()
            ->where(['id'=>$id])
            ->one();

        if ($user) {

            if ($username) $user->username = $username;
            if ($email) $user->email = $email;
            if ($password) $user->setPassword($password);
            $user->generateAuthKey();
            if ($user->save()) 
            {
                Yii::$app->response->setStatusCode(200);
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $user;
            }
            else
            {
                Yii::$app->response->setStatusCode(400);
            }
        } else {
            Yii::$app->response->setStatusCode(400);
        }
     }

     /**
     * Retur single user iformation
     *
     * @return array
     */
     public function actionAdd()
     {
        $request = Yii::$app->request;

        $username = $request->post('username');
        $email = $request->post('email');
        $password = $request->post('password');

        $user = new User();
        $user->username = $username;
        $user->email = $email;
        $user->setPassword($password);
        $user->generateAuthKey();
        
        if ($user->save()) 
        {
            Yii::$app->response->setStatusCode(200);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $user;
        }
        else
        {
            Yii::$app->response->setStatusCode(400);
        }
     }

     /**
     * Retur single user iformation
     *
     * @return array
     */
    public function actionDelete($id)
    {
        $customer = User::findOne($id);
        $customer->delete();
    }     

}