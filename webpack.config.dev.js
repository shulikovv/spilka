var path = require('path')
var webpack = require('webpack')
var NpmInstallPlugin = require('npm-install-webpack-plugin')
//var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
//const BabiliPlugin = require('babili-webpack-plugin');


module.exports = {
    devtool: 'cheap-module-eval-source-map',
    //devtool: 'eval',
    entry: [
        //'webpack-hot-middleware/client',
        'babel-polyfill',
        './app/index'
    ],
    output: {
        path: path.join(__dirname, 'web/js'),
        filename: 'bundle.js',
        publicPath: '/web/js/'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
              NODE_ENV: JSON.stringify('development')
            }
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new NpmInstallPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        //new  UglifyJSPlugin()
        //new BabiliPlugin(),
    ],
    module: {
        rules: [
            {
                loaders: ['babel-loader'],
                include: [
                    path.resolve(__dirname, "app"),
                ],
                test: /\.js$/,
            },
            { test: /\.css$/,  loader: "style-loader!css-loader!postcss-loader" },
            {
                test: /\.less$/,
                loader: "style-loader!css-loader!less-loader"
            },
            { test: /\.gif$/, loader: "url-loader?mimetype=image/png" },
            { test: /\.woff(2)?(\?v=[0-9].[0-9].[0-9])?$/, loader: "url-loader?mimetype=application/font-woff" },
            { test: /\.(ttf|eot|svg)(\?v=[0-9].[0-9].[0-9])?$/, loader: "file-loader?name=[name].[ext]" },
        ]
    },
}
