var webpack = require('webpack')
var path = require('path');

var config = {
    entry: './app/index',
    module: {
        rules: [
            { test: /\.js$/, use: [ 'babel-loader' ], exclude: /node_modules/ }
        ]
    },
    output: {
        library: 'ReactRouterRedux',
        libraryTarget: 'umd',
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'web/js')
    }
}

if (process.env.NODE_ENV === 'production') {
    config.plugins = [
        new webpack.optimize.UglifyJsPlugin({ sourceMap: true }),
        new webpack.LoaderOptionsPlugin({ minimize: true })
    ]
}

module.exports = config