var path = require('path');
var webpack = require('webpack')
var NpmInstallPlugin = require('npm-install-webpack-plugin')


module.exports = {
    devtool: 'cheap-module-eval-source-map',
    entry: [
       // 'webpack-hot-middleware/client',
        './app/index.js'
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'web/js')
    },
    plugins: [
        //new webpack.optimize.OccurenceOrderPlugin(),
        //new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new NpmInstallPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                include: [
                    path.resolve(__dirname, 'app')
                ],
                //exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ['es2015', 'react', 'stage-0'],
                    plugins: ['add-module-exports']//, 'transform-runtime']
                }
            }
        ]
    }

};