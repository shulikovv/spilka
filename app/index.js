import React from 'react'
import { push } from 'react-router-redux';
//import { render } from 'react-dom'
import ReactDOM from 'react-dom';
import Root from './containers/Root';
//import './styles/app.css'
import configureStore from './store/configureStore'
import {loginUserSuccess, RedirectToLogin} from './actions';
const store = configureStore(window.__INITIAL_STATE__);

const node = (
    <Root store={store} />
);

let token = localStorage.getItem('token');
if (token !== null) {
    console.log('token present');
   store.dispatch(loginUserSuccess(token));
   //store.dispatch(push('/events'));
} else {
   console.log('token not present');
   store.dispatch(RedirectToLogin());
}

ReactDOM.render(
  node,
  document.getElementById('root')
)
