import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import * as _ from 'lodash';
import is from 'is_js';

export function createConstants(...constants) {
    return constants.reduce((acc, constant) => {
        acc[constant] = constant;
        return acc;
    }, {});
}

export function createReducer(initialState, reducerMap) {
    return (state = initialState, action) => {
        const reducer = reducerMap[action.type];

        return reducer
            ? reducer(state, action.payload)
            : state;
    };
}

export function checkHttpStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response
    } else {
        var error = new Error(response.statusText)
        error.response = response
        throw error
    }
}

export function parseJSON(response) {
     return response.json()


}

export function getThesaurusTree (items,level = 0,levelkey = '') {
    let rego = [
        '[0-9]{2}[0]{10}',
        '((?!00)[0-9]{2})[0]{8}',
        '((?!00)[0-9]{2})[0]{6}',
        '((?!00)[0-9]{2})[0]{4}',
        '((?!00)[0-9]{2})[0]{2}',
        '((?!00)[0-9]{2})'
    ];

    let regs = [
        '[0-9]{2}[0-9]{10}',
        '((?!00)[0-9]{2})[0-9]{8}',
        '((?!00)[0-9]{2})[0-9]{6}',
        '((?!00)[0-9]{2})[0-9]{4}',
        '((?!00)[0-9]{2})[0-9]{2}',
        '((?!00)[0-9]{2})'
    ];
    let out = [];
    let subitems = _.filter(items, (o)=>o.vocab_number.match(new RegExp('^'+levelkey+rego[level])));
    subitems.forEach((item)=>{
        let children = _.filter(items, (o)=>o.vocab_number.match(new RegExp('^'+item.vocab_number.substr(0,(level+1)*2)+regs[level])));
        let o = Object.assign({}, item, {keyt: item.vocab_number.substr(0,(level+1)*2)});//{ ...item,  key: item.vocab_number.substr(0,(level+1)*2)};
        if (children.length>0) {
            o.children = getThesaurusTree(children,level+1,o.keyt);
        };
        out.push(o);
    });
    return out;
}

export function getTesaurusList(items) {
   let out = [];
   if (is.array(items)) {
     items.forEach((item)=>{
        let o = Object.assign({}, item, {keyt: item.vocab_number.substr(0,2)});
        out = [...out,o];
     })
   }
  return out;
}

export function preparePropToForm(properties) {
    let tmpForm = {
        schema : {
            type: "object",
            properties: {}
        },
        uiSchema: {}
    }
    for (let key in properties) {
        switch (properties[key].type) {
            case 'enum':
                tmpForm.schema.properties[key] = {
                    "title": properties[key].title,
                    "type": "string",
                    "enum": properties[key].enum
                }
            break;
            case 'user':
                tmpForm.schema.properties[key] = {
                    "title": properties[key].title,
                    "type": "string",
                };
                tmpForm.uiSchema[key] = {
                    "ui:widget": "userWidget"
                };
            break;
            case 'mt_tree':
                let gtt = getThesaurusTree(properties[key].options);
                if (is.array(gtt) && gtt.length==0) {
                  gtt = getTesaurusList(properties[key].options);
                }
                //console.log('gtt------>>>>',gtt,JSON.stringify(properties[key].options));
                tmpForm.schema.properties[key] = {
                    "title": properties[key].title,
                    "type": "string",
                    "options" : gtt
                };
                tmpForm.uiSchema[key] = {
                    "ui:widget": "myMtSelectWidget"
                };
            break;
            case 'mt_tree_multi':
                let gtt_multi = getThesaurusTree(properties[key].options);
                //console.log('gtt------>>>>',gtt,properties[key].options);
                tmpForm.schema.properties[key] = {
                    "title": properties[key].title,
                    "type": "string",
                    "options" : gtt_multi
                };
                tmpForm.uiSchema[key] = {
                    "ui:widget": "myMtMultiSelectWidget"
                };
            break;
            case 'org_tree':
                //let gtt = getThesaurusTree(properties[key].options);

                //console.log('gtt------>>>>',gtt,properties[key].options);
                tmpForm.schema.properties[key] = {
                    "title": properties[key].title,
                    "type": "string",
                    "options" : properties[key].options
                };
                tmpForm.uiSchema[key] = {
                    "ui:widget": "myOrgSelectWidget"
                };
            break;
            case 'date':
                tmpForm.schema.properties[key] = {
                    "title": properties[key].title,
                    "type": "string",
                };
                tmpForm.uiSchema[key] = {
                    "ui:widget": "myDateWidget"
                };
            break;
            case 'checkbox':
                tmpForm.schema.properties[key] = {
                    "title": properties[key].title,
                    "type": "boolean",
                    "default": false
                };
            break;
            case 'text':
                tmpForm.schema.properties[key] = {
                    "title": properties[key].title,
                    "type": "string",
                };
                tmpForm.uiSchema[key] = {
                    "ui:widget": "textarea"
                };
            break;
            case 'upload':
            tmpForm.schema.properties[key] = {
                "title": properties[key].title,
                "type": "string",
            };
            tmpForm.uiSchema[key] = {
                "ui:widget": "docFileWidget"
            };
        break;
            default:
                tmpForm.schema.properties[key] = {
                    "title": properties[key].title,
                    "type": properties[key].type,
                }
        }
        if (properties[key].hidden) {
            tmpForm.uiSchema[key] = {
                "ui:widget": "hidden"
            };
        }
    }
    return tmpForm;
}

export function isCan(module,permission, user) {
    console.log(module,permission,user.permissions[module][permission], user.permissions[module]);
    return user.permissions[module][permission];
}

//export function getOrganizationsTree(items,level=0)
