import React from 'react';
//import { Select } from 'antd';
//const Option = Select.Option;
import { TreeSelect } from 'antd';
const TreeNode = TreeSelect.TreeNode;



export const MyMtSelectWidget = (props) => {
  //console.log('props----->',props);
  function  handleChange(value) {
    console.log(`selected ${value}`);
    }
  const {options} = props;
  const {color, backgroundColor} = options;

  let state = {
    value: undefined,
  }
  
  let onChange = (value) => {
    console.log(arguments,value);
    //this.setState({ value });
    //value={this.state.value}
    //props.onChange(event.target.value)
  }

  let prepareTree = (tree) => {
    let out = tree.map((item)=>{
        if (item.children && item.children.length > 0) {
            return <TreeNode value={''+item.huri_code} title={item.english} key={''+item.huri_code}>{prepareTree(item.children)}</TreeNode>
        } else {
            return <TreeNode value={''+item.huri_code} title={item.english} key={''+item.huri_code}/>
        }
    })
    return out;
  }

  return (
    <TreeSelect
        showSearch
        style={{ width: "100%" }}
        value={props.value}
        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
        placeholder="Please select"
        allowClear
        //treeDefaultExpandAll
        treeNodeFilterProp="title"
        onChange={(event) => {
          console.log('....................',event);
          props.onChange(event)
          }}
        onSearch={(v)=>console.log('search',v)}
        disabled={props.readonly}
        //dropdownClassName="t555"
        //dropdownStyle={{zIndex:1000000}}
        getPopupContainer={()=>document.getElementById('select-ph')}
      >
        {prepareTree(props.schema.options)}
      </TreeSelect>
  );
};

