import {MyCustomWidget} from "./MyCustomWidget";
import {MySelectWidget} from "./MySelectWidget";
import {MyMtSelectWidget} from "./MyMtSelectWidget";
import {MyMtMultiSelectWidget} from "./MyMtMultiSelectWidget";
import {MyOrgSelectWidget} from "./MyOrgSelectWidget";
import {MyDateWidget} from "./MyDateWidget";
import {userWidget} from "./userWidget.1";
import {docFileWidget} from "./docFileWidget";
export const widgets = {
  myCustomWidget: MyCustomWidget,
  mySelectWidget: MySelectWidget,
  myMtSelectWidget: MyMtSelectWidget,
  myMtMultiSelectWidget: MyMtMultiSelectWidget,
  myOrgSelectWidget: MyOrgSelectWidget,
  myDateWidget: MyDateWidget,
  userWidget: userWidget,
  docFileWidget: docFileWidget,
};
