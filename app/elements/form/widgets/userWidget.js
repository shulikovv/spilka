import React from 'react'
import { message, Modal } from 'antd';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';
import Select2 from 'react-select2-wrapper';
var mock = new MockAdapter(axios);
mock
.onGet('/api1/person/get-person-list-for-field').reply(200, 
  [
     {
        person_id: 1,
        counting_unit: '...',
        person_name: 'Name',
        date_of_birth: '1968-01-01',
        sex: 'male',
     }, 
  ]
)
.onAny().passThrough();


class UserSelect extends React.Component {

  constructor(props) {
    super(props);
    console.log('UserSelect', props);
    this.state = {
      visible: false
    }
    this.handleOk = () => {
      this.props.onChange('dfg')
      this.setState({
        visible: false
      })
    }
    this.handleCancel = () => {
      this.setState({
        visible: false
      })
    }
  }

  async componentWillUpdate(nextProps, nextState) {
    const response = await axios.get('/api/person/get-person-form/1', {
      headers: {
          Authorization: 'Bearer '+ localStorage.getItem('token')
      }
    })
    console.log(response.data);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.visible==false) {
      $("#user-widget-grid").kendoGrid({
        dataSource: {
            type: "json",
            transport: {
                read: function(options) {
                  console.log('options',options);
                  axios.get('/api/person/get-person-list-for-field', {
                    headers: {
                        Authorization: 'Bearer '+ localStorage.getItem('token')
                    }
                  })
                  .then((result)=>{
                    console.log('grid loaded', result.data)
                    options.success(result.data);
                  })
                  .catch((err)=>{
                    options.error(err.data);
                  })
                }
            },
            pageSize: 20,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        height: 550,
        filterable: true,
        sortable: true,
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        columns: [{
            field: "counting_unit",
            title: "Counting unit",
        }, {
            field: "person_name",
            title: "Person name"
        }, {
            field: "date_of_birth",
            title: "Date of birth"
        }, {
            field: "sex",
        },{
          title: 'Actions',
          command: [
            {
              name: "Edit",
              click: (e) => {
                  var grid = $("#user-widget-grid").data("kendoGrid");
                  var data = grid.dataItem($( e.currentTarget ).parent().parent());
                  //window.location="<?= Yii::app()->createAbsoluteUrl('orders/edit'); ?>/"+data.orders_id;
                  message.info('This is a normal message '+data.person_name );
                  console.log('here', data.person_id );
              }
            }
          ]
        }]
      });
    }
    console.log('did update',prevState, $("#user-widget-grid"));
  }
  

  componentDidMount() {
    console.log('didmounted aaa');

  }

  

  render() {
    let Clicked = () => {
      this.setState({
        visible: true
      })
    }
    return (
      <span className="input-icon input-icon-right">
            <input type="text" 
                placeholder="Text Field" 
                className="form-control" 
                value={this.props.value || ''}
                required={this.props.required}
                readOnly
                style={{backgroundColor:'#ffffff'}}
                onChange={(event) => {
                  console.log(',,,/',event.target.value);
                  props.onChange(event.target.value)
                }}
            />
            <i className="ti-user" onClick={Clicked} ></i>
            <Modal
          title="Select related person"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          width='90%'
        >

          <div id="user-widget-grid"></div>
        </Modal>
      </span>
    )
  }
}



export const userWidget = (props) => {
  return (
      <UserSelect {...props} />
  )
};