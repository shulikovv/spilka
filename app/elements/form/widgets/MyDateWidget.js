import React from 'react';
import { DatePicker } from 'antd';
import 'antd/lib/date-picker/style/css';
import moment from 'moment';
//var moment = require('moment');

export const MyDateWidget = (props) => {
    console.log('props.value    ',props.value);
    return (
        <DatePicker 
                style={{ width: "100%" }}
                size="100%"
                value={props.value?moment(props.value):null}
                required={props.required}
                onChange={(date, dateString) => props.onChange(dateString)}
                disabled={props.readonly}
                getPopupContainer={()=>document.getElementById('select-ph')}
                popupStyle={{zIndex:10000000}}
                //format="DD/MM/YYYY"
                
            />
    )
}