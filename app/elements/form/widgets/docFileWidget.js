import React from 'react'
import { message, Modal, Table, Input, Button, Icon  } from 'antd';
import { Upload } from 'antd';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

class DocUpload extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fileList: [],
      uploading: false,
    }
    this.handleUpload = () => {
      const { fileList } = this.state;
      const formData = new FormData();
      fileList.forEach((file) => {
        formData.append('files[]', file);
      });

      this.setState({
        uploading: true,
      });
    };
  }





    render() {

      const { uploading } = this.state;
      const props = {
        action: '',
        onRemove: (file) => {
          this.setState(({ fileList }) => {
            const index = fileList.indexOf(file);
            const newFileList = fileList.slice();
            newFileList.splice(index, 1);
            return {
              fileList: newFileList,
            };
          });
        },
        beforeUpload: (file) => {
          this.setState(({ fileList }) => ({
            fileList: [...fileList, file],
          }));
          return false;
        },
        fileList: this.state.fileList,
      };


      return (<Upload {...props}>
        <Button>
          <Icon type="upload" /> Select File
        </Button>
      </Upload>)
    }
}


export const docFileWidget = (props) => {
  console.log('docFileWidget----->',props)
  return (
      <div >
        <DocUpload {...props} />
      </div>
  )
};
