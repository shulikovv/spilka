import React from 'react';
//import { Select } from 'antd';
//const Option = Select.Option;
import { TreeSelect } from 'antd';
const TreeNode = TreeSelect.TreeNode;



export const MyOrgSelectWidget = (props) => {
  //console.log('props----->',props);
  const {options} = props;
  const {color, backgroundColor} = options;

  let state = {
    //value: undefined,
  }
  
  let onChange = (value) => {
    console.log(arguments);
    //this.setState({ value });
    //value={this.state.value}
    //props.onChange(event.target.value)
  }

  let  prepareTree = (tree) => {
        let out = tree.map((item)=>{
            if (item.children && item.children.length > 0) {
                return <TreeNode value={''+item.organization_id} title={item.organization_title} key={''+item.organization_id}>{prepareTree(item.children)}</TreeNode>
            } else {
                return <TreeNode value={''+item.organization_id} title={item.organization_title} key={''+item.organization_id}/>
            }
        })
        return out;
    }

  return (
    <TreeSelect
        showSearch
        style={{ width: "100%" }}
        value={props.value}
        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
        placeholder="Please select"
        allowClear
        treeDefaultExpandAll
        onChange={(event) => props.onChange(event)}
        disabled={props.readonly}
      >
        {prepareTree(props.schema.options)}
      </TreeSelect>
  );
};

