import React from 'react';
//import { Select } from 'antd';
//const Option = Select.Option;
import { TreeSelect } from 'antd';
const TreeNode = TreeSelect.TreeNode;
import is from 'is_js';


export const MyMtMultiSelectWidget = (props) => {
  //console.log('props----->',props);
  function  handleChange(value) {
    console.log(`selected ${value}`);
    }
  const {options} = props;
  const {color, backgroundColor} = options;

  let state = {
    value: undefined,
  }
  
  let onChange = (value) => {
    console.log(arguments,value);
    //this.setState({ value });
    //value={this.state.value}
    //props.onChange(event.target.value)
  }

  let prepareTree = (tree) => {
    let out = tree.map((item)=>{
        if (item.children && item.children.length > 0) {
            return <TreeNode value={''+item.huri_code} title={item.english} key={''+item.huri_code}>{prepareTree(item.children)}</TreeNode>
        } else {
            return <TreeNode value={''+item.huri_code} title={item.english} key={''+item.huri_code}/>
        }
    })
    return out;
  }
  let value = [];
  let props_value = '';
  if (is.not.undefined(props.value)) {
    props_value = props.value;
  }
  try {
    value = JSON.parse(props_value)
  } catch (err) {
    console.log('value ==>', props_value);
    value = JSON.parse('['+props_value+']')
  }
  return (
    <TreeSelect
        showSearch
        style={{ width: "100%" }}
        value={value}
        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
        placeholder="Please select"
        allowClear
        multiple
        //treeDefaultExpandAll
        treeNodeFilterProp="title"
        onChange={(event) => {
          console.log('....................',event);
          props.onChange(JSON.stringify(event))
          }}
        onSearch={(v)=>console.log('search',v)}
        disabled={props.readonly}
        //dropdownClassName="t555"
        //dropdownStyle={{zIndex:1000000}}
        getPopupContainer={()=>document.getElementById('select-ph')}
      >
        {prepareTree(props.schema.options)}
      </TreeSelect>
  );
};

