import React from 'react'
import { message, Modal, Table, Input, Button, Icon  } from 'antd';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';
var mock = new MockAdapter(axios);
import Form from "react-jsonschema-form";
import Select2 from 'react-select2-wrapper';
import {getThesaurusTree} from '../../../utils/index';
import {widgets} from "../../../elements/form/widgets";

mock
.onAny().passThrough();


class UserSelect extends React.Component {

  constructor(props) {
    super(props);
    console.log('UserSelect', props);
    this.loadE();
    this.state = {
      visible: false,
      filterDropdownVisible: false,
      //data,
      person: {

      },
      searchText: '',
      filtered: false,
      countingUnitOptions: [],
      sexOptions: [],
      searchFormData: {},
      dataSource: [
     ]
    }
    this.handleOk = () => {
      this.props.onChange('dfg')
      this.setState({
        visible: false
      })
    }
    this.handleCancel = () => {
      this.setState({
        visible: false
      })
    }

    this.onInputChange = (e) => {
      this.setState({ searchText: e.target.value });
    }
    this.onSearch = (value) => {
        console.log('onSearch',value, this.state.searchText);
    }
    this.doSearch = ({formData}) => {
      axios.get('/api/person/get-person-list-for-form', {
        headers: {
            Authorization: 'Bearer '+ localStorage.getItem('token')
        },
        params: {
          ...formData
        },
        paramsSerializer: function(params) {
          return qs.stringify(params, {arrayFormat: 'brackets'})
        },
      })
      .then((result)=>{
          this.setState({dataSource:result.data});
      })
    }

    this.personSelected = (person) => {
      this.props.onChange(person.person_id.toString());

      this.setState({
        person: person,
        visible: false
      })
    }

  }

  async loadE(){
    let counting = await axios.get('/api/thesaurus/7', {
      headers: {
          Authorization: 'Bearer '+ localStorage.getItem('token')
      }
    });
    let sex = await axios.get('/api/thesaurus/39', {
      headers: {
          Authorization: 'Bearer '+ localStorage.getItem('token')
      }
    });
    let tree = getThesaurusTree(counting.data);
    let sextree = getThesaurusTree(sex.data);
    this.setState({
      countingUnitOptions: tree,
      sexOptions: sextree
    })
    //console.log('.......>>>',sextree,tree);
  }

  async componentWillUpdate(nextProps, nextState) {
    console.log('will update', nextState);
    const response = await axios.get('/api/person/get-person-form/1', {
      headers: {
          Authorization: 'Bearer '+ localStorage.getItem('token')
      }
    })
    console.log(response.data);
  }

  componentDidUpdate(prevProps, prevState) {
    //if (prevState.visible==false) {
    //}
    console.log('did update',prevState, this.state);
  }
  
  

  componentDidMount() {
    console.log('didmounted aaa');

  }

  

  render() {
    let Clicked = () => {
      this.setState({
        visible: true
      })
    }

    let Clear = () => {
      this.setState({
        person: {}
      })
      this.props.onChange()
    }

    const columns = [
      {
        title: 'Счетная единца',
        dataIndex: 'counting_unit',
        key: 'counting_unit',
      },
      {
        title: 'Имя',
        dataIndex: 'person_name',
        key: 'person_name',
      },
      {
        title: 'Дата рождения',
        dataIndex: 'date_of_birth',
        key: 'date_of_birth',
      },
      {
        title: "Пол",
        dataIndex: 'sex'
      },
      {
        title: 'Действия',
        key: 'actions',
        render: (text, record) => (
            <span>
              <a onClick={()=> this.personSelected(record)}>Выбрать</a>
            </span>
        )
      }
      ]
      const uiSchema = {
        counting_unit: {
          "ui:widget": "myMtSelectWidget"
        },
        sex: {
          "ui:widget": "myMtSelectWidget"
        }
      }
      const schema = {
        type: "object",
        properties: {
          //person_record_number: {type: "string", title: "Person Record Number"},
          counting_unit: {
            type: "string", 
            title: "Счетная единца",
            options: this.state.countingUnitOptions
          },
          person_name: {type: "string", title: "Имя"},
          date_of_birth: {type: "string", title: "Дата рождения"},
          sex: {
            type: "string", 
            title: "Пол",
            options: this.state.sexOptions
          },
        }
      };
    return (
      <span className="input-icon input-icon-right">
            <input type="text" 
                placeholder="Select related person" 
                className="form-control" 
                value={this.state.person.person_name || ''}
                required={this.props.required}
                readOnly
                style={{backgroundColor:'#ffffff'}}
                onChange={(event) => {
                  //this.props.value
                  //this.state.person.person_name
                  //this.props.onChange(event.target.value)
                }}
            />
            <i className="ti-close" onClick={Clear} style={{right:'23px'}}></i>
            <i className="ti-user" onClick={Clicked} ></i>
            <Modal
          title="Select related person"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          width='90%'
        >
        <Form 
        uiSchema={uiSchema}
        schema={schema}
        widgets={widgets}
        formData={this.state.searchFormData} 
        onChange={({formData})=>this.setState({searchFormData:formData})}
        onSubmit={this.doSearch}
        onError={()=>console.log("errors123")}>
        <button type="submit" className='ant-btn ant-btn-primary ant-btn-lg'>Искать</button>
        </Form><br/>
          <Table rowKey='person_id' dataSource={this.state.dataSource} columns={columns} />
        </Modal>
      </span>
    )
  }
}

export const userWidget = (props) => {
  return (
      <UserSelect {...props} />
  )
};