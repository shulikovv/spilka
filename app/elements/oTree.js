import React from 'react'
import { Tree } from 'antd';
import 'antd/lib/tree/style/css';

export const OTree = (props) => {
  const {options} = props;
  const {color, backgroundColor} = options;
  return (
      <span className="input-icon">
            <input type="text" 
                placeholder="Text Field" 
                className="form-control" 
                value={props.value}
                required={props.required}
                onChange={(event) => props.onChange(event.target.value)}
            />
            <i className="ti-user"></i> 
      </span>
  );
};