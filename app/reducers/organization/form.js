import * as is from 'is_js';

const initialState = {
    form: {
        tabs: []
    },
    formData: {
        tabs: []
    },
    tree: [],
    treeDefaultSelectedKeys: '1',

};

export default function organizationForm(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'ORGLIST_FORM_STRUCTURE_LOADED':
            return {...state,form: action.payload.formStructure}
        break;
        case 'ORGLIST_FORM_TREE_LOADED':
            if (is.array(action.payload.tree) && action.payload.tree.length > 0 && is.propertyDefined(action.payload.tree[0],'organization_id')) {
                return {...state,tree: action.payload.tree, treeDefaultSelectedKeys: action.payload.tree[0].organization_id, currentOrg: action.payload.tree[0].organization_id}
            } else {
                return {...state,tree: action.payload.tree}
            }
        break;
        case 'ORGLIST_SET_CURRENT_SEL_ORG':
            return {...state, currentOrg: action.payload.org}
        break;
        default:
            return state;
    }
}