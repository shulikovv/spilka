const initialState = {
    form: {
        tabs: []
    }
};

export default function organizationForm(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'ORGLIST_FORM_STRUCTURE_LOADED':
            return {...state,form: action.payload.formStructure}
        break;
        default:
            return state;
    }
}