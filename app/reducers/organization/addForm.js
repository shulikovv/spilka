const initialState = {
    form: {
        schema: {},
        uiSchema: {},
        data: {}
    },
    errors: {}
};

export default function orgAddForm(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'ORGADD_FORM_STRUCTURE_LOADED':
            return {...state,form: action.payload.formStructure}
        break;
        case 'ORGADD_FORM_DATA_LOADED':
            return {...state,form: {...state.form,data: action.payload.data}}
        break;
        case 'ORGADD_SAVE_FORM_ERRORS':
            return {...state, errors: action.payload.errors}
        break;
        case 'ORGADD_SAVE_FORM_OK':
            return {...state, errors: {}}
        break
        default:
            return state;
    }
}