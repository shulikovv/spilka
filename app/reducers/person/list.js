import * as is from 'is_js';

const initialState = {
    data: [
        {
            person_id: '1',
            counting_unit: '',
            person_name: 'Mike',
            date_of_birth: '2017-06-21',
            sex: 'male'
        },
    ],
    filtr: {},
    total: 0,
    page: 0,
    columns: [
        {
          title: '№',
          dataIndex: 'person_id',
          'width': 150  
        },
        {
          title: 'Единица измер.',
          dataIndex: 'counting_unit',
          'width': 250
        },
        {
          title: 'Имя',
          dataIndex: 'person_name',
          width: 350
        },
        {
          title: 'Дата рожд.',
          dataIndex: 'date_of_birth',
          width: 250
        },
        {
          title: 'Пол',
          dataIndex: 'sex',
          width: 250
        },

    ],
    errors: {}
};

export default function organizationForm(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'PERSON_LIST_DATA_LOADED':
            return {...state,data:action.payload.list}
        default:
            return state;
    }
}