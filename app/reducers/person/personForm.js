import * as is from 'is_js';

const initialState = {
    form: {
        schema: {},
        uiSchema: {},
        data: {}
    },
    errors: {}
};

export default function organizationForm(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'PERSON_FORM_STRUCTURE_LOADED':
            return {...state,form: action.payload.formStructure}
        case 'ERROR_ADD_PERSON':
            return {...state, errors: action.payload.errors}
        default:
            return state;
    }
}
