import * as is from 'is_js';

const initialState = {
    form: {
        schema: {},
        uiSchema: {},
        data: {}
    },
    aform: {
        type: '',
        schema: {},
        uiSchema: {},
        data: {}
    },
    addresses : {
        tableData: [],
        fiter: {},
        total: 0,
        page: 0,
        modalvisible: false,
        modalviewvisible: false,
        modaleditvisible: false,
    },
    bioforms: {
        tableData: [],
        filter: {},
        total: 0,
        page: 0,
        modalvisible: false,
        modalviewvisible: false,
        modaleditvisible: false,
    },
    bioform: {
        type: '',
        schema: {},
        uiSchema: {},
        data: {}
    },
    documents: {
        modaladdvisible: false,
        modalselectvisible: false,
        modalviewvisible: false,
    },
    errors: {}
};

export default function organizationForm(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'PERSON_VIEW_FORM_STRUCTURE_LOADED':
            return {...state,form: action.payload.formStructure}
        break;
        case 'PERSON_VIEW_ADDRESS_FORM_STRUCTURE_LOADED':
            return {...state,aform: action.payload.formStructure}
        break;
        case 'PERSON_VIEW_BIOGRAPHIC_FORM_STRUCTURE_LOADED':
            return {...state,bioform: action.payload.formStructure}
        break;
        case 'OPEN_ADD_ADDRESS_MODAL':
            return {
                ...state, 
                addresses: {
                    ...state.addresses, 
                    modalvisible: true
                },
                aform: {
                    ...state.aform,
                    type: 'add',
                    data: {
                        person_id: action.payload.personId
                    }
                }
            }
        case 'CLOSE_ADD_ADDRESS_MODAL':
            return {...state, addresses: {...state.addresses, modalvisible: false}}
        case 'CLOSE_EDIT_ADDRESS_MODAL':
            return {...state, addresses: {...state.addresses, modaleditvisible: false}}
        case 'CLOSE_VIEW_ADDRESS_MODAL':
            return {...state, addresses: {...state.addresses, modalviewvisible: false}}
        case 'OPEN_VIEW_ADDRESS_MODAL':
            return {
                ...state,  
                addresses: {
                    ...state.addresses, 
                    modalviewvisible: true
                },
                aform: {
                    ...state.aform,
                    type: 'view',
                    data: action.payload.personAddress
                }
            }
        case 'OPEN_EDIT_ADDRESS_MODAL':
            return {
                ...state,  
                addresses: {
                    ...state.addresses, 
                    modaleditvisible: true
                },
                aform: {
                    ...state.aform,
                    type: 'edit',
                    data: action.payload.personAddress
                }
            }
        case 'PERSON_ADDRESS_DATA_LOADED':
            return {...state, aform: {...state.aform, data: action.payload.formData || {}}}
        case 'PERSON_ADDRESSES_DATA_LOADED':
            return {...state, addresses: {...state.addresses, tableData: action.payload.addresses}}
        case 'OPEN_ADD_BIO_MODAL' :
            return {
                ...state, 
                bioforms: {
                    ...state.bioforms,  
                    modalvisible: true
                }, 
                bioform: {
                    ...state.bioform, 
                    data: {
                        person: action.payload.personId
                    }, 
                    type: 'add'
                }
            }
        case 'CLOSE_ADD_BIO_MODAL' :
            return {...state, bioforms: {...state.bioforms, modalvisible: false}}
        case 'PERSON_BIO_DATA_LOADED':
            return {...state, bioforms: {...state.bioforms, tableData: action.payload.bio}}
        case 'OPEN_VIEW_BIO_MODAL':
            return {...state, bioforms: {...state.bioforms,  modalvisible: true}, bioform: {...state.bioform, data: action.payload.personBio, type: 'view'}}
        case 'OPEN_EDIT_BIO_MODAL':
            return {...state, bioforms: {...state.bioforms,  modalvisible: true}, bioform: {...state.bioform, data: action.payload.personBio, type: 'edit'}}
        case 'OPEN_ADD_PERSON_DOC_MODAL':
            return {...state, documents: {...state.documents, modaladdvisible: true}}
        case 'CLOSE_ADD_PERSON_DOC_MODAL':
            return {...state, documents: {...state.documents, modaladdvisible: false}}
        case 'OPEN_SEL_PERSON_DOC_MODAL':
            return {...state, documents: {...state.documents, modalselectvisible: true}}
        case 'CLOSE_SEL_PERSON_DOC_MODAL':
            return {...state, documents: {...state.documents, modalselectvisible: false}}
        default:
            return state;
    }
}