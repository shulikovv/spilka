import * as is from 'is_js';

const initialState = {
    form: {
        schema: {},
        uiSchema: {},
        data: {}
    },
    errors: {}
};

export default function eventForm(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'EVENT_FORM_STRUCTURE_LOADED':
            return {...state,form: action.payload.formStructure}
        break;
        default:
            return state;
    }
}