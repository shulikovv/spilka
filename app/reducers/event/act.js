import * as is from 'is_js';

const initialState = {
    personslist: [],
    form: {
        schema: {},
        uiSchema: {},
        data: {}
    },
    errors: {},
    acts: [
        {
            id: 0,
            date: '',
            victimId: 0,
            victimName: '',
            typeActId: 0,
            typeActName: '',
            perpetrators: [
                {
                    perpetratorId: 0,
                    perpetratorName: '',
                    involvementId: 0,
                    involvementName: ''
                }
            ]
        }
    ],
    act: {
        victim: {}
    },
    victimForm: {
        schema: {},
        uiScema: {}
    }
};

export default function eventForm(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'PERSON_LIST_FOR_ACT_LOADED':
            return {...state,personslist: action.payload.personlist}
        break;
        case 'SET_ADD_ACT_VICTIM_RETURN_PATH':
            return {...state, returnpath: action.payload.returnpath}
        break;
        case 'VICTIM_DATA_LOADED':
            return { ...state, act: {...state.act,victim: action.payload.victimForm.data}, victimForm: {schema:action.payload.victimForm.schema, uiSchema: action.payload.victimForm.uiSchema}}
        default:
            return state;
    }
}