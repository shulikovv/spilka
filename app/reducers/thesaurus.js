const initialState = {
    list: [],
    tree: [],
    isLoading: false,
    isLoaded: false,    
}
export default function user(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'THESAURUS_START_LOADING':
            return { ...state, isLoading: true, isLoaded: false}
        case 'THESAURUS_LOADED':
            return { ...state, isLoading: false, isLoaded: true, list: action.payload.list, tree: action.payload.tree}
        default:
            return state;
    }
}