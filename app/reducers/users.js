import React from 'react';
import { Table, Icon, Button, Popconfirm } from 'antd';
import { push } from 'react-router-redux';
import 'antd/lib/popconfirm/style/css';
import {findIndex} from 'lodash';

const initialState = {
    list: [],
    form: {},
    addFormSchema:{
        type: "object",
        "required": [
            "username",
            "email",
            "password"
        ],
        properties: {
            username: {
                "title": "User name",
                "type": "string",
                minLength: 5
            },
            email: {
                "type": "string",
                "format": "email"
            },
            password: {
                "type": "string",
                "format": "password",
                minLength: 5
            },
            password2: {
                "type": "string",
                "format": "password"
            },
        }
    },
    editFormSchema:{
        type: "object",
        "required": [
            "username",
            "email",
        ],
        properties: {
            username: {
                "title": "User name",
                "type": "string",
                minLength: 5
            },
            email: {
                "type": "string",
                "format": "email"
            },
            password: {
                "type": "string",
                "format": "password",
                minLength: 5
            },
            password2: {
                "type": "string",
                "format": "password"
            },
        }
    },
    viewFormSchema:{
        type: "object",
        properties: {
            username: {
                "title": "User name",
                "type": "string",
                minLength: 5
            },
            email: {
                "type": "string",
                "format": "email"
            },
        }
    },
    listCollumn: [
        {
          title: 'User name',
          dataIndex: 'username'
        },
        {
          title: 'Email',
          dataIndex: 'email'
        },
    ],
    isLoading: false,
    isLoaded: false,
    showAddUserModal: false,
    showViewUserModal: false,
    showEditUserModal: false,
    viewFormData: {
        username: '',
        email: ''
    },
    viewFormUiSchema : {
        "ui:readonly": true
    }
}
//
export default function user(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'USERS_LIST_START_LOADING':
            return { ...state, isLoading: true, isLoaded: false}
        case 'USERS_LIST_LOADED':
            return { ...state, isLoading: false, isLoaded: true, list: action.payload.user_list}
        case 'ADD_USER_SHOW_MODAL':
            return { ...state, showAddUserModal: true}
        case 'ADD_USER_CLOSE_MODAL':
            return { ...state, showAddUserModal: false}
        case 'ADD_DATA_TO_USER_LIST':
            let NewUserList = [ ...state.list];
            NewUserList.push({
                id: NewUserList.length+100,
                username: action.payload.userData.username,
                email: action.payload.userData.email,
            });
            return { ...state, list: NewUserList}
        case 'VIEW_USER_SHOW_MODAL':
            let index = findIndex(state.list, {id: action.payload.id});
            return { ...state, showViewUserModal: true, viewFormData: state.list[index]}
        case 'VIEW_USER_CLOSE_MODAL':
            return { ...state, showViewUserModal: false}
        case 'EDIT_USER_SHOW_MODAL':
            let indexEdit = findIndex(state.list, {id: action.payload.id});
            return { ...state, showEditUserModal: true, viewFormData: state.list[indexEdit]}
        case 'EDIT_USER_CLOSE_MODAL':
            return { ...state, showEditUserModal: false}
        case 'CHANGE_DATA_IN_USER_LIST':
            let indexChange = findIndex(state.list, {id: action.payload.userData.id});
            let NewUserListEdit = [ ...state.list];
            NewUserListEdit[indexChange] = {
                id: action.payload.userData.id,
                username : action.payload.userData.username,
                email : action.payload.userData.email,
            };
            return { ...state, showEditUserModal: true, list: NewUserListEdit}
        default:
            return state;
    }
}