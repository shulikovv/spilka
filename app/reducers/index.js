import { combineReducers } from 'redux';
//import page from './page'
import user from './user';
import users from './users';
import person from './person';
import loginForm from './loginForm';
import thesauri from './thesauri';
import thesaurus from './thesaurus';
import org1 from './organizations1';
import vechicles from './vechicles';
import permission from './permission';
import orgForm from './organization/form';
import orgAddForm from './organization/addForm';
import personForm from './person/personForm';
import personView from './person/personView';
import personList from './person/list';
import eventForm from './event/form';
import eventAct from './event/act';
import elemUserWidget from './elements/userWidget';
import docs from './document/document';

//import { routerStateReducer } from 'redux-router';
import { routerReducer } from 'react-router-redux';
export default combineReducers({
    person,
    loginForm,
    user,
    users,
    thesauri,
    thesaurus,
    org1,
    vechicles,
    permission,
    orgForm,
    orgAddForm,
    personForm,
    personView,
    personList,
    eventForm,
    eventAct,
    docs,
    //router: routerStateReducer,
    routing: routerReducer
})
