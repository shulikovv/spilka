const initialState = {
    errors: [],
    persondata: {

    },
    loading: false,
    schema : {
        type: "object",
        properties: {
            test: {
                "type": "string",
                enum: [
                    "asdff",
                    "ssjksjlds",
                    "sdkldks;"
                ]
            },
            "counting_unit": {
                "title": "Counting unit",
                "type": "string",
            },
            "person_name": {
                "title": "Name",
                "type": "string",
            },
            "confidentiality": {
                "type": "boolean",
                "title": "Confidentiality",
            },
            "other_names": {
                "title": "Other names",
                "type": "string",
            },
            "date_of_birth": {
                "title": "Date of Birth",
                "type": "string",
                "format": "date"
            },
            "date_of_birth_type": {
                "title": "Date of Birth Type",
                "type": "string",
                "enum": [
                    "estimate",
                    "noday",
                    "nomonth"
                ],
                "enumNames": [
                    "Estimate",
                    "Unknown day",
                    "Unknown month and day"
                ]
            },
            "place_of_birth": {
                "title": "Place of Birth",
                type: "string"
            },
            "locality_of_birth": {
                "title": "Locality of Birth",
                type: "string"
            },
            "sex": {
                "title": "Sex",
                type: "string"
            },
            "sexual_orientation": {
                "title": "Sexual Orientation",
                type: "string"
            },
            "identification_documents": {
                title: "Identification Documents",
                type: "string",
                format: "textarea"
            },
            "civil_status": {
                "title": "Civil Status",
                "type": "string"
            },
            "dependants": {
                "title": "Dependants",
                "type": "string"
            },
            "formal_education": {
                "title": "Formal Education",
                "type": "string"
            },
            "other_training": {
                "title": "Other Training",
                "type": "string"
            },
            "occupation": {
                "title": "Occupation",
                "type": "string"
            },
            "local_term_for_occupation": {
                "title": "Local Term for Occupation",
                "type": "string"
            },
            "health": {
                "title":"Health",
                "type": "string"
            },
            "medical_records": {
                "title":"Medical records",
                "type": "string"
            },
            "physical_description": {
                "title":"Physical description",
                "type": "string"
            },
            "deceased": {
                "title":"Deceased",
                "type": "boolean"
            },
            "date_deceased": {
                "title":"Date Deceased",
                "type": "string"
            },
            "date_deceased_type": {
                "title":"Date Deceased Type",
                "type": "string",
                "enum": [
                    "estimate",
                    "noday",
                    "nomonth"
                ],
                "enumNames": [
                    "Estimate",
                    "Unknown day",
                    "Unknown month and day"
                ]

            },
            "group_description": {
                "title":"Group description",
                "type": "string"
            },
            "number_of_persons_in_group": {
                "title":"Number of persons in group",
                "type": "string"
            },
            "religion": {
                "title":"Religion",
                "type": "string"
            },
            "citizenship": {
                "title":"Citizenship",
                "type": "string"
            },
            "ethnic_background": {
                "title":"Ethnic Background",
                "type": "string"
            },
            "other_background": {
                "title":"Other Background",
                "type": "string"
            },
            "general_characteristics": {
                "title":"General Characteristics",
                "type": "string",
                "format": "textarea",
            },
            "language": {
                "title":"Language",
                "type": "string"
            },
            "local_language": {
                "title":"Local Language",
                "type": "string"
            },
            "national_origin": {
                "title":"National Origin",
                "type": "string"
            },
            "remarks": {
                "title":"Remarks",
                "type": "string",
                "format": "textarea",
            },
            "reliability_as_source": {
                "title":"Reliability as Source",
                "type": "string"
            },
            "reliability_as_intervening_party": {
                "title":"Reliability as Intervening Party",
                "type": "string"
            },
            "files": {
                "title":"Files",
                "type": "string"
            },
            "date_received": {
                "title":"Date Received",
                "type": "string"
            },
            "project_title": {
                "title":"Project Title",
                "type": "string"
            },
            "comments": {
                "title":"Comments",
                "type": "string",
                "format": "textarea"
            },
        }
    },
    columns: [
        {
          title: '#',
          dataIndex: 'preson_id',
          'width': 150  
        },
        {
          title: 'Единица измерения',
          dataIndex: 'unit',
          'width': 250
        },
        {
          title: 'Имя',
          dataIndex: 'name',
          width: 350
        },
        {
          title: 'Дата рожд.',
          dataIndex: 'birth',
          width: 250
        },
        {
          title: 'Пол',
          dataIndex: 'sex',
          width: 250
        }  
    ]
}
export default function person(state = initialState, action = {type: null}) {
    switch (action.type) {
        default:
            return state;
    }
}
