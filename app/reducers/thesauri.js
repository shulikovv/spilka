const initialState = {
    list: [],
    isLoading: false,
    isLoaded: false,    
}
export default function user(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'THESAURI_LIST_START_LOADING':
            return { ...state, isLoading: true, isLoaded: false}
        case 'THESAURI_LIST_LOADED':
            return { ...state, isLoading: false, isLoaded: true, list: action.payload.list}
        default:
            return state;
    }
}