import * as is from 'is_js';

const initialState = {
    form: {
        schema: {},
        uiSchema: {},
        data: {}
    },
    errors: {},
};

export default function eventForm(state = initialState, action = {type: null}) {
    switch (action.type) {
      default:
        return state;
    }
}