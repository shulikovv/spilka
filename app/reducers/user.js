const initialState = {
    profileSchema: {
        type: "object",
        properties: {
            firstName: {
                "title": "First name",
                "type": "string",
                minLength: 5
            },
            lastName: {
                "title": "Last name",
                "type": "string",
            },
            Organization: {
                "title": "Organization",
                "type": "string",
            },
            email: {
                "type": "string",
                "format": "email"
            },
        }
    },
    changePasswordScema: {
        type: "object",
        "required": [
            "password"
        ],
        properties: {
            password: {
                "title": "Current Password",
                "type": "string",
                "format": "password",
                minLength: 5
            },
            passwordNew1: {
                "title": "New Password",
                "type": "string",
                "format": "password",
                minLength: 5
            },
            passwordNew2: {
                "title": "Confirm new Password",
                "type": "string",
                "format": "password"
            },
        }
    },
    user: {},
    roles: ['admin'],
    permissions: {
        dashboard: {
            menuview: false
        },
        'events': {
            'menuview': true,
            create: true,
        },
        person: {
            menuview: true,
            read: true,
            create: true,
            update: true,
            delete: true,
            moderate: true,
            advanced: true
        },
        organizations: {
            menuview: true
        },
        vechicles: {
            menuview: true
        },
        documents: {
            menuview: true,
            create: true
        },
        admin: {
            menuview: true,
            usercreate: false
        }
    }
}
export default function user(state = initialState, action = {type: null}) {
    switch (action.type) {
        default:
            return state;
    }
}

