import {findIndex} from 'lodash';
const initialState = {
  roles: {
      list: [
          {
              role_id: 1,
              role_name: 'Operator'
          },
          {
              role_id: 2,
              role_name: 'Moderator'
          },
          {
              role_id: 3,
              role_name: 'Analyst'
          },
          {
              role_id: 4,
              role_name: 'Administrator'
          },
      ],
      columns: [
          {
              title: 'id',
              dataIndex: 'role_id'
          },
          {
              title: 'Name',
              dataIndex: 'role_name'
          },
      ],
        showViewModal: false,
        showAddModal: false,
        viewFormSchema: {
            type: "object",
            properties: {
                role_id: {
                    "title": "Id",
                    "type": "string",
                },
                role_name: {
                    "type": "string",
                },
            }
        },
        viewFormUiSchema: {
            "ui:readonly": true
        },
        viewFormData: {}
        
  },
  groups: {
      list: [
          {
              group_id: 1,
              group_name: 'Group #1'
          },
          {
              group_id: 2,
              group_name: 'Group #2'
          },
      ],
      columns: [
          {
              title: 'id',
              dataIndex: 'group_id'
          },
          {
              title: 'Name',
              dataIndex: 'group_name'
          },
      ],
        showViewModal: false,
        showAddModal: false,
        viewFormSchema: {
            type: "object",
            properties: {
                group_id: {
                    "title": "Id",
                    "type": "string",
                },
                group_name: {
                    "type": "string",
                },
            }
        },
        viewFormUiSchema: {
            "ui:readonly": true
        },
        viewFormData: {}

  },
    tolerance: {
      list: [
          {
              tolerance_id: 1,
              tolerance_name: 'General'
          },
          {
              tolerance_id: 2,
              tolerance_name: 'Increased'
          },
      ],
        columns: [
            {
                title: 'id',
                dataIndex: 'tolerance_id'
            },
            {
                title: 'Name',
                dataIndex: 'tolerance_name'
            },
        ],
        showViewModal: false,
        showAddModal: false,
        viewFormSchema: {
            type: "object",
            properties: {
                tolerance_id: {
                    "title": "Id",
                    "type": "string",
                },
                tolerance_name: {
                    "type": "string",
                },
            }
        },
        viewFormUiSchema: {
            "ui:readonly": true
        },
        viewFormData: {}
    }
};
export default function permission(state = initialState, action = {type: null}) {
    let index;
    switch (action.type) {
        case 'VIEW_TOLERANCE_SHOW_MODAL':
            index = findIndex(state.tolerance.list, {tolerance_id: action.payload.id});
            return {...state, tolerance : {...state.tolerance, showViewModal: true, viewFormData: state.tolerance.list[index]}}
        case 'VIEW_TOLERANCE_CLOSE_MODAL':
            return {...state, tolerance : {...state.tolerance, showViewModal: false}}
        case 'ADD_TOLERANCE_SHOW_MODAL':
            return {...state, tolerance : {...state.tolerance, showAddModal: true}}
        case 'ADD_TOLERANCE_CLOSE_MODAL':
            return {...state, tolerance : {...state.tolerance, showAddModal: false}}


        case 'VIEW_ROLES_SHOW_MODAL':
            index = findIndex(state.roles.list, {role_id: action.payload.id});
            return {...state, roles : {...state.roles, showViewModal: true, viewFormData: state.roles.list[index]}}
        case 'VIEW_ROLES_CLOSE_MODAL':
            return {...state, roles : {...state.roles, showViewModal: false}}
        case 'ADD_ROLES_SHOW_MODAL':
            return {...state, roles : {...state.roles, showAddModal: true}}
        case 'ADD_ROLES_CLOSE_MODAL':
            return {...state, roles : {...state.roles, showAddModal: false}}


        case 'VIEW_GROUPS_SHOW_MODAL':
            index = findIndex(state.groups.list, {group_id: action.payload.id});
            return {...state, groups : {...state.groups, showViewModal: true, viewFormData: state.groups.list[index]}}
        case 'VIEW_GROUPS_CLOSE_MODAL':
            return {...state, groups : {...state.groups, showViewModal: false}}
        case 'ADD_GROUPS_SHOW_MODAL':
            return {...state, groups : {...state.groups, showAddModal: true}}
        case 'ADD_GROUPS_CLOSE_MODAL':
            return {...state, groups : {...state.groups, showAddModal: false}}
            
            
        default:
            return state;
    }
}