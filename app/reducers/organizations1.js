const initialState = {
    org1Tree : [],
    org1Organization: {
        'information' : {
            //organization_title: '',
            //organization_abb: '',
            //organization_description: ''
        },
        'departments' : [],
        'persnal' : [],
        'vechicles' : []
    },
    showAddModal: false,
    schema : {
        type: "object",
        properties: {
            "org_title": {
                "title": "Organization Title",
                "type": "string",
            },
            "org_abbreviation": {
                "title": "Abbreviation",
                "type": "string",
            },
            "org_parent": {
                "title": "Parent Organization",
                "type": "string",
            },
            "org_departments": {
                "title": "Departments",
                "type": "string",
            },
            "org_recognition": {
                "title": "Recognition marks",
                "type": "string",
            },
            "org_is_state": {
                "title": "is State",
                "type": "boolean",
            },
            "org_initial_date": {
                "title": "Initial Date",
                "type": "string",
                "format": "date",
            },
            "org_description": {
                "title": "Description",
                "type": "string",
                format: "textarea",
            },
            "org_features": {
                "title": "features",
                "type": "string",
                format: "textarea"
            },
            "test": {
                "type": "string",
            }
        }
    },
    uiSchema : {
        org_title : {
             "ui:widget": "myCustomWidget",
             "ui:options": {
                backgroundColor: "red"
            }
        },
        test : {
            "ui:widget": "mySelectWidget"
            //"ui:widget": "myCustomWidget",
        }
    },

}

export default function organization1(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'ORG1_TREE_LOADED':
            return {...state, isLoading: false, isLoaded: true, org1Tree: action.payload.org_tree}
        case 'ORG1_ORGANIZATION_DATA_LOADED':
            return {...state, org1Organization: action.payload.org}
        case 'ORG1_ADD_ORGANIZATION_MODAL':
            return {...state, showAddModal: true}
        case 'ORG1_CLOSE_ADD_ORGANIZATION_MODAL':
            return {...state, showAddModal: false}
        default:
            return state;
    }
}