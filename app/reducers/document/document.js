import React from 'react';
import * as is from 'is_js';

const initialState = {
  table: {
    columns: [
      {
          title: 'Внутр №',
          dataIndex: 'doc_number',
          render: (text, record) => <a onClick={() => console.log(record.doc_number)}>{text}</a>
      },
      {
          title: 'Название',
          dataIndex: 'title',
          render: (text, record) => <a onClick={() => console.log(record.eventRecordNumber)}>{text}</a>
      },
      {
          title: 'Дата создания',
          dataIndex: 'date_created'
      },
      {
          title: 'Дата подтверждения',
          dataIndex: 'date_submited'
      },
      {
          title: 'Тип',
          dataIndex: 'type'
      },
      {
          title: 'Формат',
          dataIndex: 'format'
      },
    ],
    data: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    filters: {},
    sorter: {},

  },
  form: {
    schema: {},
    uiSchema: {},
    data: {},
  },
  modal: {
    visible: false,
  }
};

export default function organizationForm(state = initialState, action = {type: null}) {
    switch (action.type) {
        case 'DOCUMENTS_LIST_LOADED':
            return {...state,
              table: {
                ...state.table,
                data: action.payload.documents
              }}
        case 'CLOSE_DOCUMENTS_MODAL':
          return {...state, modal: {...state.modal, visible: false}}
        case 'OPEN_ADD_DOCUMENT_MODAL':
          return {...state, modal: {...state.modal, visible: true}}
        case 'DOCUMENT_FORM_STRUCTURE_LOADED':
          return {...state, form: {...state.form, schema: action.payload.form.schema, uiSchema: action.payload.form.uiSchema, data: {}}}
        default:
            return state;
    }
}
