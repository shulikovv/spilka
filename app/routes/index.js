import React from 'react';
import {Route, IndexRoute, Redirect} from 'react-router';
import {App, Login} from '../containers';
import {
    HomeView,
    Signin,
    Forgot,
    UserProfile,
    Events,
    Persons,
    Organizations,
    Organizations2,
    OrganizationsAlt2,
    OrganizationsAlt3,
    organizationView,
    Documents,
    Document,
    Dashboard,
    Documentation,
    eventsEdit,
    eventsView,
    noLang,
    Thesauri,
    Thesaurus,
    Users,
    Roles,
    Permissions,
    PersonEdit,
    PersonAdd,
    PersonView,
    UsersList,
    Vechicles,
    AddVechcle,
    Groups,
    Tolerance,
    OrganizationAddForm,
    OrganizationEditForm,
    PersonEditNew,
    EventsAddAct,
} from '../views';

export default(
    <Route>
        {/*<Redirect from="/" to="/events" />*/}
        <Route path='/' component={App}>
            <IndexRoute component={Dashboard}/>
            <Route path="user/profile" component={UserProfile}></Route>
            <Route path="dashboard" component={Dashboard}></Route>
            <Route authorize={['admin']} path="events" component={Events}></Route>
            <Route path="events/add" component={eventsEdit}></Route>
            <Route path="events/edit/:userID" component={eventsEdit}></Route>
            <Route path="events/view/:eventID" component={eventsView}></Route>

            <Route path="persons" component={Persons}>
            </Route>
            <Route path="editpersontmp" component={PersonEditNew}></Route>
            <Route path="addperson" component={PersonAdd}></Route>
            <Route path="editperson/:userID" component={PersonEditNew}></Route>
            <Route path="viewperson/:userID" component={PersonView}></Route>
            <Route path="organizations" component={Organizations}></Route>
            <Route path="organizations2" component={Organizations2}></Route>
            <Route path="organizations3" component={OrganizationsAlt3}></Route>
            <Route path="organizations/add" component={OrganizationAddForm}></Route>
            <Route path="organizations/view/:orgID" component={OrganizationEditForm}></Route>
            <Route path="organizations/edit/:orgID" component={OrganizationEditForm}></Route>
            <Route path="vechicles" component={Vechicles}></Route>
            <Route path="vechicles/add" component={AddVechcle}></Route>
            <Route path="documents" component={Documents}></Route>
            <Route path="document" component={Document}></Route>
            <Route path="events/add-act" component={EventsAddAct}></Route>

            <Route path="admin">
                <Route path="thesauri" component={Thesauri}></Route>
                <Route path="thesaurus/:thesaurusID" component={Thesaurus}></Route>
                <Route path="user" component={Users}>
                    <Route path="users" component={UsersList}></Route>
                    <Route path="roles" component={Roles}></Route>
                    <Route path="groups" component={Groups}></Route>
                    <Route path="tolerance" component={Tolerance}></Route>
                    <Route path="permissions" component={Permissions}></Route>
                </Route>
                <Route path="/admin/language" component={HomeView}></Route>
                <Route path="/admin/system-config" component={HomeView}></Route>
                <Route path="/admin/printer-config" component={HomeView}></Route>
                <Route path="/admin/dashboard-config" component={HomeView}></Route>
            </Route>
            <Route path='dictionaries'>
                <Route path='models' component={HomeView}></Route>
            </Route>
            <Route path='documentation' component={Documentation}/>
            <Route path='nolang' component={noLang}/>
            <Route path='true'/>
        </Route>
        <Route path='login' component={Login}>
            <IndexRoute component={Signin}/>
            <Route path='forgot' component={Forgot}/>
        </Route>
        <Route path='/**' component={App}></Route>
    </Route>
);
