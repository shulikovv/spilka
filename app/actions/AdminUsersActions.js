import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON } from '../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

var mock = new MockAdapter(axios);

mock.onGet('/api/userslist').reply(200, {
    users: [
        { 
            id: 1, 
            username: 'admin',
            email: 'admin@mail.com'
        },
        { 
            id: 2, 
            username: 'tester',
            email: 'tester@mail.com'
        },
    ]
}).onAny().passThrough();

export function loadUsersList() {
   return function(dispatch) {
       dispatch(loadingUsersProgress());
       axios.get('/api/user/list', {
           headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
       })
           .then(function (response) {
               console.log('loading user true ->',response);
               //let user = jwtDecode(response.data);
               //localStorage.setItem('token',response.data);
               dispatch(loadingUsersSuccess({users:response.data}));
               //dispatch(push('/'));
           })
           .catch(function (error) {
               console.log('....................',error);
           });
   }
}

export function loadingUsersProgress(){
    return {
        type: 'USERS_LIST_START_LOADING'
    }
}

export function loadingUsersSuccess(response) {
    
    return {
        type: 'USERS_LIST_LOADED',
        payload: {
            user_list: response.users,
        }
    }
}

export function addUser() {
    return {
        type: 'ADD_USER_SHOW_MODAL',
        payload: {
        }
    }
}


export function viewUser(userId) {
    return {
        type: 'VIEW_USER_SHOW_MODAL',
        payload: {
            id: userId
        }
    }
}

export function editUser(userId) {
    return {
        type: 'EDIT_USER_SHOW_MODAL',
        payload: {
            id: userId
        }
    }
}

export function closeEditUserModal() {
    return {
        type: 'EDIT_USER_CLOSE_MODAL'
    }
}

export function closeViewUserModal() {
    return {
        type: 'VIEW_USER_CLOSE_MODAL'
    }
}

export function closeAddUserModal() {
    return {
        type: 'ADD_USER_CLOSE_MODAL'
    }
}

export function changeDataInUserList(formData) {
    return {
        type: 'CHANGE_DATA_IN_USER_LIST',
        payload: {
            userData: formData
        }
    }
}

export function submitEditUserModal(formData) {
    return function(dispatch) {
        delete(formData.password2);
        if (!formData.password) delete(formData.password);

         axios.put('/api/user/'+formData.id, 
        qs.stringify(formData), {
           headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then(function (response) {
            dispatch(changeDataInUserList(formData));
            dispatch(closeEditUserModal())
        })
        .catch(function (error) {
            console.log('....................',error);
        });
    }
}



export function addDataToUserList(formData) {
    return {
        type: 'ADD_DATA_TO_USER_LIST',
        payload: {
            userData: formData
        }
    }
}


export function submitAddUserModal(formData) {
    return function(dispatch) {
        console.log('submitAddUserModal')
        axios.post('/api/user', 
        qs.stringify(formData), {
           headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then(function (response) {
            dispatch(addDataToUserList(formData));
            dispatch(closeAddUserModal())
        })
        .catch(function (error) {
            console.log('....................',error);
        });
        
    }
}



export function deleteUser(userID) {
    return function(dispatch) {
        axios.delete('/api/user/'+userID,{
           headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then(function (response) {
            dispatch(loadUsersList());
        })
        .catch(function (error) {
            console.log('....................',error);
        });

    }
}