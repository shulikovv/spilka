import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON, getThesaurusTree, preparePropToForm } from '../../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

var mock = new MockAdapter(axios);

mock
/*.onGet('/api/organization/list').reply(200, 
  [
     {
        organization_id: 1,
        organization_parent: 0,
        organization_has_children: 0,
        organization_title: 'Organization 1',
        organization_abb: 'O1',
        organization_description: 'Any text',
        organization_features: 'Any text'
     }, 
  ]
)*/
.onAny().passThrough();

export function getErrorWhenAdd(errors) {
    return (dispatch) => {
        console.log(errors);
        dispatch({
            type: 'ERROR_ADD_PERSON',
            payload: {
                errors: errors
            }
        })
    }
}

export function personFormSavedOk() {
    return (dispatch) => {
        console.log('personFormSavedOk');
    }
}

export function personFormSubmit(formData={}) {
    return (dispatch) => {
        //console.log('personFormSubmit in Add ->', formData);
        axios.post('/api/person', qs.stringify(formData), {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((resp)=>{
            console.log('resp',resp);
            if (resp.data.status == 'ok') {
                dispatch(personFormSavedOk());
                dispatch(push('/persons'));
            } else if (resp.data.status == 'error') {
                console.log(resp.data.errors);
                dispatch(getErrorWhenAdd(resp.data.errors))
            }
        })
    }
}

export function personFormReseived(formStructure) {
    return {
        type: 'PERSON_FORM_STRUCTURE_LOADED',
        payload: {
            formStructure: formStructure
        }
    }
}

export function getPersonForm($Id='') {
    if ($Id !== '') {
        $Id = '/'+$Id;
    }
    return (dispatch) => {
        axios.get('/api/person/get-person-form'+$Id, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((response)=>{
            console.log('Form Structure --->', response);
            let _formSchema = response.data.schema;
            let schema = preparePropToForm(_formSchema.properties);
            let tmpForm = {
                schema: {
                    type: "object",
                    properties: {...schema.schema.properties}
                },
                uiSchema: {
                    //"ui:readonly": true, 
                    ...schema.uiSchema
                },
                data: {
                }                
            }
            dispatch(personFormReseived(tmpForm));
            console.log('}}}}}}-------->>>>',tmpForm);
        })
    }
}
