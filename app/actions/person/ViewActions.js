import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON, getThesaurusTree, preparePropToForm } from '../../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

export function personViewFormReseived(formStructure) {
    return {
        type: 'PERSON_VIEW_FORM_STRUCTURE_LOADED',
        payload: {
            formStructure: formStructure
        }
    }
}

export function editPerson(person_id) {
    return function(dispatch) {
        dispatch(push('/editperson/'+person_id));
    }
 }

 export function personViewAFormReseived(formStructure) {
    return {
        type: 'PERSON_VIEW_ADDRESS_FORM_STRUCTURE_LOADED',
        payload: {
            formStructure: formStructure
        }
    }
}

export function personViewBioFormReseived(formStructure) {
    return {
        type: 'PERSON_VIEW_BIOGRAPHIC_FORM_STRUCTURE_LOADED',
        payload: {
            formStructure: formStructure
        }
    }
}


export function getPersonViewAddressForm() {
    return (dispatch) => {
        axios.get('/api/person/get-person-aform', {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((response)=>{
            console.log('Form Structure --->', response);
            let _formSchema = response.data.schema;
            let schema = preparePropToForm(_formSchema.properties);
            let formData = response.data.data;
            if (is.object(formData) && is.not.array(formData)) {
                for (let rowKey in formData) {
                    if (formData[rowKey]===null) {
                        delete(formData[rowKey]);
                    }
                }
            } else {
                formData = {}
            }
            let tmpForm = {
                schema: {
                    type: "object",
                    required: _formSchema.required || [],
                    properties: {...schema.schema.properties}
                },
                uiSchema: {
                    //"ui:readonly": true, 
                    ...schema.uiSchema
                },
                data: formData           
            }
            dispatch(personViewAFormReseived(tmpForm));
            console.log('}}}}}}-------->>>>',tmpForm);
        })
    }    
}

export function getPersonViewBioForm() {
    return (dispatch) => {
        axios.get('/api/person/get-person-bioform', {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((response)=>{
            console.log('Form Structure --->', response);
            let _formSchema = response.data.schema;
            let schema = preparePropToForm(_formSchema.properties);
            let formData = response.data.data;
            if (is.object(formData) && is.not.array(formData)) {
                for (let rowKey in formData) {
                    if (formData[rowKey]===null) {
                        delete(formData[rowKey]);
                    }
                }
            } else {
                formData = {}
            }
            let tmpForm = {
                schema: {
                    type: "object",
                    required: _formSchema.required || [],
                    properties: {...schema.schema.properties}
                },
                uiSchema: {
                    //"ui:readonly": true, 
                    ...schema.uiSchema
                },
                data: formData           
            }
            dispatch(personViewBioFormReseived(tmpForm));
            console.log('}}}}}}-------->>>>',tmpForm);
        })
    }    
}

export function getPersonViewForm($Id='') {
    if ($Id !== '') {
        $Id = '/'+$Id;
    }
    return (dispatch) => {
        axios.get('/api/person/get-person-form'+$Id, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((response)=>{
            console.log('Form Structure --->', response);
            let _formSchema = response.data.schema;
            let schema = preparePropToForm(_formSchema.properties);
            let formData = response.data.data;
            if (is.object(formData) && is.not.array(formData)) {
                for (let rowKey in formData) {
                    if (formData[rowKey]===null) {
                        delete(formData[rowKey]);
                    }
                }
            } else {
                formData = {}
            }
            let tmpForm = {
                schema: {
                    type: "object",
                    properties: {...schema.schema.properties}
                },
                uiSchema: {
                    //"ui:readonly": true, 
                    ...schema.uiSchema
                },
                data: formData           
            }
            dispatch(personViewFormReseived(tmpForm));
            console.log('}}}}}}-------->>>>',tmpForm);
        })
    }
}

export function addAddressFormSubmit (formData, personId) {
    return dispatch => {
        axios.put('/api/person/add-person-address'+(personId?'/':'')+personId, qs.stringify({
            ...formData,
            person_id: personId
            } ),{
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
        })
        .then((resp)=>{
            console.log(formData, personId);
            dispatch(closeAddAddressModal())
            dispatch(loadAddresses(personId));
            })
        .catch(()=>{
            console.log(formData, personId);
        })
    }
}

export function editAddressFormSubmit (formData, personId) {
    return dispatch => {
        for (let key in formData) {
            if (key == 'address_type_text' || key == 'country_text') {
                delete(formData[key]);
            }
        }

        axios.post('/api/person/edit-person-address'+(personId?'/':'')+personId, qs.stringify({
            ...formData,
            person_id: personId
            } ),{
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
        })
        .then((resp)=>{
            console.log(formData, personId);
            dispatch(closeEditAddressModal())
            dispatch(loadAddresses(personId));
            })
        .catch(()=>{
            console.log(formData, personId);
        })
    }
}

export function addBioFormSubmit (formData, personId) {
    return dispatch => {
        axios.put('/api/person/add-person-biodetail'+(personId?'/':'')+personId, qs.stringify({
            ...formData,
            //person_id: personId
            } ),{
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
        })
        .then((resp)=>{
            console.log(formData, personId);
            dispatch(closeAddBioModal())
            dispatch(loadBio(personId));
            })
        .catch(()=>{
            console.log(formData, personId);
        })
    }
}

export function editBioFormSubmit (formData, personId) {
    return dispatch => {
        axios.post('/api/person/edit-person-biodetail'+(personId?'/':'')+personId, qs.stringify({
            ...formData,
            //person_id: personId
            } ),{
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
        })
        .then((resp)=>{
            console.log(formData, personId);
            dispatch(closeAddBioModal())
            dispatch(loadBio(personId));
            })
        .catch(()=>{
            console.log(formData, personId);
        })
    }
}

export function deleteBioRecord(record, person_id = 0) {
    return dispatch => {
        axios.delete('/api/person/biodetail/'+record.biographic_details_id,{
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
        })
        .then((response)=>{
            dispatch(loadBio(person_id));
        })
    }
}

export function deleteAddressRecord(record, person_id = 0) {
    return dispatch => {
        axios.delete('/api/person/address/'+record.address_id,{
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
        })
        .then((response)=>{
            dispatch(loadAddresses(person_id));
        })
    }
}
export function closeAddAddressModal () {
    return {
        type: 'CLOSE_ADD_ADDRESS_MODAL'
    }
}

export function closeAddBioModal () {
    return {
        type: 'CLOSE_ADD_BIO_MODAL'
    }
}
export function closeViewAddressModal () {
    return {
        type: 'CLOSE_VIEW_ADDRESS_MODAL'
    }
}
export function closeEditAddressModal () {
    return {
        type: 'CLOSE_EDIT_ADDRESS_MODAL'
    }
}

export function openAddAddressModal(personId) {
    return {
        type: 'OPEN_ADD_ADDRESS_MODAL',
        payload: {
            personId: personId
        }
    }
}

export function openAddBioModal(personId) {
    return {
        type: 'OPEN_ADD_BIO_MODAL',
        payload: {
            personId: personId
        }
    }
}

export function loadBio(personId) {
    return dispatch => {
        axios.get('/api/person/get-person-bio'+(personId?'/':'')+personId, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
        })
        .then((resp)=>{
            console.log(personId, resp.data);
            dispatch({
                type: 'PERSON_BIO_DATA_LOADED',
                payload: {
                    bio: resp.data.data
                }
            })
            //dispatch(closeAddAddressModal())
            })
        .catch(()=>{
            console.log( personId);
        })
    }
}

export function viewPersonBiographic(personBio) {
    return dispatch => {
        console.log(personBio);
        if (is.not.boolean(personBio.confidentiality)) {
            personBio.confidentiality = (parseInt(personBio.confidentiality) == 1)
        }
        if (is.null(personBio.remarks)) {
            personBio.remarks = '';
        }
        dispatch({
            type: 'OPEN_VIEW_BIO_MODAL',
            payload: {
                personBio: personBio
            }
        })
    }
}
export function editPersonBiographic(personBio) {
    return dispatch => {
        console.log(personBio);
        if (is.not.boolean(personBio.confidentiality)) {
            personBio.confidentiality = (parseInt(personBio.confidentiality) == 1)
        }
        for (let key in personBio) {
            if (is.null(personBio[key])) {
                delete(personBio[key]);
            }

        }
        if (is.null(personBio.remarks)) {
            personBio.remarks = '';
        }
        dispatch({
            type: 'OPEN_EDIT_BIO_MODAL',
            payload: {
                personBio: personBio
            }
        })
    }
}

export function loadAddresses (personId) {
    return dispatch => {
        axios.get('/api/person/get-person-addresses'+(personId?'/':'')+personId, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
        })
        .then((resp)=>{
            console.log(personId, resp.data);
            dispatch({
                type: 'PERSON_ADDRESSES_DATA_LOADED',
                payload: {
                    addresses: resp.data.data
                }
            })
            //dispatch(closeAddAddressModal())
            })
        .catch(()=>{
            console.log( personId);
        })
    }
}

export function viewPersonAddress(personAddress) {
    return dispatch => {
        console.log(personAddress);
        //if (is.not.boolean(personAddress.confidentiality)) {
        //    personAddress.confidentiality = (parseInt(personAddress.confidentiality) == 1)
        //}
        //if (is.null(personAddress.remarks)) {
        //    personAddress.remarks = '';
        //}
        dispatch({
            type: 'OPEN_VIEW_ADDRESS_MODAL',
            payload: {
                personAddress: personAddress
            }
        })
    }
}

export function editPersonAddress(personAddress) {
    return dispatch => {
        console.log(personAddress);
        //if (is.not.boolean(personAddress.confidentiality)) {
        //    personAddress.confidentiality = (parseInt(personAddress.confidentiality) == 1)
        //}
        for (let key in personAddress) {
            if (is.null(personAddress[key])) {
                delete(personAddress[key]);
            }
        }
        if (is.null(personAddress.remarks)) {
            personAddress.remarks = '';
        }
        dispatch({
            type: 'OPEN_EDIT_ADDRESS_MODAL',
            payload: {
                personAddress: personAddress
            }
        })
    }
}

export function closeAddDocModal() {
    return {
        type: 'CLOSE_ADD_PERSON_DOC_MODAL'
    }
}

export function closeSelDocModal() {
    return {
        type: 'CLOSE_SEL_PERSON_DOC_MODAL'
    }
}

export function openAddDocModal() {
    return {
        type: 'OPEN_ADD_PERSON_DOC_MODAL'
    }
}

export function openSelDocModal() {
    return {
        type: 'OPEN_SEL_PERSON_DOC_MODAL'
    }
}