import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON, getThesaurusTree } from '../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';
//import {getThesaurusTree} from '../utils/index';

export function loadThesauriList() {
   return function(dispatch) {
       dispatch(loadingThesauriProgress());
       axios.get('/api/thesauri', {
           headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
       })
           .then(function (response) {
               console.log('loading thesauri true ->',response);
               dispatch(loadingThesauriSuccess(response.data));
           })
           .catch(function (error) {
               console.log('....................',error);
           });
   }
}

export function loadingThesauriProgress()
{
    return {
        type: 'THESAURI_LIST_START_LOADING'
    }
}

export function loadingThesauriSuccess(thesauri)
{
    return {
        type: 'THESAURI_LIST_LOADED',
        payload: {
            list: thesauri,
        }
    }
}

export function thesaurusView(thesaurusID) {
    return (dispatch) => dispatch(push('/admin/thesaurus/'+thesaurusID));
}

export function loadingThesaurusProgress()
{
    return {
        type: 'THESAURUS_START_LOADING'
    }
}

export function loadingThesaurusSuccess(thesaurus)
{
    let thesauriTree = getThesaurusTree(thesaurus);
    console.log('tree ----------------------->',thesauriTree);
    return {
        type: 'THESAURUS_LOADED',
        payload: {
            list: thesaurus,
            tree: thesauriTree,
        }
    }
}

export function loadThesaurus(thesaurusID) {
    console.log('try load thesaurus ', thesaurusID);
   return function(dispatch) {
       dispatch(loadingThesaurusProgress());
       axios.get('/api/thesaurus/'+thesaurusID, {
           headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
       })
           .then(function (response) {
               console.log('loading thesauri true ->',response);
               dispatch(loadingThesaurusSuccess(response.data));
           })
           .catch(function (error) {
               console.log('....................',error);
           });
   }
}
