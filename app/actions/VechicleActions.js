import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON } from '../utils';
import is from 'is_js';

export function vechicleEdit(eventId) {
   return function(dispatch) {
       dispatch(push('/vechicles/edit/'+eventId));
   }
}

export function vechicleView(orgId) {
   return function(dispatch) {
       dispatch(push('/vechicles/view/'+orgId));
   }
}

export function vechicleAdd() {
  return (dispatch) => dispatch(push('/vechicles/add'))
}

export function vechicleDummyAction(token) {
  return {
    type: LOGIN_USER_SUCCESS,
    payload: {
      token: token
    }
  }
}