import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON, getThesaurusTree, preparePropToForm } from '../../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

var mock = new MockAdapter(axios);

mock
/*
.onGet('/api/document/get-documents-list').reply(200,
  {
    status: 'ok',
    data: [
      {
          doc_id: 0,
          doc_number: '',
          title: 'Test document title',
          creator:'somebody',
          description:'hdfhdhskdfhfksd',
          date_created:'2010-10-06',
          date_submited: '2011-11-02',
          format: '',
          type: '',
          language: '',
          extension: '',
          real_file_name:''
      },
    ]
  }
)
*/
    .onAny().passThrough();

export function getDocumentsList(personId) {
  return dispatch => {
    axios.get('/api/document/get-documents-list'+(is.existy(personId)?'/'+personId:''), {
        headers: {
            Authorization: 'Bearer '+ localStorage.getItem('token')
        }
    })
    .then((result)=>{
        dispatch({
          type: 'DOCUMENTS_LIST_LOADED',
          payload: {
            documents: result.data
          }
        })
    })
  }
}

export function getDocumentForm() {
  return dispatch => {
    axios.get('/api/document/get-document-form', {
        headers: {
            Authorization: 'Bearer '+ localStorage.getItem('token')
        }
    })
    .then((response)=>{
      console.log('doc Form Structure --->', response);
      let _formSchema = response.data.schema;
      let schema = preparePropToForm(_formSchema.properties);
      let formData = response.data.data || {};
      for (let rowKey in formData) {
          if (formData[rowKey]===null) {
              delete(formData[rowKey]);
          }
      }
      let tmpForm = {
          schema: {
              type: "object",
              properties: {...schema.schema.properties}
          },
          uiSchema: {
              //"ui:readonly": true,
              ...schema.uiSchema
          },
          data: formData
      }
      //dispatch(personFormReseived(tmpForm));
      dispatch({
        type: 'DOCUMENT_FORM_STRUCTURE_LOADED',
        payload: {
          form: tmpForm
        }
      })
      console.log('}}}}}}-------->>>>',tmpForm);
    })
  }
}

export function addDocument() {
  return {
    type: 'OPEN_ADD_DOCUMENT_MODAL'
  }
}

export function closeOpenedModal() {
   return {
     type: 'CLOSE_DOCUMENTS_MODAL'
   }
}
