import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON } from '../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

var mock = new MockAdapter(axios);

mock.onGet('/api/organization/list').reply(200, 
  [
     {
        organization_id: 1,
        organization_parent: 0,
        organization_has_children: 0,
        organization_title: 'Organization 1',
        organization_abb: 'O1',
        organization_description: 'Any text',
        organization_features: 'Any text'
     }, 
     {
        organization_id: 3,
        organization_parent: 0,
        organization_has_children: 0,
        organization_title: 'Organization 2',
        organization_abb: 'O2',
        organization_description: 'Any text too',
        organization_features: 'Any text too'
     }, 
  ]
).onAny().passThrough();

export function orgGetList() {
  return (dispatch) => {
      axios.get('/api/organization/list', {
           headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
       })
      .then(function (response) {
          console.log('loading user true ->',response);
          //let user = jwtDecode(response.data);
          //localStorage.setItem('token',response.data);
          //dispatch(loadingUsersSuccess({users:response.data}));
          //dispatch(push('/'));
      })
      .catch(function (error) {
          console.log('....................',error);
      });
  }
}

function orgTreeLoaded(tree) {
    return {
        type: 'ORG1_TREE_LOADED',
        payload: {
            org_tree: tree
        }
    }
}

export function orgGetTree() {
  return (dispatch) => {
      axios.get('/api/organization/tree', {
           headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
       })
      .then(function (response) {
          console.log('loading org tree true ->',response);
          dispatch(orgTreeLoaded(response.data));
          dispatch(orgGetOrganization(response.data[0].organization_id));
          //let user = jwtDecode(response.data);
          //localStorage.setItem('token',response.data);
          //dispatch(loadingUsersSuccess({users:response.data}));
          //dispatch(push('/'));
      })
      .catch(function (error) {
          console.log('....................',error);
      });
  }
}


export function orgGetChildrenList(parentID) {
  return (dispatch) => {
      axios.get('/api/organization/children-list/'+parentID, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
            params: {
              'id': parentID
            },
            paramsSerializer: function(params) {
              return qs.stringify(params, {arrayFormat: 'brackets'})
            },
       })
      .then(function (response) {
          console.log('loading orgGetChildrenList true ->',response);
          //let user = jwtDecode(response.data);
          //localStorage.setItem('token',response.data);
          //dispatch(loadingUsersSuccess({users:response.data}));
          //dispatch(push('/'));
      })
      .catch(function (error) {
          console.log('....................',error);
      });
  }
}

export function orgGetChildrenTree(parentID) {
  return (dispatch) => {
      axios.get('/api/organization/children-tree/'+parentID, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
            params: {
              'id': parentID
            },
            paramsSerializer: function(params) {
              return qs.stringify(params, {arrayFormat: 'brackets'})
            },
       })
      .then(function (response) {
          console.log('loading orgGetChildrenTree true ->',response);
          //let user = jwtDecode(response.data);
          //localStorage.setItem('token',response.data);
          //dispatch(loadingUsersSuccess({users:response.data}));
          //dispatch(push('/'));
      })
      .catch(function (error) {
          console.log('....................',error);
      });
  }
}


export function orgEdit(eventId) {
   return function(dispatch) {
       dispatch(push('/events/edit/'+eventId));
   }
}

export function orgView(orgId) {
   return function(dispatch) {
      axios.get('/api/organization/'+orgId, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
            params: {
              'id': orgId
            },
            paramsSerializer: function(params) {
              return qs.stringify(params, {arrayFormat: 'brackets'})
            },
       })
      .then(function (response) {
          console.log('loading orgGetChildrenTree true ->',response);
          //let user = jwtDecode(response.data);
          //localStorage.setItem('token',response.data);
          //dispatch(loadingUsersSuccess({users:response.data}));
          //dispatch(push('/'));
          //dispatch(push('/organizations/view/'+orgId));
      })
      .catch(function (error) {
          console.log('....................',error);
      });

   }
}

function orgOrganizationDataLoaded(organization) {
    return {
        type: 'ORG1_ORGANIZATION_DATA_LOADED',
        payload: {
            org : organization
        }
    }
}

export function orgGetOrganization(orgID) {
    return (dispatch) => {
        axios.get('/api/organization/get-organization/'+orgID, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            },
            params: {
              'id': orgID
            },
            paramsSerializer: function(params) {
              return qs.stringify(params, {arrayFormat: 'brackets'})
            },
        })
        .then((response)=>{
            console.log('orgGetOrganization -> ',response);
            dispatch(orgOrganizationDataLoaded(response.data));
        })
      .catch(function (error) {
          console.log('....................',error);
      });
    }
  }

export function orgAdd() {
  return (dispatch) => dispatch(push('/events/add'))
}

export function orgDummyAction(token) {
  return {
    type: LOGIN_USER_SUCCESS,
    payload: {
      token: token
    }
  }
}

export function orgAddOrganization() {
  return {
    type: 'ORG1_ADD_ORGANIZATION_MODAL',
  }
}

export function orgCloseAddOrganizationModal() {
  return {
    type: 'ORG1_CLOSE_ADD_ORGANIZATION_MODAL',
  }
}
