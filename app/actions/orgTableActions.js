import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON } from '../utils';
//import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

var mock = new MockAdapter(axios);

mock
/*.onGet('/api/organization/list').reply(200, 
  [
     {
        organization_id: 1,
        organization_parent: 0,
        organization_has_children: 0,
        organization_title: 'Organization 1',
        organization_abb: 'O1',
        organization_description: 'Any text',
        organization_features: 'Any text'
     }, 
     {
        organization_id: 3,
        organization_parent: 0,
        organization_has_children: 0,
        organization_title: 'Organization 2',
        organization_abb: 'O2',
        organization_description: 'Any text too',
        organization_features: 'Any text too'
     }, 
  ]
)*/
.onAny().passThrough();

export function getOrgTable() {
    return (dispatch) => {
        axios.get('/api/organization/short-tree', {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then(function (response) {
            console.log(response.data);
            //dispatch(orgTreeLoaded(response.data));
            //dispatch(orgGetOrganization(response.data[0].organization_id));
            //let user = jwtDecode(response.data);
            //localStorage.setItem('token',response.data);
            //dispatch(loadingUsersSuccess({users:response.data}));
            //dispatch(push('/'));
        })
        .catch(function (error) {
            console.log('....................',error);
        });
    }
}