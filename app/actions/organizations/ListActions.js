import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON, getThesaurusTree, preparePropToForm } from '../../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

var mock = new MockAdapter(axios);

mock
/*.onGet('/api/organization/list').reply(200, 
  [
     {
        organization_id: 1,
        organization_parent: 0,
        organization_has_children: 0,
        organization_title: 'Organization 1',
        organization_abb: 'O1',
        organization_description: 'Any text',
        organization_features: 'Any text'
     }, 
     {
        organization_id: 3,
        organization_parent: 0,
        organization_has_children: 0,
        organization_title: 'Organization 2',
        organization_abb: 'O2',
        organization_description: 'Any text too',
        organization_features: 'Any text too'
     }, 
  ]
)*/
.onAny().passThrough();

export function Org3TreeLoaded(tree) {
    return {
        type: 'ORGLIST_FORM_TREE_LOADED',
        payload: {
            tree: tree
        }
    }
}

export function getOrgTable() {
    return (dispatch) => {
        axios.get('/api/organization/short-tree', {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then(function (response) {
            console.log('1-stage --->>>',response.data);
            //dispatch(orgTreeLoaded(response.data));
            //dispatch(orgGetOrganization(response.data[0].organization_id));
            //let user = jwtDecode(localStorage.getItem('token'));
            //console.log('user-->',user);
            dispatch(Org3TreeLoaded(response.data));
            //dispatch(getOrgForm());
            return(response)
        })
        .then((response)=>{
            console.log('2-stage --->>>', response);
        })
        .catch(function (error) {
            console.log('....................',error);
        });
    }
}

export function orgFormReseived(formStructure) {
    return {
        type: 'ORGLIST_FORM_STRUCTURE_LOADED',
        payload: {
            formStructure: formStructure
        }
    }
}

export function setCurrentOrg(org) {
    return {
        type: 'ORGLIST_SET_CURRENT_SEL_ORG',
        payload: {
            org: org
        }
    }
}

export function getOrgFormData(form, rowId) {
    return (dispatch) => {
        console.log('getOrgFormData --->',form, rowId );
        axios.get('/api/organization/form-schema', {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        
    }
}

export function getOrgForm($Id='') {
    if ($Id !== '') {
        $Id = '/'+$Id;
    }
    return (dispatch) => {
        axios.get('/api/organization/form-schema'+$Id, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((response)=>{
            console.log('Form Structure --->', response);
            let _formData = response.data;
            let formData = {
                type: _formData.type,
            }
            if (_formData.type == 'tabs') {
                formData.tabs = [];
                let tmpTab;
                _formData.tabs.forEach((item, index)=>{
                    let item_form_data = {};
                    if (is.not.array(item.form.data) && is.object(item.form.data)) {
                        item_form_data = item.form.data;
                    }
                    if (item.type == 'form') {
                        let schema = preparePropToForm(item.form.schema.properties);
                        
                        tmpTab = {
                            type : 'form',
                            title: item.title,
                            id: item.id,
                            form : {
                                schema: {
                                    type: "object",
                                    properties: {...schema.schema.properties}
                                },
                                uiSchema: {
                                    "ui:readonly": true, ...schema.uiSchema
                                },
                                data:item_form_data
                            }
                        }
                       
                    } else if (item.type == 'table') {
                        tmpTab = {
                            type: 'table',
                            title: item.title,
                            id: item.id,
                            form: {
                                key: item.form.key,
                                columns: item.form.columns,
                                data: item.form.data
                            }
                        }
                    }
                    formData.tabs.push(tmpTab);
                });
            }
            dispatch(orgFormReseived(formData));
            console.log('}}}}}}-------->>>>',formData);
        })
    }
}

export function addNewOrganization() {
    return (dispatch) => {
        dispatch(push('organizations/add'));
    }
}

export function editOrganization(Id='') {
    return (dispatch) => {

        dispatch(push('organizations/edit/'+Id));
    }
}