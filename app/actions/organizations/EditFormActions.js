import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON, getThesaurusTree, preparePropToForm } from '../../utils';
//import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

var mock = new MockAdapter(axios);

mock
/*.onGet('/api/organization/list').reply(200, 
  [
  ]
)*/
.onAny().passThrough();


export function orgEditFormReseived(formStructure) {
    return {
        type: 'ORGADD_FORM_STRUCTURE_LOADED',
        payload: {
            formStructure: formStructure
        }
    }
}

export function orgEditDataLoaded(formData) {
    return {
        type: 'ORGADD_FORM_DATA_LOADED',
        payload: {
            data: formData
        }
    }
}

export function orgEditLodaData(orgID) {
    return (dispatch) => {
        axios.get('/api/organization/edit-form-data/'+orgID, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((response)=>{
            console.log('Form Data --->', response);
            dispatch(orgEditDataLoaded(response.data));
        })
    }
}

export function getOrgEditForm(orgID) {
    return (dispatch) => {
        axios.get('/api/organization/add-form-schema', {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((response)=>{
            console.log('Form Structure --->', response);
            let _formSchema = response.data;
            let schema = preparePropToForm(_formSchema.properties);
            let tmpForm = {
                schema: {
                    type: "object",
                    properties: {...schema.schema.properties}
                },
                uiSchema: {
                    "ui:readonly": false, ...schema.uiSchema
                },
                data: {
                }                
            }

            dispatch(orgEditLodaData(orgID));
            dispatch(orgEditFormReseived(tmpForm));
            console.log('}}}}}}-------->>>>',tmpForm);
        })
    }
}

export function getErrorWhenEdit(errors) {
    return {
        type: 'ORGADD_SAVE_FORM_ERRORS',
        payload: {
            errors: errors
        }
    }
}

export function editOrgFormSavedOk() {
    return {
        type: 'ORGADD_SAVE_FORM_OK',
    }
}

export function submitEditOrgForm(formData) {
    return (dispatch) => {
        axios.put('/api/organization/'+formData.organization_id, qs.stringify(formData), {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((resp)=>{
            console.log('resp',resp);
            if (resp.data.status == 'ok') {
                dispatch(editOrgFormSavedOk());
                dispatch(push('organizations3'));
            } else if (resp.data.status == 'error') {
                console.log(resp.data.errors);
                dispatch(getErrorWhenEdit(resp.data.errors))
            }
        })
    }
}