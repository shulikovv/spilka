import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON, getThesaurusTree, preparePropToForm } from '../../utils';
//import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

var mock = new MockAdapter(axios);

mock
/*.onGet('/api/organization/list').reply(200, 
  [
  ]
)*/
.onAny().passThrough();


export function orgAddFormReseived(formStructure) {
    return {
        type: 'ORGADD_FORM_STRUCTURE_LOADED',
        payload: {
            formStructure: formStructure
        }
    }
}

export function getErrorWhenAdd(errors) {
    return {
        type: 'ORGADD_SAVE_FORM_ERRORS',
        payload: {
            errors: errors
        }
    }
}

export function addOrgFormSavedOk() {
    return {
        type: 'ORGADD_SAVE_FORM_OK',
    }
}

export function submitAddOrgForm(formData) {
    return (dispatch) => {
        axios.post('/api/organization', qs.stringify(formData), {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((resp)=>{
            console.log('resp',resp);
            if (resp.data.status == 'ok') {
                dispatch(addOrgFormSavedOk());
                dispatch(push('organizations3'));
            } else if (resp.data.status == 'error') {
                console.log(resp.data.errors);
                dispatch(getErrorWhenAdd(resp.data.errors))
            }
        })
    }
}

export function getOrgAddForm() {
    return (dispatch) => {
        axios.get('/api/organization/add-form-schema', {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((response)=>{
            console.log('Form Structure --->', response);
            let _formSchema = response.data;
            let schema = preparePropToForm(_formSchema.properties);
            let tmpForm = {
                schema: {
                    type: "object",
                    properties: {...schema.schema.properties}
                },
                uiSchema: {
                    //"ui:readonly": true, 
                    ...schema.uiSchema
                },
                data: {
                }                
            }
            dispatch(orgAddFormReseived(tmpForm));
            console.log('}}}}}}-------->>>>',tmpForm);
        })
    }
}