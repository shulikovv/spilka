import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON } from '../utils';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

export function viewTolerance(id) {
    return {
        type: 'VIEW_TOLERANCE_SHOW_MODAL',
        payload: {
            id: id
        }
    }
}

export function closeviewToleranceModal() {
    return {
        type: 'VIEW_TOLERANCE_CLOSE_MODAL'
    }
}

export function addTolerance() {
    return {
        type: 'ADD_TOLERANCE_SHOW_MODAL',
    }
}

export function closeaddToleranceModal() {
    return {
        type: 'ADD_TOLERANCE_CLOSE_MODAL'
    }
}

export function viewRole(id) {
    return {
        type: 'VIEW_ROLES_SHOW_MODAL',
        payload: {
            id: id
        }
    }
}

export function closeviewRoleModal() {
    return {
        type: 'VIEW_ROLES_CLOSE_MODAL'
    }
}

export function addRole() {
    return {
        type: 'ADD_ROLES_SHOW_MODAL',
    }
}

export function closeaddRoleModal() {
    return {
        type: 'ADD_ROLES_CLOSE_MODAL'
    }
}

export function viewGroup(id) {
    return {
        type: 'VIEW_GROUPS_SHOW_MODAL',
        payload: {
            id: id
        }
    }
}

export function closeviewGroupModal() {
    return {
        type: 'VIEW_GROUPS_CLOSE_MODAL'
    }
}

export function addGroup() {
    return {
        type: 'ADD_GROUPS_SHOW_MODAL',
    }
}

export function closeaddGroupModal() {
    return {
        type: 'ADD_GROUPS_CLOSE_MODAL'
    }
}

