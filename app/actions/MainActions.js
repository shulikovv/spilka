import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON } from '../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';

export function action1() {
   return function(dispatch) {
   }
}

export function action2(token) {
  return {
    type: LOGIN_USER_SUCCESS,
    payload: {
      token: token
    }
  }
}
