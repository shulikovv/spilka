import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON, getThesaurusTree, preparePropToForm } from '../../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

var mock = new MockAdapter(axios);

mock
/*.onGet('/api/organization/list').reply(200, 
  [
     {
        organization_id: 1,
        organization_parent: 0,
        organization_has_children: 0,
        organization_title: 'Organization 1',
        organization_abb: 'O1',
        organization_description: 'Any text',
        organization_features: 'Any text'
     }, 
  ]
)*/
.onAny().passThrough();

export function addVictim(returnpath) {
    return dispatch => {
        dispatch({
            type: 'SET_ADD_ACT_VICTIM_RETURN_PATH',
            payload: {
                returnpath: returnpath
            }
        })
        dispatch(push('/events/add-act'));
    }
}

export function ReturnTo(returnpath) {
    return dispatch => {
        dispatch(push(returnpath))
    }
}

export function selectPresonForAct(person_id = '') {
    if (person_id !== '') {
        person_id = '/'+person_id;
    }
    return dispatch => {
        console.log('from selectPresonForAct --> ', person_id)
        axios.get('/api/person/get-person-form'+person_id, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((response)=>{
            console.log('Form Structure --->', response);
            let _formSchema = response.data.schema;
            let schema = preparePropToForm(_formSchema.properties);
            let formData = response.data.data;
            for (let rowKey in formData) {
                if (formData[rowKey]===null) {
                    //formData[rowKey]= undefined;
                    delete(formData[rowKey]);
                }
            }
            let tmpForm = {
                schema: {
                    type: "object",
                    properties: {...schema.schema.properties}
                },
                uiSchema: {
                    //"ui:readonly": true, 
                    ...schema.uiSchema
                },
                data: formData          
            }
            dispatch({
                type: 'VICTIM_DATA_LOADED',
                payload: {
                    victimForm: tmpForm
                }
            });
            console.log('}}}}}}-------->>>>',tmpForm);
        })
    }
}

export function getEventForm($Id='') {
    if ($Id !== '') {
        $Id = '/'+$Id;
    }
    return (dispatch) => {
        axios.get('/api/event/get-event-form'+$Id, {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((response)=>{
            console.log('Form Structure --->', response);
            let _formSchema = response.data.schema;
            let schema = preparePropToForm(_formSchema.properties);
            let formData = response.data.data;
            for (let rowKey in formData) {
                if (formData[rowKey]===null) {
                    //formData[rowKey]= undefined;
                    delete(formData[rowKey]);
                }
            }
            let tmpForm = {
                schema: {
                    type: "object",
                    required: _formSchema.required || [],
                    properties: {...schema.schema.properties}
                },
                uiSchema: {
                    //"ui:readonly": true, 
                    ...schema.uiSchema
                },
                data: formData          
            }
            dispatch({
                type: 'EVENT_FORM_STRUCTURE_LOADED',
                payload: {
                    formStructure: tmpForm  
                }
            });
            console.log('}}}}}}-------->>>>',tmpForm);
        })
    }
}