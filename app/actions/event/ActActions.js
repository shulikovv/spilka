import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON, getThesaurusTree, preparePropToForm } from '../../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

var mock = new MockAdapter(axios);

mock
/*.onGet('/api/organization/list').reply(200, 
  [
     {
        organization_id: 1,
        organization_parent: 0,
        organization_has_children: 0,
        organization_title: 'Organization 1',
        organization_abb: 'O1',
        organization_description: 'Any text',
        organization_features: 'Any text'
     }, 
  ]
)*/
.onAny().passThrough();


export function getPersonListForAct(){
    return dispatch => {
        axios.get('/api/person/get-person-list-for-act', {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then(result=>{
            dispatch({
                type: 'PERSON_LIST_FOR_ACT_LOADED',
                payload: {
                    personlist: result.data
                }
            })
        })
    }
}