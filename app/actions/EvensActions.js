import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON } from '../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';

export function eventEdit(eventId) {
   return function(dispatch) {
       dispatch(push('/events/edit/'+eventId));
   }
}

export function eventView(eventId) {
   return function(dispatch) {
       dispatch(push('/events/view/'+eventId));
   }
}

export function addEvent() {
  return (dispatch) => dispatch(push('/events/add'))
}

export function action2event(token) {
  return {
    type: LOGIN_USER_SUCCESS,
    payload: {
      token: token
    }
  }
}
