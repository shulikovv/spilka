import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON } from '../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import jwt from 'jwt-simple';

export function AddPerson() {
   return function(dispatch) {
       dispatch(push('/addperson'));
   }
}

export function editPerson(person_id) {
   return function(dispatch) {
       dispatch(push('/editperson/'+person_id));
   }
}

export function viewPerson(person_id) {
   return function(dispatch) {
       dispatch(push('/viewperson/'+person_id));
   }
}

export function personListDataLoaded(list) {
    return {
        type: 'PERSON_LIST_DATA_LOADED',
        payload: {
            list: list
        }
    }
}

export function getPersonListData() {
    return (dispatch) => {
        axios.get('/api/person/list', {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then(function (response) {
            console.log('1-stage --->>>',response.data);
            dispatch(personListDataLoaded(response.data));
            //dispatch(orgGetOrganization(response.data[0].organization_id));
            //let user = jwtDecode(localStorage.getItem('token'));
            //console.log('user-->',user);
            //dispatch(Org3TreeLoaded(response.data));
            //dispatch(getOrgForm());
            return(response)
        })
        .then((response)=>{
            console.log('2-stage --->>>', response);
        })
        .catch(function (error) {
            console.log('....................',error);
        });
    }
}

export function listActions(record, type) {
    return (dispatch, context) => {
        console.log('Del-->', record, type, context);
        switch (type) {
            case 'delete':
            axios.delete('/api/person/'+record.person_id, {
                headers: {
                    Authorization: 'Bearer '+ localStorage.getItem('token')
                }
            })
            .then((resp)=>{
                console.log('resp',resp);
                if (resp.data.status == 'ok') {
                    dispatch(getPersonListData());
                    //dispatch(personFormSavedOk());
                    //dispatch(push('/persons'));
                } else if (resp.data.status == 'error') {
                    //console.log(resp.data.errors);
                    //dispatch(getErrorWhenAdd(resp.data.errors))
                }
            })
            break;
        }
    }
}
