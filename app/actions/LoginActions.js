import { push } from 'react-router-redux';
import { checkHttpStatus, parseJSON } from '../utils';
import jwtDecode from 'jwt-decode';
import is from 'is_js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import jwt from 'jwt-simple';
//import jwt from 'jsonwebtoken';
//import querystring from 'querystring';
import qs from 'qs';

var payload = { foo: 'bar' };
var secret = 'testing';

var mock = new MockAdapter(axios);

mock.onPost('/api/login1').reply(200, {
    users: [
        { id: 1, name: 'John Smith' }
    ]
}).onAny().passThrough();

export function loginUser(username, password) {
   return function(dispatch) {
       axios.post('/api/login', qs.stringify({username: username, password: password}))
           .then(function (response) {
               console.log('login true ->',response,jwtDecode(response.data));
               //let user = jwtDecode(response.data);
               localStorage.setItem('token',response.data);
               dispatch(loginUserSuccess(response.data));
               dispatch(push('/'));

           })
           .catch(function (error) {
               console.log('....................',error);
           });
   }
}

export function logoutUser() {
    localStorage.removeItem('token');
    return (dispatch) => {
        dispatch(push('/login'));
    }
}

export function isLogined() {
    return (dispatch,getState) => {
        let token = localStorage.getItem('token');
        console.log('GSU-->',getState().user);
        if (token !== null) {
           // dispatch(loginUserSuccess(token));
        } else {
            dispatch(RedirectToLogin());
        }
    }
}

export const loginUser2 = (username, password) => (
    (dispatch, getState) => {
        console.log('GSU-->',getState().user);
        var token = jwt.encode(payload, secret);
        console.log(jwtDecode(token));
        //console.log(jwt.sign({ foo: 'bar' }, 'shhhh1h', { expiresIn: 60}));
        axios.post('/api/jwt', {})
            .then(function (response) {
                console.log(',,,,,,,,,,,,,,,,,,,1',jwtDecode(response.data));
                axios.post('/api/jwt2', qs.stringify({token: jwt.encode(jwtDecode(response.data), secret)}))
                    .then((resp2)=>{
                        console.log('^^^^^^^^^^',resp2.data);
                    })
                    .catch((e)=>{
                        console.log('^^^error^^', e);
                    })
            })
            .catch(function (error) {
                console.log('....................1',error);
            });
    }
)


export function loginUserSuccess(token) {
    let user = jwtDecode(token);
    return {
        type: 'LOGIN_USER_SUCCESS',
        payload: {
            name: user.username,
            id: user.id,
            accessToken: user.accessToken
        }
    }

}

export function RedirectToLogin() {
    return (dispatch) => dispatch(push('/login'));
}

export function loginUserFailure(error) {
    return (dispatch) => dispatch(push('/login'));
}

export function action2e(token) {
  return {
    type: LOGIN_USER_SUCCESS,
    payload: {
      token: token
    }
  }
}
