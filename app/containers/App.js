import React from 'react';
import {Link} from 'react-router';
import is from 'is_js'

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../actions';
import { LocaleProvider } from 'antd';

import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import Settings from '../components/Settings';
import Footer from '../components/Footer';
//import enUS from 'antd/lib/locale-provider/en_US';
import ruRU from 'antd/lib/locale-provider/ru_RU';
import main from 'react-structured-filter';

import './../styles/app.css';


var routes = {
  'dictionaries' : [
    '/dictionaries/models',
    '/dictionaries/model-groups',
    '/dictionaries/site-groups',
    '/dictionaries/country-grups',
  ],
  'reports': [
    '/reports/rep1'
  ]
}




export class CoreLayout extends React.Component {
    componentWillMount() {
      console.log('app-there -->',this.props);
    }
    componentWillUpdate() {
        this.props.actions.isLogined();
        console.log('app- will update -->',this.props);
    }
    componentDidMount() {
      console.log('app-here -->',this.props);
        Main.init();
    }
    render () {
        return (
            <LocaleProvider locale={ruRU}>
              <div id="app" className="app-navbar-fixed app-sidebar-fixed app-footer-fixed">
                <div>
                    <Sidebar {...this.props}/>
                    <div className="app-content">
                        <Header location={this.props.location.pathname} actions={this.props.actions} user={this.props.user} />
                        <div className="main-content">
                            <div className="wrap-content container" id="container">
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                    <Footer/>
                    <Settings/>
                </div>
                <div id="for_modal">
                </div>
              </div>
            </LocaleProvider>
        );
    }
}

//console.log('state-->',state);

const mapStateToProps = (state) => ({
    user: state.user,
    userName: state.user.name
});

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(CoreLayout);
