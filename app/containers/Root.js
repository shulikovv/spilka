import React from 'react';
import {Provider} from 'react-redux';
import routes from '../routes';
//import {ReduxRouter} from 'redux-router';
import { Router, browserHistory  } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux'


export default class Root extends React.Component {

    constructor(props) {
        super(props);
    }

    render () {
        const history = syncHistoryWithStore(browserHistory, this.props.store);
        return (
            <Provider store={this.props.store}>
                <Router  history={history}>
                    {routes}
                </Router>
            </Provider>
        );
    }
}

