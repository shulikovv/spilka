import React from 'react';
import {Link} from 'react-router';
import is from 'is_js'

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../actions';


var routes = {
  'dictionaries' : [
    '/dictionaries/models',
    '/dictionaries/model-groups',
    '/dictionaries/site-groups',
    '/dictionaries/country-grups',
  ],
  'reports': [
    '/reports/rep1'
  ]
}




export class CoreLayout extends React.Component {
    componentWillMount() {
      //this.props.actions.LoadUserData();
      console.log('app-there -->',this.props);
    }
    componentDidMount() {
      console.log('app-here -->',this.props);
    }
    render () {
        return (
          <div className="login">
              <div className="row">
                  {this.props.children}
              </div>
          </div>
        );
    }
}

//console.log('state-->',state);

const mapStateToProps = (state) => ({
    user: 'state.auth.user',
    userName: 'state.auth.userName'
});

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(CoreLayout);
