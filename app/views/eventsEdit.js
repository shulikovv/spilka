import React from 'react';
import {Link} from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import EventForm from "../components/Events/eventForm";
import * as actionCreators from '../actions/event';

class eventsEdit extends React.Component {

    constructor(props) {
        super(props);

    }

    componentWillMount() {
      this.props.actions.getEventForm();
    }

    render () {
        console.log('events edit -->', this.props);
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Events Edit Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
              {/* end: PAGE TITLE */}
              <EventForm loading={false} {...this.props}/>
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
  form         : state.eventForm.form,
  //loading : state.person.loading,
  //schema : state.person.schema,
});

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(eventsEdit);