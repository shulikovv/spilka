import React from 'react';
import {Link} from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions';

import { Table, Icon, Button, Popconfirm, Modal } from 'antd';
import {viewPerson} from '../actions/PersonActions';
import {editPerson} from '../actions/PersonActions';


import {Select} from 'antd';
const Option = Select.Option;
import 'antd/lib/select/style/css';


class Thesauri extends React.Component {

    constructor(props) {
        super(props);
        this.state = { showModal: false };
        this.columns = [
            {
              title: 'Key',
              dataIndex: 'no'
            },
            {
              title: 'Term',
              dataIndex: 'term',
              render: (text, record) => <a onClick={()=>  this.props.actions.thesaurusView(record.no)}>{text}</a>
            },
            {
              title: 'Actions',
              key: 'actions',
              render: (text, record) => (
                  <span>
                    <a onClick={()=> console.log(record.no)}>Translate</a>
                    <span className="ant-divider" />
                    <a onClick={()=> this.props.actions.thesaurusView(record.no)}>View</a>
                    <span className="ant-divider" />
                    <Popconfirm title="Sure to delete?" onConfirm={() => console.log(record.term, 'delete')}>
                      <a>Delete</a>
                    </Popconfirm>
                  </span>
              ),
              width: 250
            },
          ];

    }

    componentWillMount() {
        this.props.actions.loadThesauriList();
    }


    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true });
    }



    render() {
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
             {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Thesauri Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
            {/* end: PAGE TITLE */}
              <div className="panel-body">
                <div className="row">
                  <div className="col-md-12">
                      <div style={{ marginBottom: 16 }}>
                         {/* <Button
                              type="primary"
                              icon="plus"
                              onClick={()=> console.log('1')}
                          >
                              Add New Thesaurus
                          </Button> */}

                      </div>
                  </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Table rowKey='no' loading={this.props.loading} rowSelection={rowSelection} dataSource={this.props.list} columns={this.columns} />
                    </div>
                </div>                
                <div className="row">
                </div>
              </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    list   : state.thesauri.list,
    loading : state.thesauri.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Thesauri);
