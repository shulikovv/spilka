import React from 'react';
import {Link} from 'react-router';

export default class HomeView extends React.Component {

    render () {
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Dashboard Page</h1>
                    <span className='mainDescription'>I do not know what mast be on dashboad, help to deal with it</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
              {/* end: PAGE TITLE */}
              <div className="panel panel-white no-radius" style={{marginTop: 20}}>
                <div className="panel-body">
                  <div className="partition-light-grey padding-15 text-center margin-bottom-20">
                    <h4 className="no-margin">Monthly Statistics</h4>
                    <span className="text-light">based on the major browsers</span>
                  </div>
                  <div id="accordion" className="panel-group accordion accordion-white no-margin">
                    <div className="panel no-radius">
                      <div className="panel-heading">
                        <h4 className="panel-title">
                          <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" className="accordion-toggle padding-15">
                            <i className="icon-arrow" />
                            This Month <span className="label label-danger pull-right">3</span>
                          </a></h4>
                      </div>
                      <div className="panel-collapse collapse in" id="collapseOne">
                        <div className="panel-body no-padding partition-light-grey">
                          <table className="table">
                            <tbody>
                              <tr>
                                <td className="center">1</td>
                                <td>Google Chrome</td>
                                <td className="center">4909</td>
                                <td><i className="fa fa-caret-down text-red" /></td>
                              </tr>
                              <tr>
                                <td className="center">2</td>
                                <td>Mozilla Firefox</td>
                                <td className="center">3857</td>
                                <td><i className="fa fa-caret-up text-green" /></td>
                              </tr>
                              <tr>
                                <td className="center">3</td>
                                <td>Safari</td>
                                <td className="center">1789</td>
                                <td><i className="fa fa-caret-up text-green" /></td>
                              </tr>
                              <tr>
                                <td className="center">4</td>
                                <td>Internet Explorer</td>
                                <td className="center">612</td>
                                <td><i className="fa fa-caret-down text-red" /></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div className="panel no-radius">
                      <div className="panel-heading">
                        <h4 className="panel-title">
                          <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" className="accordion-toggle padding-15 collapsed">
                            <i className="icon-arrow" />
                            Last Month
                          </a></h4>
                      </div>
                      <div className="panel-collapse collapse" id="collapseTwo">
                        <div className="panel-body no-padding partition-light-grey">
                          <table className="table">
                            <tbody>
                              <tr>
                                <td className="center">1</td>
                                <td>Google Chrome</td>
                                <td className="center">5228</td>
                                <td><i className="fa fa-caret-up text-green" /></td>
                              </tr>
                              <tr>
                                <td className="center">2</td>
                                <td>Mozilla Firefox</td>
                                <td className="center">2853</td>
                                <td><i className="fa fa-caret-up text-green" /></td>
                              </tr>
                              <tr>
                                <td className="center">3</td>
                                <td>Safari</td>
                                <td className="center">1948</td>
                                <td><i className="fa fa-caret-up text-green" /></td>
                              </tr>
                              <tr>
                                <td className="center">4</td>
                                <td>Internet Explorer</td>
                                <td className="center">456</td>
                                <td><i className="fa fa-caret-down text-red" /></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        );
    }

}
