import React from 'react';
import {Link} from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions';

import { Table, Icon, Button, Popconfirm, Modal } from 'antd';
import {viewPerson} from '../actions/PersonActions';
import {editPerson} from '../actions/PersonActions';


import {Select} from 'antd';
const Option = Select.Option;
import 'antd/lib/select/style/css';


class Thesaurus extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            showModal: false,
            filteredInfo: null,
            sortedInfo: null, 
        };
        this.handleChange = (pagination, filters, sorter) => {
            console.log('Various parameters', pagination, filters, sorter);
            this.setState({
              filteredInfo: filters,
              sortedInfo: sorter,
            });
          }
        
    }

    componentWillMount() {
        console.log('will try load thesaurus '+this.props.params.thesaurusID)
        this.props.actions.loadThesaurus(this.props.params.thesaurusID);
    }


    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true });
    }


    render() {
        let { sortedInfo, filteredInfo } = this.state;
        sortedInfo = sortedInfo || {};
        const columns = [
        {
            title: 'Name',
            dataIndex: 'english',
            sorter: (a, b) => a.english - b.english,
            sortOrder: sortedInfo.columnKey === 'english' && sortedInfo.order,
        },
        {
            title: 'Key',
            dataIndex: 'huri_code',
            sorter: (a, b) => a.huri_code - b.huri_code,
            sortOrder: sortedInfo.columnKey === 'huri_code' && sortedInfo.order,
        },
        ];
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
             {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Thesaurus Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
            {/* end: PAGE TITLE */}
              <div className="panel-body">
                <div className="row">
                  <div className="col-md-12">
                      <div style={{ marginBottom: 16 }}>
                         {/* <Button
                              type="primary"
                              icon="plus"
                              onClick={()=> console.log('1')}
                          >
                              Add New Thesaurus
                          </Button> */}

                      </div>
                  </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Table rowKey='vocab_number' loading={this.props.loading} rowSelection={rowSelection} dataSource={this.props.list} columns={columns} onChange={this.handleChange} />
                    </div>
                </div>                
                <div className="row">
                </div>
              </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    list   : state.thesaurus.tree,
    loading : state.thesaurus.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Thesaurus);
