import React from 'react';
import {Link} from 'react-router';
import { Button, Modal, Popover, Tooltip, OverlayTrigger  } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions';

class Forgot extends React.Component {
    render() {

        return (
            <div className="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
              <div className="logo margin-top-30">
                <h2>UHHRU</h2>
              </div>
                {/* start: FORGOT BOX */}
              <div className="box-forgot">
                <form className="form-forgot">
                  <fieldset>
                    <legend>
                      Forget Password?
                    </legend>
                    <p>
                      Enter your e-mail address below to reset your password.
                    </p>
                    <div className="form-group">
                <span className="input-icon">
                  <input type="email" className="form-control" name="email" placeholder="Email" />
                  <i className="fa fa-envelope-o" /> </span>
                    </div>
                    <div className="form-actions">
                      <Link to='/login' className="btn btn-primary btn-o">
                        <i className="fa fa-chevron-circle-left" /> Log-In
                      </Link>
                      <button type="submit" className="btn btn-primary pull-right">
                        Submit <i className="fa fa-arrow-circle-right" />
                      </button>
                    </div>
                  </fieldset>
                </form>
                  {/* start: COPYRIGHT */}
                <div className="copyright">
                  © <span className="current-year" /><span className="text-bold text-uppercase"> UHHRU</span>. <span>All rights reserved</span>
                </div>
                  {/* end: COPYRIGHT */}
              </div>
                {/* end: FORGOT BOX */}
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    filter   : 'state.report.reportFilterForm',
});

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Forgot);
