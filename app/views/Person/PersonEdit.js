import React from 'react';
import {Link} from 'react-router';
import Form from "react-jsonschema-form";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../../actions/PersonActions';
import { Spin } from 'antd';


class PersonEdit extends React.Component {
    
    constructor(props) {
        super(props);
        this.uiSchema = {
            //"ui:readonly": true,
            test: {
                "ui:widget": "hidden"//"select"
            },
            date_of_birth_type: {
                "ui:widget": "select"
            },
            date_deceased_type: {
                "ui:widget": "select"
            }
        }
    
        this.formData = {
            person_name : "Dfg"
        }
        this.onSubmit = ({formData}) => console.log("yay I'm valid!",formData);
        
    }


    logg(type) {
        console.log('*******',type);
    }


    render () {
        console.log('d--->',this.props.location.pathname);
        const title = (this.props.location.pathname == '/addperson')?'Add':'Edit';
        return (
            <div>
                {/* start: PAGE TITLE */}
                <section id='page-title'>
                    <div className='row'>
                    <div className='col-sm-8'>
                        <h1 className='mainTitle'>{title} Person Page</h1>
                        <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                    </div>
                    <ol className='breadcrumb'>
                        <li>
                        <span>Pages</span>
                        </li>
                        <li className='active'>
                        <span>Blank Page</span>
                        </li>
                    </ol>
                    </div>
                </section>
                {/* end: PAGE TITLE */}
              <Spin tip="Loading..." size="large" spinning={this.props.loading}>
                <Form schema={this.props.schema}
                      uiSchema={this.uiSchema}
                      onChange={this.logg("changed")}
                      onSubmit={this.onSubmit}
                      onError={this.logg("errors")}
                      liveValidate={true}
                      showErrorList={true}
                      formData={this.formData}
                />
                </Spin>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    //form         : '',
    loading : state.person.loading,
    schema : state.person.schema,
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(PersonEdit);