import React from 'react';
import {Link} from 'react-router';
import Form from "react-jsonschema-form";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../../actions/person/ViewActions';
import * as docActionCreators from '../../actions/document/DocumentActions';
import { Spin, Tabs, Button, Popconfirm} from 'antd';
const TabPane = Tabs.TabPane;



import PersonView from '../../components/Person/PersonView';

import {components} from 'griddle-react';

class PersonEdit extends React.Component {
    
    constructor(props) {
        super(props);
        this.uiSchema = {
            "ui:readonly": true,
            test: {
                "ui:widget": "hidden"//"select"
            },
            date_of_birth_type: {
                "ui:widget": "select"
            },
            date_deceased_type: {
                "ui:widget": "select"
            }
        }
    
        this.formData = {
            person_name : "Dfg"
        }
        this.onSubmit = ({formData}) => console.log("yay I'm valid!",formData);
    }

    componentWillMount() {
        this.props.actions.getPersonViewForm(this.props.params.userID);
        this.props.actions.getPersonViewAddressForm();
        this.props.actions.getPersonViewBioForm();
    }

    componentWillUpdate(nextProps, nextState) {
        
    }



    logg(type) {
        console.log('*******',type);
    }

    

    render () {
        console.log('d--->',this.props.location.pathname);
        const title = (this.props.location.pathname == '/addperson')?'Add':'Edit';
        return (
            <div>
                {/* start: PAGE TITLE */}
                <section id='page-title'>
                    <div className='row'>
                    <div className='col-sm-8'>
                        <h1 className='mainTitle'>Просмотр данных о человеке</h1>
                        <span className='mainDescription'></span>
                    </div>
                    <ol className='breadcrumb'>
                        <li>
                        <span>Pages</span>
                        </li>
                        <li className='active'>
                        <span>Blank Page</span>
                        </li>
                    </ol>
                    </div>
                </section>
                {/* end: PAGE TITLE */}
              <Spin tip="Loading..." size="large" spinning={this.props.loading}>
                    <PersonView {...this.props}></PersonView>
                </Spin>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    form    : state.personView.form,
    loading : state.person.loading,
    schema  : state.person.schema,
    aform   : state.personView.aform,
    bioform : state.personView.bioform,
    docs    : state.docs,
    personView: {
        addresses : {
            modalvisible    : state.personView.addresses.modalvisible,
            modalviewvisible: state.personView.addresses.modalviewvisible,
            modaleditvisible: state.personView.addresses.modaleditvisible,
            tableData       : state.personView.addresses.tableData,
        },
        bioform : {
            modalvisible    : state.personView.bioforms.modalvisible,
            modalviewvisible: state.personView.bioforms.modalviewvisible,
            modaleditvisible: state.personView.bioforms.modaleditvisible,
            tableData       : state.personView.bioforms.tableData,
        },
        documents: state.personView.documents,
    },
    user: state.user
});
console.log('here-here')
const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators({...actionCreators, ...docActionCreators}, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(PersonEdit);