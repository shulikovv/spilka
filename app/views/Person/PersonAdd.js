import React from 'react';
import {Link} from 'react-router';
import Form from "react-jsonschema-form";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../../actions/person/FormAddActions';
import { Spin } from 'antd';


import {widgets} from "../../elements/form/widgets";
import * as _ from 'lodash';

import PersonForm from '../../components/Person/PersonForm';

class PersonAddNew extends React.Component {
    
    constructor(props) {
        super(props);
        this.formData = {
            person_name : "Dfg"
        }
        this.onSubmit = ({formData}) => {this.props.actions.personFormSubmit(formData)};
        
    }

    componentWillMount () {
        this.props.actions.getPersonForm();
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('update-15');
    }
    

    logg(type) {
        console.log('*******',type);
    }


    render () {
        console.log('d--->',this.props.location.pathname);
        const title = (this.props.location.pathname == '/addperson')?'Добавить':'Редактировать';
        return (
            <div>
                {/* start: PAGE TITLE */}
                <section id='page-title'>
                    <div className='row'>
                    <div className='col-sm-8'>
                        <h1 className='mainTitle'>{title} данные о человеке</h1>
                        <span className='mainDescription'>...</span>
                    </div>
                    <ol className='breadcrumb'>
                        <li>
                        <span>Pages</span>
                        </li>
                        <li className='active'>
                        <span>Blank Page</span>
                        </li>
                    </ol>
                    </div>
                </section>
                {/* end: PAGE TITLE */}
                <Spin tip="Loading..." size="large" spinning={this.props.loading}>
                    <PersonForm {...this.props}></PersonForm>
                </Spin>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    form         : state.personForm.form,
    errors         : state.personForm.errors,
    loading : state.person.loading,
    //schema : state.personForm.schema,
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(PersonAddNew);