import React from 'react';
import {Link} from 'react-router';
import _ from 'lodash';
import { Table, Icon, Button, Popconfirm } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions';
import Events from './Events';


class Vechicles extends React.Component {

    constructor(props) {
        super(props);
        this.dataSource = [
            {
                id : 1,
                vechicle_id: '1',
                vechicle_purpose: '',
                vechicle_brand: 'Mike',
                vechicle_color: 'red',
                vechicle_type: 'male',
                vechicle_model: '',
                vechicle_number: '',
            },
            {
                id : 2,
                vechicle_id: '1',
                vechicle_purpose: '',
                vechicle_brand: 'Mike',
                vechicle_color: 'red',
                vechicle_type: 'male',
                vechicle_model: '',
                vechicle_number: '',
            },
    
            ];
    
        this.columns = [
            {
              title: 'Purpose',
              dataIndex: 'vechicle_purpose',
            },
            {
              title: 'Brand',
              dataIndex: 'vechicle_brand',
            },
            {
              title: 'Color',
              dataIndex: 'vechicle_color',
            },
            {
              title: 'Type',
              dataIndex: 'vechicle_type',
            },
            {
              title: 'Model',
              dataIndex: 'vechicle_model',
            },
            {
              title: 'Number',
              dataIndex: 'vechicle_number',
            },
            {
                title: 'Actions',
                key: 'actions',
                render: (text, record) => (
                    <span>
                      <a onClick={()=>  console.log(record.id)}>View</a>
                      <span className="ant-divider" />
                      <a onClick={()=> console.log(record.id)}>Edit</a>
                      <span className="ant-divider" />
                      <Popconfirm title="Sure to delete?" onConfirm={() => console.log(record.id, 'delete')}>
                        <a>Delete</a>
                      </Popconfirm>
                    </span>
                )
            },
        ];
    }




    render () {
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Vechicles Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
            {/* end: PAGE TITLE */}
            <div className="panel-body">
                    <div className="row">
                        <div className="col-md-12">
                            <div style={{ marginBottom: 16 }}>
                                <Button
                                    type="primary"
                                    icon="plus"
                                    onClick={()=> console.log('here')}
                                    style={{ marginRight: 10 }}
                                >
                                    Add New Vechcle
                                </Button>
                                <Button
                                    type="primary"
                                    icon="plus"
                                    onClick={()=> console.log('here')}
                                    
                                >
                                    Add New war Vechcle
                                </Button>

                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <Table rowKey='id' rowSelection={rowSelection} dataSource={this.dataSource} columns={this.columns} />
                        </div>
                    </div>
              </div>
              {/* this.props.location.pathname */}
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    form         : '',
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Vechicles);
