import HomeView from './HomeView';
import Signin from './signin';
import Forgot from './forgot';
import UserProfile from './userProfile';
import Events from './Events';
import Persons from './Persons';
import Documents from './Documents';
import Document from './Document';
import Dashboard from './Dashboard';
import eventsEdit from './eventsEdit';
import eventsView from './eventsView';
import noLang from './noLanguage';
import Thesauri from './Thesauri';
import Thesaurus from './Thesaurus';
import PersonEdit from './Person/PersonEdit';
import PersonAdd from './Person/PersonAdd';
import PersonEditNew from './Person/PersonEditNew';
import PersonView from './Person/PersonView';
import Users from './Users';
import UsersList from './Admin/Users/UserList';
import Groups from './Admin/Users/Groups';
import Tolerance from './Admin/Users/Tolerance';
import Roles from './Admin/Users/Roles';
import Organizations from './Organizations';
import Organizations2 from "./Organizations.1";
import OrganizationsAlt2 from "./Organizations.2";
import OrganizationsAlt3 from "./Organizations.3";
import organizationView from './organizationView';
import Vechicles from "./Vechicles";
import AddVechcle from "./Vechicle/Add";
import OrganizationAddForm from "./organizationAddForm";
import OrganizationEditForm from "./organizationEditForm";
import EventsAddAct from "./eventsAddAct";
import Documentation from "./Documentation";

export {
  HomeView,
  Signin,
  Forgot,
  UserProfile,
  Events,
  Persons,
  Organizations,
  Organizations2,
  OrganizationsAlt2,
  OrganizationsAlt3,
  organizationView,
  Documents,
  Document,
  Dashboard,
  Documentation,
  eventsEdit,
  eventsView,
  noLang,
  Thesauri,
  Thesaurus,
  Users,
  Roles,
  Permissions,
  PersonEdit,
  PersonAdd,
  PersonView,
  UsersList,
  Vechicles,
  AddVechcle,
  Groups,
  Tolerance,
  OrganizationAddForm,
  OrganizationEditForm,
  PersonEditNew,
  EventsAddAct,
};