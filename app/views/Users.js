import React from 'react';
import {Link, browserHistory} from 'react-router';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Griddle from 'griddle-react';
import StructuredFilter from 'react-structured-filter';
import { Table, Icon, Button, Popconfirm } from 'antd';

//import * as actionCreators from '../actions/PersonActions';
import * as actionCreators from '../actions';

class Persons extends React.Component {

    constructor(props) {
        super(props);
    }

    render () {
        let self = this;
        let actions = this.props.actions;
        console.log('Users-->',this.props);
        var children = React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, {
                //actions: actions
            })
        })
        return (
            <div>
                {children}
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    //form         : '',
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Persons);