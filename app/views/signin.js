import React from 'react';
import {Link} from 'react-router';
import { Button, Modal, Popover, Tooltip, OverlayTrigger  } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Form from "react-jsonschema-form";

import * as actionCreators from '../actions';

class Signin extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            model: {},

        };
        console.log('[[[[',Form);
        this.errorSchema = {
            password: {
                errors:[
                   // "Passwords don't match"
                    ]
            }
        }


        this.errors = [
            {
                stack: 'password: Passwords don\'t match678'
            }
            ]

        this.schema = {
            "title": "Sign in to your account",
            "description": "Please enter your name and password to log in.",
            type: "object",
            properties: {
                "username": {
                    "title": "User name",
                    "type": "string",
                    "minLength": 4,
                },
                "password": {
                    "title": "Password",
                    "type": "string",
                    "minLength": 4,
                }
            }
        }

        this.uiSchema = {
            username: {
                "ui:options": {
                    label: false,
                    errors: [
                        "test"
                    ]
                },
                "ui:placeholder": "User name"
            },
            password: {
                "ui:options": {
                    label: false
                },
                "ui:placeholder": "Password",
                "ui:widget": "password",
            },
        }
        this.validate = () => {
            // validate={this.validate()}
            var self = this;
            return function (formData, errors) {
                //if (formData.password !== 'admin1') {
                //    errors.password.addError("Passwords don't match");
                //}
                //errors.password.addError(self.errors[0].stack);
                return errors;
    
            }
        }
    
        this.onSubmit = ({formData}) => {
            this.props.actions.loginUser(formData.username,formData.password);
            console.log("yay I'm valid!",formData);
        }
    

    }



 
    componentWillMount() {

    }

    render() {
        let errorsBlock = ''
        if (this.props.errors.length > 0) {
            errorsBlock = <div className="alert alert-danger">
                      <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                    {this.props.errors.map((item, index) => <p key={index}> {item} </p>)}
                  </div>;
        }
        return (
            <div className="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
              <div className="logo margin-top-30">
                <h2>UHHRU {this.props.userName}</h2>
              </div>
                {/* start: LOGIN BOX */}
              <div className="box-login">
                  {errorsBlock}
                <Form
                      className="form-login"
                      schema={this.schema}
                      uiSchema={this.uiSchema}
                      onSubmit={this.onSubmit}
                      liveValidate={true}

                      showErrorList={false}
                      errorSchema = {this.errorSchema}
                >
                  <div className="form-actions">
                    <button type="submit" className="btn btn-primary pull-right">
                      Login <i className="fa fa-arrow-circle-right" />
                    </button>
                  </div>
                </Form>
                  {/* start: COPYRIGHT */}
                <div className="copyright">
                  © <span className="current-year" /><span className="text-bold text-uppercase"> UHHRU</span>. <span>All rights reserved</span>
                </div>
                  {/* end: COPYRIGHT */}
              </div>
                {/* end: LOGIN BOX */}
            </div>

        );
    }
}



const mapStateToProps = (state) => ({
    userName   : state.user.name,
    errors     : state.loginForm.errors
});

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Signin);
