import React from 'react';
import {Link} from 'react-router';

export default class HomeView extends React.Component {

    render () {
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Starter Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
              {/* end: PAGE TITLE */}
              Hello
              <Link to='/reports/rep1'> Rep test </Link>
              {/* this.props.location.pathname */}
            </div>
        );
    }

}
