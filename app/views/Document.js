import React from 'react';
import {Link, browserHistory} from 'react-router';
import {push} from 'react-router-redux';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import _ from 'lodash';
import {Upload, message, Table, Icon, Button, Popconfirm} from 'antd';
import * as actionCreators from '../actions';
import Form from "react-jsonschema-form";
import axios from 'axios';
//import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';
import FileDownload from 'react-file-download';

import AddForm from '../components/Document/simpleForm/addForm';
//import PersonForm from '../components/Person/PersonForm';

class DocumentEditView extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            fileList: [],
            uploading: false,
        }

    }


    render() {
        const {uploading} = this.state;
        let formData = {};
        const handleUpload = () => {
            const {fileList} = this.state;
            const formData = new FormData();
            fileList.forEach((file) => {
                console.log('file');
                formData.append('file', file);
            });
            formData.append('foo', 'bar');
            this.setState({
                uploading: true,
            });

            axios.post('/api/document/store-document', formData, {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                }
            })
                .then((resp) => {
                    console.log('resp', resp);
                    if (resp.data.status == 'ok') {
                        console.log(resp.data);
                        this.setState({
                            fileList: [],
                            uploading: false,
                        });
                        //dispatch(personFormSavedOk());
                        //dispatch(push('/persons'));
                    } else if (resp.data.status == 'error') {
                        this.setState({
                            uploading: false,
                        });
                        console.log(resp.data.errors);
                        //dispatch(getErrorWhenAdd(resp.data.errors))
                    }
                })

        };

        const testDownload = () => {
            axios.get('/api/document/get-file', {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                }
            })
                .then((resp) => {
                    console.log('resp', resp);
                    FileDownload(resp.data, 'filename.less')
                })
        };

        const schema = {
            title: "Todo",
            type: "object",
            required: ["title"],
            properties: {
                title: {type: "string", title: "Title", default: "A new task"},
                done: {type: "boolean", title: "Done?", default: false},
                "file": {
                    "type": "string",
                    "format": "data-url",
                    "title": "Single file"
                },
            }
        };

        const log = (type) => console.log.bind(console, type);

        const props = {
            multiple: false,
            action: '',
            onRemove: (file) => {
                this.setState(({fileList}) => {
                    const index = fileList.indexOf(file);
                    const newFileList = fileList.slice();
                    newFileList.splice(index, 1);
                    return {
                        fileList: newFileList,
                    };
                });
            },
            beforeUpload: (file) => {
                formData = {...formData, file2: file};
                this.setState(({fileList}) => ({
                    fileList: [file],
                }));
                return false;
            },
            fileList: this.state.fileList,
        };

        const onSubmit = ({formData}) => {
            const {fileList} = this.state;

            const _formData = new FormData();
            fileList.forEach((file) => {
                console.log('file');
                _formData.append('file2', file);
            });
            for (let key in formData) {
                _formData.append(key, formData[key]);
            }
            _formData.append('foo', 'bar');

            axios.post('/api/document/store-document2', _formData, {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                }
            })
                .then((resp) => {
                    console.log('resp', resp);
                    if (resp.data.status == 'ok') {
                        console.log(resp.data);
                        this.setState({
                            fileList: [],
                            uploading: false,
                        });
                        //dispatch(personFormSavedOk());
                        //dispatch(push('/persons'));
                    } else if (resp.data.status == 'error') {
                        this.setState({
                            uploading: false,
                        });
                        console.log(resp.data.errors);
                        //dispatch(getErrorWhenAdd(resp.data.errors))
                    }
                })
        };

        return (
            <div>
                {/* start: PAGE TITLE */}
                <section id='page-title'>
                    <div className='row'>
                        <div className='col-sm-8'>
                            <h1 className='mainTitle'>Документ</h1>
                            <span className='mainDescription'>Заготовка для редактирования документа</span>
                        </div>
                        <ol className='breadcrumb'>
                            <li>
                                <span>Pages</span>
                            </li>
                            <li className='active'>
                                <span>Blank Page</span>
                            </li>
                        </ol>
                    </div>
                </section>
                {/* end: PAGE TITLE */}
                <AddForm />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    //form         : '',
    columns: state.personList.columns,
    data: state.personList.data,
    user: state.user
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(DocumentEditView);
