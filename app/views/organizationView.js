import React from 'react';
import {Link} from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions';

import Form from "react-jsonschema-form";
import Victims from "../components/Events/victims";
import Sources from "../components/Events/sources";
import Interventions from "../components/Events/interventions";
import ChainOfEvents from "../components/Events/chainOfEvents";
import Documents from "../components/Events/documents";
import AuditLog from "../components/Events/auditLog";
import Permissions from "../components/Events/Permissions";
import EventForm from "../components/Events/eventForm";
import ViewFormButtons from "../components/Events/viewFormButtons1";

import OrgForm from "../components/Organization/orgForm";
import Addresses from "../components/Organization/addresses";
import Staff from "../components/Organization/staff";
import Sublevels from "../components/Organization/sublevels";
import Vechicles from "../components/Organization/vechicles"

import { Tabs } from 'antd';

const TabPane = Tabs.TabPane;




class organizationView extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = ({formData}) => console.log("yay I'm valid!",formData);
    }

  
    logg(type) {
        console.log('*******',type);
    }


    render () {
        console.log('events edit -->', this.props);
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Organization View Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
                {/* end: PAGE TITLE */}
                <div className="panel-body">
                    <div className="row">
                        <div className="col-mb-12">
                            <Tabs defaultActiveKey="1">
                                <TabPane tab="Organization record" key="1"><OrgForm loading={false}></OrgForm></TabPane>
                                <TabPane tab="Addresses" key="2"><Addresses></Addresses></TabPane>
                                <TabPane tab="Sublevels" key="3"><Sublevels></Sublevels></TabPane>
                                <TabPane tab="Stuff" key="4"><Staff></Staff></TabPane>
                                <TabPane tab="Vechicles" key="5"><Vechicles actions={this.props.actions}></Vechicles></TabPane>
                            </Tabs>
                        </div>
                    </div>
                </div>    
            </div>
        );
    }

}


const mapStateToProps = (state) => ({
    form         : '',
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(organizationView);