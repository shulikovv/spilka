import React from 'react';
import {Link} from 'react-router';
import _ from 'lodash';
import { Table, Icon, Button, Popconfirm } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/organizations';
import Events from './Events';

import OrgTree from '../components/Org/OrgTree';
import OrgForm from '../components/Org/OrgForm';

class Organizations extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }

    componentWillMount() {
        this.props.actions.getOrgTable();
    }

    render () {
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Organizations Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
            {/* end: PAGE TITLE */}
            <div className="panel-body">
                    <div className="row">
                        <div className="col-md-3">
                            <OrgTree {...this.props}></OrgTree>
                        </div>
                        <div className="col-md-9">
                            <OrgForm {...this.props}></OrgForm>
                        </div>
                    </div>
              </div>
              {/* this.props.location.pathname */}
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    form: state.orgForm.form,
    orgTreeData  : state.orgForm.tree,
    treeDefaultSelectedKeys: state.orgForm.treeDefaultSelectedKeys
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Organizations);
