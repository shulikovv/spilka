import React from 'react';
import {Link} from 'react-router';
import Form from "react-jsonschema-form";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../../actions/event';
import { Spin } from 'antd';

import EventForm from "../components/Events/eventForm";

class EventEdit extends React.Component {
    
    uiSchema = {
        //"ui:readonly": true,
        test: {
            "ui:widget": "hidden"//"select"
        },
        date_of_birth_type: {
            "ui:widget": "select"
        },
        date_deceased_type: {
            "ui:widget": "select"
        }
    }

    formData = {
        person_name : "Dfg"
    }

    logg(type) {
        console.log('*******',type);
    }

    
    componentWillMount() {
        this.props.actions.getEventForm();
    }
    

    onSubmit = ({formData}) => console.log("yay I'm valid!",formData);

    render () {
        console.log('d--->',this.props.location.pathname);
        const title = (this.props.location.pathname == '/addperson')?'Add':'Edit';
        return (
            <div>
                {/* start: PAGE TITLE */}
                <section id='page-title'>
                    <div className='row'>
                    <div className='col-sm-8'>
                        <h1 className='mainTitle'>{title} Event Page</h1>
                        <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                    </div>
                    <ol className='breadcrumb'>
                        <li>
                        <span>Pages</span>
                        </li>
                        <li className='active'>
                        <span>Blank Page</span>
                        </li>
                    </ol>
                    </div>
                </section>
                {/* end: PAGE TITLE */}
              <EventForm {...this.props}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    form         : state.EventForm.form,
    loading : state.person.loading,
    schema : state.person.schema,
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(EventEdit);