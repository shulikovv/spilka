import React from 'react';
import {Link} from 'react-router';
import _ from 'lodash';
import { Table, Icon, Button, Popconfirm } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions';
import Events from './Events';


class Organizations extends React.Component {

    constructor(props) {
        super(props);
        this.dataSource = [
                {
                    id: 1,
                    title: 'Organization 1',
                    shortTitle: 'O1',
                    dateUpdated: '2014-04-04',
                    updatedBy: 'admin',
                    children: [
                        {
                            id: 3,
                            title: 'Department 1',
                            shortTitle: 'O1',
                            dateUpdated: '2014-04-04',
                            updatedBy: 'admin',
                        },
                        {
                            id: 4,
                            title: 'Department 2',
                            shortTitle: 'O1',
                            dateUpdated: '2014-04-04',
                            updatedBy: 'admin',
                            children: [
                                {
                                    id: 7,
                                    title: 'sub Department 1',
                                    shortTitle: 'O1',
                                    dateUpdated: '2014-04-04',
                                    updatedBy: 'admin',
                                },
                                {
                                    id: 8,
                                    title: 'sub Department 1',
                                    shortTitle: 'O1',
                                    dateUpdated: '2014-04-04',
                                    updatedBy: 'admin',
                                },
                                {
                                    id: 9,
                                    title: 'sub Department 1',
                                    shortTitle: 'O1',
                                    dateUpdated: '2014-04-04',
                                    updatedBy: 'admin',
                                },

                            ]
                        },
                    ]
                },
                {
                    id: 2,
                    title: 'Organization 2',
                    shortTitle: 'O1',
                    dateUpdated: '2014-04-04',
                    updatedBy: 'admin',
                    children: [
                        {
                            id: 5,
                            title: 'Department 3',
                            shortTitle: 'O1',
                            dateUpdated: '2014-04-04',
                            updatedBy: 'admin',
                            children: [
                                {
                                    id: 6,
                                    title: 'subdepartment 1',
                                    shortTitle: 'O1',
                                    dateUpdated: '2014-04-04',
                                    updatedBy: 'admin',
                                    children: [
                                    ]
                                },
                            ]
                        },
                    ]
                },
            ];

        this.columns = [
            {
            title: 'Organization Title',
            dataIndex: 'title',
            width: 250,
            render: (text, record) => <a onClick={()=>  this.props.actions.orgView(record.id)}>{text}</a>
            },
            {
            title: 'Short Title',
            dataIndex: 'shortTitle',
            render: (text, record) => <a onClick={()=>  this.props.actions.orgView(record.id)}>{text}</a>
            },
            {
                title: 'Actions',
                key: 'actions',
                render: (text, record) => (
                    <span>
                    <a onClick={()=>  this.props.actions.orgView(record.id)}>View</a>
                    <span className="ant-divider" />
                    <a onClick={()=> console.log(record.id)}>Edit</a>
                    <span className="ant-divider" />
                    <Popconfirm title="Sure to delete?" onConfirm={() => console.log(record.id, 'delete')}>
                        <a>Delete</a>
                    </Popconfirm>
                    </span>
                )
            },
        ];



    }


    render () {
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Organizations Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
            {/* end: PAGE TITLE */}
            <div className="panel-body">
                    <div className="row">
                        <div className="col-md-12">
                            <div style={{ marginBottom: 16 }}>
                                <Button
                                    type="primary"
                                    icon="plus"
                                    onClick={()=> console.log('here')}
                                >
                                    Add New Organization
                                </Button>

                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <Table rowKey='id' rowSelection={rowSelection} dataSource={this.dataSource} columns={this.columns} />
                        </div>
                    </div>
              </div>
              {/* this.props.location.pathname */}
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    form         : '',
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Organizations);
