import React from 'react';
import {Link} from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/organizations/AddFormActions';
import { Alert} from 'antd';
import * as _ from 'lodash';

import Form from "react-jsonschema-form";

import {widgets} from "../elements/form/widgets";
import axios from 'axios';
import qs from 'qs';

class organizationAddForm extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = ({formData}) => {
            //console.log('submit',e);
            //
            this.props.actions.submitAddOrgForm(formData);
        };
    
        this.validate = (formData, errors) => {
            //if (formData.pass1 !== formData.pass2) {
            //    errors.pass2.addError("Passwords don't match");
            //}
            console.log('validate',formData, errors, errors.organization_id);
            if (errors.organization_id) {
                //errors.organization_id.addError("Passwords don't match");
            }
            //errors.addError("Passwords glob don't match");
            return errors;
        }
    
        this.getErrList = (errors) => {
            let _arr = [];
            for (let key in errors) {
                errors[key].map((item,index)=> {
                    _arr.push(<li key={key+index}>{item}</li>);
                })
            }
            return _arr;
        }
    
    }

    componentWillMount() {
        this.props.actions.getOrgAddForm();
        axios.get('/api/organization/get-user', {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((resp)=>{
            console.log('resp',resp);
        })
    }
  
    componentWillUnmount() {
        this.props.actions.addOrgFormSavedOk();
    }

    logg(type) {
        console.log('*******',type);
    }


    render () {
        console.log('events edit -->', this.props);
          const transformErrors = errors => {
              console.log('transformErrors', errors);
                return errors.map(error => {
                // use error messages from JSON schema if any
                if (error.schema.messages && error.schema.messages[error.name]) {
                    return {
                    ...error,
                    message: error.schema.messages[error.name]
                    };
                }
                return error;
                });
            };
        let errorsList = '';
        if (_.size(this.props.errors)>0) {
            errorsList = (<div role="alert" className="alert alert-danger">
                                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <ul>
                                    {this.getErrList(this.props.errors)}
                                </ul>
                            </div>);
        }
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Organization Add Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
                {/* end: PAGE TITLE */}
                <div className="panel-body">
                    <div className="row">
                        <div className="col-mb-12">
                           {errorsList}
                            <Form schema={this.props.form.schema}
                            uiSchema={this.props.form.uiSchema}
                            formData={this.props.form.data}
                            widgets={widgets}
                            onSubmit={this.onSubmit}
                            showErrorList={true}
                            liveValidate={true}
                            validate={this.validate}
                            transformErrors={transformErrors}
                            ></Form>
                        </div>
                    </div>
                </div>    
            </div>
        );
    }

}


const mapStateToProps = (state) => ({
    form      :  state.orgAddForm.form,
    errors    :  state.orgAddForm.errors,
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(organizationAddForm);