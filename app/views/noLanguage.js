import React from 'react';
import {Link} from 'react-router';

export default class HomeView extends React.Component {

    render () {
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <span className='mainDescription'>Tere are no language yet</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
              {/* end: PAGE TITLE */}
            </div>
        );
    }

}
