import React from 'react';
import {Link, browserHistory} from 'react-router';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Griddle from 'griddle-react';
import StructuredFilter from 'react-structured-filter';
import { Table, Icon, Button, Popconfirm } from 'antd';
import { isCan } from '../utils/index'
import * as actionCreators from '../actions/PersonActions';

class Persons extends React.Component {

    updateFilter(filter){
        // Set our filter to json data of the current filter tokens
        //this.setState({filter: JSON.stringify(filter)});
        console.log('ssss',filter);
    };

    

    constructor(props){
        super(props);
        this.uf = (filter) => console.log('aaaaa',filter);
        
        this.state = {
            columns: [...props.columns, {
                title: 'Действия',
                key: 'actions',
                render: (text, record) => {
                    let Actions = [];
                    if (isCan('person','read', this.props.user)) {
                        Actions.push(<span key='read'>
                            <a onClick={()=> props.actions.viewPerson(record.person_id)}>Смотреть</a>
                            {(()=>{
                                if (isCan('person', 'update', this.props.user) || isCan('person', 'delete', this.props.user)) {
                                    return (<span className="ant-divider" />)
                                }
                            })()}
                        </span>)
                    }
                    if (isCan('person','update', this.props.user)) {
                        Actions.push(<span key='update'>
                            <a onClick={()=> props.actions.editPerson(record.person_id)}>Редактировать</a>
                            {(()=>{
                                if (isCan('person', 'delete', this.props.user)) {
                                    return (<span className="ant-divider" />)
                                }
                            })()}
                        </span>)
                    }
                    if (isCan('person', 'delete', this.props.user)) {
                        Actions.push(<span key='delete'>
                                  <Popconfirm title="Sure to delete?" onConfirm={() => this.props.actions.listActions(record, 'delete')}>
                                    <a>Удалить</a>
                                  </Popconfirm>
                        </span>)
                    }
                    return (
                        <span>
                        {Actions}
                      </span>
  
                    )
                },
                width: 250
            }]
        };
    }

    componentWillMount() {
        this.props.actions.getPersonListData();
    }


    render () {
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        let addButton;
        if (isCan('person','create', this.props.user)) {
            addButton = (<Button
                type="primary"
                icon="plus"
                onClick={()=> this.props.actions.AddPerson()}
            >
                Add New Person
            </Button>)
        }
        return (
            <div>
                <style>
                    {".filter-tokenizer .rdt{position: inherit; }"}
                </style>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Люди</h1>
                    <span className='mainDescription'></span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
              {/* end: PAGE TITLE */}
                <div className="container">
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <div style={{ marginBottom: 16 }}>
                                        {addButton}
                                    </div>
                                </div>
                            </div>
                            <div className="row" style={{display: 'none'}}>
                                <div className="col-md-12">
                                    <StructuredFilter
                                        placeholder=""
                                        options={[
                                            {category:"Symbol", type:"textoptions", options: function() {return ['sdd','ddfg','bugaga'];}},
                                            {category:"Name",type:"text"},
                                            {category:"Price",type:"number"},
                                            {category:"MarketCap",type:"number"},
                                            {category:"IPO", type:"date"},
                                        ]}
                                        customClasses={{
                                            input: "filter-tokenizer-text-input",
                                            results: "filter-tokenizer-list__container",
                                            listItem: "filter-tokenizer-list__item"
                                        }}
                                        onChange={this.updateFilter}
                                        onTokenAdd={this.uf}
                                        onTokenRemove={this.updateFilter}
                                    />
                                    <br/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <Table rowKey="person_id" rowSelection={rowSelection} dataSource={this.props.data} columns={this.state.columns} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

              {/* this.props.location.pathname */}
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    //form         : '',
    columns: state.personList.columns,
    data: state.personList.data,
    user: state.user
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Persons);