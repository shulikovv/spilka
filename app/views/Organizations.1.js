import React from 'react';
import {Link} from 'react-router';
import _ from 'lodash';
import { Table, Icon, Button, Popconfirm, Tree, Modal, Alert } from 'antd';
import Form from "react-jsonschema-form";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions';
import Events from './Events';

import { Select } from 'antd';
const Option = Select.Option;

import {widgets} from "../elements/form/widgets";

const TreeNode = Tree.TreeNode;

class Organizations extends React.Component {

    constructor(props) {
        super(props);
        this.columns = [
            {
              title: 'Organization ID',
              dataIndex: 'organization_id',
              render: (text, record) => <a onClick={()=>  this.props.actions.orgView(record.id)}>{text}</a>
            },
            {
              title: 'Organization Title',
              dataIndex: 'organization_title',
              render: (text, record) => <a onClick={()=>  this.props.actions.orgView(record.id)}>{text}</a>
            },
            {
              title: 'Short Title',
              dataIndex: 'organization_abb',
              render: (text, record) => <a onClick={()=>  this.props.actions.orgView(record.id)}>{text}</a>
            },
            {
                title: 'Actions',
                key: 'actions',
                render: (text, record) => (
                    <span>
                      <a onClick={()=>  this.props.actions.orgView(record.id)}>View</a>
                      <span className="ant-divider" />
                      <a onClick={()=> console.log(record.id)}>Edit</a>
                      <span className="ant-divider" />
                      <Popconfirm title="Sure to delete?" onConfirm={() => console.log(record.id, 'delete')}>
                        <a>Delete</a>
                      </Popconfirm>
                    </span>
                )
            },
        ];
    
        this.schema = {
            type: "object",
            properties: {
                "organization_title": {
                    "title": "Title",
                    "type": "string",
                },
                "organization_abb": {
                    "title": "Abbreviation",
                    "type": "string",
                },
                "organization_description": {
                    "title": "Description",
                    "type": "string",
                },
            }
        }
    
        this.uiSchema = {
            "ui:readonly": true,
        }
    
        this.prepareTree = (tree) => {
            let out = tree.map((item)=>{
                if (item.children && item.children.length > 0) {
                    return <TreeNode title={item.organization_title} key={''+item.organization_id}>{this.prepareTree(item.children)}</TreeNode>
                } else {
                    return <TreeNode title={item.organization_title} key={''+item.organization_id}/>
                }
            })
            return out;
        }
    
        this.onSelect = (e) => {
            this.props.actions.orgGetOrganization(e[0]);
            console.log('onSElect',e)
        };
    }


    componentWillMount() {
        //this.props.actions.orgGetList();
        //this.props.actions.orgGetChildrenList(1);
        this.props.actions.orgGetTree();
        //this.props.actions.orgGetChildrenTree(1);
    }



    render () {
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
                {/*<style>{`.ant-modal-mask, .ant-modal-wrap { z-index: 1000000;}`}</style>*/}
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Organizations Page.</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content 1</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
            {/* end: PAGE TITLE */}
            <div className="panel-body">

                    <div className="row">
                        <div className="col-sm-3">
                                <div className="panel panel-white" id="panel1">
                                <div className="panel-heading">
                                    <h4 className="panel-title text-primary">Organizations</h4>
                                    <div className="panel-tools">
                                    <a data-original-title="Collapse" data-toggle="tooltip" data-placement="top" className="btn btn-transparent btn-sm panel-collapse" href="#"><i className="ti-minus collapse-off" /><i className="ti-plus collapse-on" /></a>
                                    </div>
                                </div>
                                <div className="panel-body">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div style={{ marginBottom: 16 }}>
                                                <Button
                                                    type="primary"
                                                    icon="plus"
                                                    onClick={()=> this.props.actions.orgAddOrganization()}
                                                >
                                                    Add New Organization
                                                </Button>

                                            </div>
                                        </div>
                                    </div>
                                    <Tree
                                        showLine
                                        defaultExpandedKeys={[]}
                                        defaultSelectedKeys={["1"]}
                                        onSelect={this.onSelect}
                                    >
                                        {this.prepareTree(this.props.orgTree)}
                                    </Tree>
                                    
                                </div>
                                </div>
                            </div>

                        <div className="col-sm-9">
                            <div className="tabbable">
                                <ul id="myTab2" className="nav nav-tabs nav-justified">
                                <li  className="active">
                                    <a href="#orgTab_information" data-toggle="tab" onClick={()=> console.log('here--1!')}>
                                        Information
                                    </a>
                                </li>
                                <li>
                                    <a href="#orgTab_departments" data-toggle="tab" onClick={()=> console.log('here--1!')}>
                                        Departments
                                    </a>
                                </li>
                                <li>
                                    <a href="#orgTab_personal" data-toggle="tab">
                                        Personal
                                    </a>
                                </li>
                                <li>
                                    <a href="#orgTab_vechicles" data-toggle="tab" onClick={()=> console.log('here--!!')}>
                                        Vechicles
                                    </a>
                                </li>
                                </ul>
                                <div className="tab-content">
                                    <div className="tab-pane fade in active" id="orgTab_information">
                                        <Button type="primary" style={{marginRight: 8}}>Edit</Button>
                                        {/*<Button type="primary" style={{marginRight: 8}}>Print</Button>*/}
                                        <Popconfirm title="Sure to delete?" onConfirm={() => console.log('delete')}>
                                            <Button type="primary">Delete</Button>
                                        </Popconfirm>
                                        <Form schema={this.schema}
                                            uiSchema={this.uiSchema}
                                            formData={this.props.orgInfo.information}
                                        >
                                        <div></div>
                                        </Form>
                                    </div>
                                    <div className="tab-pane fade" id="orgTab_departments">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div style={{ marginBottom: 16 }}>
                                                    <Button
                                                        type="primary"
                                                        icon="plus"
                                                        onClick={()=> console.log('here')}
                                                    >
                                                        Add New Department
                                                    </Button>

                                                </div>
                                            </div>
                                        </div>
                                        <Table rowKey='organization_id' rowSelection={rowSelection} dataSource={this.props.orgInfo.departments} columns={this.columns} />
                                    </div>
                                    <div className="tab-pane fade" id="orgTab_personal">
                                        <Table columns={this.props.orgPersonal.columns}></Table>
                                    </div>
                                    <div className="tab-pane fade" id="orgTab_vechicles">
                                        <Table columns={this.props.orgVechicles.columns}></Table>
                                    </div>
                                </div>
                            </div>

                        </div>
                <Modal
                    title="Create new Organization"
                    visible={this.props.showAddModal}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.props.actions.orgCloseAddOrganizationModal()}
                    width={1000}
                    wrapClassName="modalw"
                    >
                    <Alert
                        message="Informational Notes"
                        description="tommorrow :)."
                        type="info"
                        showIcon
                    />
                    <Select defaultValue="lucy" style={{ width: 120 }} onChange={(value)=>console.log(`selected ${value}`)}>
      <Option value="jack">Jack</Option>
      <Option value="lucy">Lucy</Option>
      <Option value="disabled" disabled>Disabled</Option>
      <Option value="Yiminghe">yiminghe</Option>
    </Select>
                    <Form 
                        schema={this.props.formSchema}
                        uiSchema={this.props.formUiSchema} 
                        formData={this.props.formData}
                        //validate={this.validate}
                        onSubmit={()=> this.props.actions.orgCloseAddOrganizationModal()}
                        widgets={widgets}
                        />
                </Modal> 
                    </div>
              </div>
              {/* this.props.location.pathname */}
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    orgTree         : state.org1.org1Tree,
    orgInfo         : state.org1.org1Organization,
    orgPersonal     : {
        columns : state.person.columns
    },
    orgVechicles    : {
        columns : state.vechicles.columns
    },
    showAddModal: state.org1.showAddModal,
    formSchema: state.org1.schema,
    formUiSchema: state.org1.uiSchema,
    formData: {}
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Organizations);
