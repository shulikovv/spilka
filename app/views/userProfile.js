import React from 'react';
import {Link} from 'react-router';
import { Button, Modal, Popover, Tooltip, OverlayTrigger  } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions';
import { Tabs } from 'antd';
const TabPane = Tabs.TabPane;
import Form from "react-jsonschema-form";

class UserProfile extends React.Component {
    render() {
        function callback(key) {
        console.log(key);
        }
        return (
            <div>
                {/* start: PAGE TITLE */}
                <section id='page-title'>
                    <div className='row'>
                        <div className='col-sm-8'>
                            <h1 className='mainTitle'>USER PROFILE</h1>
                            <span className='mainDescription'>There are many systems which have a need for user profile pages which display personal information on each member.</span>
                        </div>
                        <ol className='breadcrumb'>
                            <li>
                                <span>Pages</span>
                            </li>
                            <li className='active'>
                                <span>Blank Page</span>
                            </li>
                        </ol>
                    </div>
                </section>
                {/* end: PAGE TITLE */}
                <div className='container-fluid container-fullw bg-white'>
                    <Tabs defaultActiveKey="1" onChange={callback}>
                        <TabPane tab="Edit profile" key="1">
                            <Form 
                            schema={this.props.profileSchema}
                            uiSchema={this.props.profileUiSchema} 
                            formData={this.props.profileData}
                            />
                        </TabPane>
                        <TabPane tab="Change password" key="2">
                            <Form 
                            schema={this.props.passwordScema}
                            uiSchema={this.props.passwordUiSchema} 
                            formData={this.props.passwordData}
                            />
                        </TabPane>
                    </Tabs>
                </div>
            </div>

        );
    }
}

const mapStateToProps = (state) => ({
    profileSchema   : state.user.profileSchema,
    profileUiSchema: {},
    profileData: {},
    passwordScema : state.user.changePasswordScema,
    passwordUiSchema: {},
    passwordData: {}
});

const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
