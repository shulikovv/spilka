import React from 'react';
import {Link, browserHistory} from 'react-router';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/EvensActions';
import _ from 'lodash';
import { Table, Icon, Button, Popconfirm } from 'antd';



class Events extends React.Component {

    constructor(props) {
        super(props);

        this.onCustom = this.onCustom.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
            this.dataSource = [
                {
                    key: 1,
                    eventRecordNumber: 1,
                    eventTitle: 'Alleged extrajudicial killings of three brothers, including a minor, namely Mr. Eric Miraflores, Mr. Raymond Miraflores and Rosmil Miraflores',
                    confidentiality: true,
                    geographicalTerm: "Philippines",
                    initialDate: '2010-06-02',
                    dateUpdated: '2014-04-04',
                    updatedBy: 'admin'
                },
                {
                    key: 2,
                    eventRecordNumber: 2,
                    eventTitle: 'Alleged extrajudicial killings of three brothers, including a minor, namely Mr. Eric Miraflores, Mr. Raymond Miraflores and Rosmil Miraflores',
                    confidentiality: true,
                    geographicalTerm: "Philippines",
                    initialDate: '2010-06-02',
                    dateUpdated: '2014-04-04',
                    updatedBy: 'admin'
                },
                {
                    key: 3,
                    eventRecordNumber: 3,
                    eventTitle: 'Alleged extrajudicial killings of three brothers, including a minor, namely Mr. Eric Miraflores, Mr. Raymond Miraflores and Rosmil Miraflores',
                    confidentiality: true,
                    geographicalTerm: "Philippines",
                    initialDate: '2010-06-02',
                    dateUpdated: '2014-04-04',
                    updatedBy: 'admin'
                },
            ];

            this.columns = [
            {
            title: 'Event Record Number',
            dataIndex: 'eventRecordNumber'
            },
            {
            title: 'Event Title',
            dataIndex: 'eventTitle',
            render: (text, record) => <a onClick={()=>  this.props.actions.eventView(record.eventRecordNumber)}>{text}</a>
            },
            {
            title: 'Confidentiality',
            dataIndex: 'confidentiality'
            },
            {
            title: 'Geographical Term',
            dataIndex: 'geographicalTerm'
            },
            {
            title: 'Initial Date',
            dataIndex: 'initialDate'
            },
            {
            title: 'Final Date',
            dataIndex: 'dateUpdated'
            },
            {
                title: 'Actions',
                key: 'actions',
                render: (text, record) => (
                    <span>
                    <a onClick={()=>  this.props.actions.eventView(record.eventRecordNumber)}>View</a>
                    <span className="ant-divider" />
                    <a onClick={()=> this.props.actions.eventEdit(record.eventRecordNumber)}>Edit</a>
                    <span className="ant-divider" />
                    <Popconfirm title="Sure to delete?" onConfirm={() => console.log(record.name, 'delete')}>
                        <a>Delete</a>
                    </Popconfirm>
                    </span>
                )
            },
        ];

    }

    


    onCustom(e) {
        console.log('Custom ------->',e);
    }

    componentWillMount() {
        const { routes } = this.props; // array of routes
        const { router } = this.context;

        // check if user data available
        /*const user = JSON.parse(localStorage.getItem('user'));
        if (!user) {
            // redirect to login if not
            router.push('/login');
        }*/

        // get all roles available for this route
        const routeRoles = _.chain(routes)
            .filter(item => item.authorize) // access to custom attribute
            .map(item => item.authorize)
            .flattenDeep()
            .value();

        console.log('EWM-->',routes,this.context,routeRoles);

        // compare routes with user data
        /*if (_.intersection(routeRoles, user.roles).length === 0) {
            router.push('/not-authorized');
        }*/
    }

    componentDidMount() {
        console.log('Events mounted', this.props);
    }

    callback(key) {
        console.log(key);
    }

    render () {
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
              {/* start: PAGE TITLE */}
                <section id='page-title'>
                    <div className='row'>
                    <div className='col-sm-8'>
                        <h1 className='mainTitle'>Events Page</h1>
                        <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                    </div>
                    <ol className='breadcrumb'>
                        <li>
                        <span>Pages</span>
                        </li>
                        <li className='active'>
                        <span>Blank Page</span>
                        </li>
                    </ol>
                    </div>
                </section>
                {/* end: PAGE TITLE */}
                <div className="panel-body">
                    <div className="row">
                        <div className="col-md-12">
                            <div style={{ marginBottom: 16 }}>
                                <Button
                                    type="primary"
                                    icon="plus"
                                    onClick={()=> this.props.actions.addEvent()}
                                >
                                    Add New Event
                                </Button>

                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <Table rowSelection={rowSelection} dataSource={this.dataSource} columns={this.columns} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    form         : '',
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Events);