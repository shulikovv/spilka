import React from 'react';
import {Link} from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//import * as actionCreators from '../actions';
import * as actionCreators from '../actions/event';
import * as formAddDctionCreators from '../actions/person/FormAddActions';

import Form from "react-jsonschema-form";
import Victims from "../components/Events/victims";
import Sources from "../components/Events/sources";
import Interventions from "../components/Events/interventions";
import ChainOfEvents from "../components/Events/chainOfEvents";
import Documents from "../components/Events/documents";
import AuditLog from "../components/Events/auditLog";
import Permissions from "../components/Events/Permissions";
import EventForm from "../components/Events/eventForm";
import ViewFormButtons from "../components/Events/viewFormButtons1";
import { Tabs } from 'antd';
const TabPane = Tabs.TabPane;


import VictimActPerpetratorInvolvementForm from '../components/Events/act/victimActPerpetratorInvolvementForm';

class eventsAddAct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            model: {},
            errorSchema: {
              __errors: [],
                password: {
                  __errors:[
                      "Passwords don't match"
                  ]
                }
            },
            errors: [
                {
                    stack: 'password: Passwords don\'t match'
                }
            ]
        };
        this.onSubmit = ({formData}) => console.log("yay I'm valid!",formData);
        this.schema = {
            type: "object",
            properties: {
                foo: {
                    type: "object",
                    properties: {
                        bar: {type: "string"}
                    }
                },
                baz: {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            description: {
                                "type": "string"
                            }
                        }
                    }
                },
                test: {
                    "type": "string",
                    enum: [
                        "asdff",
                        "ssjksjlds",
                        "sdkldks;"
                    ]
                },
                "age": {
                    "title": "Age",
                    "type": "number",
                    "minimum": 18
                }
            }
        }
    
        this.uiSchema = {
            foo: {
                bar: {
                    "ui:widget": "textarea"
                },
            },
            baz: {
                // note the "items" for an array
                items: {
                    description: {
                        "ui:widget": "textarea"
                    }
                }
            },
            test: {
                "ui:widget": "select"
            }
        }
        this.formlyConfig = {
            name: 'myFormly',
            fields: [
                {
                    key: 'otherFrameworkOrLibrary',
                    type: 'text',
                    data: {
                        label: 'What are you building with?',
                    }
                }
            ]
        }
    
    }


    logg(type) {
        console.log('*******',type);
    }



    onFormlyUpdate(model) {
        this.setState({model: model});
    }

    ErrorListTemplate(props) {
        const {errors} = props;
        return (
            <div>
                {errors.map((error, i) => {
                    return (
                        <li key={i}>
                            {error.stack}
                        </li>
                    );
                })}
            </div>
        );
    }


    componentWillMount() {
        this.props.formAddActions.getPersonForm();
        this.props.actions.getPersonListForAct();
    }

    render () {
        console.log('events edit -->', this.props);
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Add act page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
                {/* end: PAGE TITLE */}
                <div className="panel-body">
                    <div className="row">
                        <div className="col-mb-12">
                            <VictimActPerpetratorInvolvementForm {...this.props}/>
                        </div>
                    </div>
                </div>    
            </div>
        );
    }

}


const mapStateToProps = (state) => ({
    form         : '',
    personForm   : state.personForm.form,
    personslist  : state.eventAct.personslist,
    actReturnPath: state.eventAct.returnpath,
    victim : {
        data: state.eventAct.act.victim,
        victimForm: {
            schema: state.eventAct.victimForm.schema,
            uiSchema: state.eventAct.victimForm.uiSchema,
        }
    }
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch),
    formAddActions : bindActionCreators(formAddDctionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(eventsAddAct);