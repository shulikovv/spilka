import React from 'react';
import {Link} from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/organizations/EditFormActions';

import Form from "react-jsonschema-form";

import {widgets} from "../elements/form/widgets";
import axios from 'axios';
import qs from 'qs';

class organizationAddForm extends React.Component {
    constructor(props) {
        super(props);

        this.onSubmit = ({formData}) => {
            this.props.actions.submitEditOrgForm(formData);
        };
    }

    componentWillMount() {
        console.log('organizationEditForm --> props --> ',this.props);
        this.props.actions.getOrgEditForm(this.props.params.orgID);
        axios.get('/api/organization/get-user', {
            headers: {
                Authorization: 'Bearer '+ localStorage.getItem('token')
            }
        })
        .then((resp)=>{
            console.log('resp',resp);
        })
    }
  
    logg(type) {
        console.log('*******',type);
    }

    render () {
        console.log('events edit -->', this.props);
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Organization Edit Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
                {/* end: PAGE TITLE */}
                <div className="panel-body">
                    <div className="row">
                        <div className="col-mb-12">
                            <Form schema={this.props.form.schema}
                            uiSchema={this.props.form.uiSchema}
                            formData={this.props.form.data}
                            widgets={widgets}
                            onSubmit={this.onSubmit}
                            noValidate={true}
                            ></Form>
                        </div>
                    </div>
                </div>    
            </div>
        );
    }

}


const mapStateToProps = (state) => ({
    form      :  state.orgAddForm.form,
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(organizationAddForm);