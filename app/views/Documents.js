import React from 'react';
import {Link} from 'react-router';
import _ from 'lodash';
import {Table, Icon, Button, Popconfirm, Modal} from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/document/DocumentActions';
import Form from "react-jsonschema-form";
import { Alert } from 'antd';
import {widgets} from "../elements/form/widgets";

class DocumentsView extends React.Component {

    constructor(props) {
        super(props);
        this.dataSource = [

        ];

    }

    componentWillMount() {
        this.props.actions.getDocumentsList();
        this.props.actions.getDocumentForm();
    }

    componentWillUpdate(nextProps, nextState) {

    }

    render() {
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        this.columns = [
           ...this.props.docs.table.columns,
            {
                title: 'Действия',
                key: 'actions',
                render: (text, record) => (
                    <span>
                  <a onClick={() => console.log(record.eventRecordNumber)}>Смотреть</a>
                  <span className="ant-divider"/>
                  <a onClick={() => console.log(record.eventRecordNumber)}>Редактировать</a>
                  <span className="ant-divider"/>
                  <Popconfirm title="Sure to delete?" onConfirm={() => console.log(record.name, 'delete')}>
                    <a>Удалить</a>
                  </Popconfirm>
                </span>
                )
            },
        ];
        return (
            <div>
              <style>{`.ant-modal-mask, .ant-modal-wrap { z-index: 1000000 !important;}`}</style>
                {/* start: PAGE TITLE */}
                <section id='page-title'>
                    <div className='row'>
                        <div className='col-sm-8'>
                            <h1 className='mainTitle'>Документы</h1>
                            <span className='mainDescription'>Все документы системы</span>
                        </div>
                        <ol className='breadcrumb'>
                            <li>
                                <span>Pages</span>
                            </li>
                            <li className='active'>
                                <span>Blank Page</span>
                            </li>
                        </ol>
                    </div>
                </section>
                {/* end: PAGE TITLE */}
                <div className="panel-body">
                    <div className="row">
                        <div className="col-md-12">
                            <div style={{marginBottom: 16}}>
                                <Button
                                    type="primary"
                                    icon="plus"
                                    onClick={() => this.props.actions.addDocument()}
                                >
                                    Добавить новый документ
                                </Button>

                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <Table rowKey='doc_id' rowSelection={rowSelection} dataSource={this.props.docs.table.data} columns={this.columns}/>
                        </div>
                    </div>
                </div>
                {/* this.props.location.pathname */}
                <Modal
                    title="Edit Address for User"
                    visible={this.props.docs.modal.visible}
                    footer={null}
                    onCancel={()=> this.props.actions.closeOpenedModal()}
                    >
                    <Form
                      schema={this.props.docs.form.schema}
                      uiSchema={this.props.docs.form.uiSchema}
                      widgets={widgets}
                      formData={this.props.docs.form.data}
                      >
                      dghgdgjdgjdgjdf!!!!
                    </Form>
                </Modal>
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    //treeDefaultSelectedKeys: state.orgForm.treeDefaultSelectedKeys
    docs: state.docs
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(DocumentsView);
