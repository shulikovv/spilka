import React from 'react';
import {Link} from 'react-router';
import CivilForm from "../../components/Vechicle/civilVechcleForm";

export default class HomeView extends React.Component {

    constructor(props) {
        super(props);

    }

    render () {
        console.log('events edit -->', this.props);
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Add Civil Vechicle Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
              {/* end: PAGE TITLE */}
              <CivilForm loading={false}/>
            </div>
        );
    }

}
