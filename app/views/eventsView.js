import React from 'react';
import {Link} from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions';
import * as eventActionCreators from '../actions/event';

import Form from "react-jsonschema-form";
import Victims from "../components/Events/victims";
import Sources from "../components/Events/sources";
import Interventions from "../components/Events/interventions";
import ChainOfEvents from "../components/Events/chainOfEvents";
import Documents from "../components/Events/documents";
import AuditLog from "../components/Events/auditLog";
import Permissions from "../components/Events/Permissions";
import EventForm from "../components/Events/eventForm";
import ViewFormButtons from "../components/Events/viewFormButtons1";
import { Tabs } from 'antd';

const TabPane = Tabs.TabPane;




class eventsView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            model: {},
            errorSchema: {
              __errors: [],
                password: {
                  __errors:[
                      "Passwords don't match"
                  ]
                }
            },
            errors: [
                {
                    stack: 'password: Passwords don\'t match'
                }
            ]
        };

        this.schema = {
            type: "object",
            properties: {
                foo: {
                    type: "object",
                    properties: {
                        bar: {type: "string"}
                    }
                },
                baz: {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            description: {
                                "type": "string"
                            }
                        }
                    }
                },
                test: {
                    "type": "string",
                    enum: [
                        "asdff",
                        "ssjksjlds",
                        "sdkldks;"
                    ]
                },
                "age": {
                    "title": "Age",
                    "type": "number",
                    "minimum": 18
                }
            }
        }
    
        this.uiSchema = {
            foo: {
                bar: {
                    "ui:widget": "textarea"
                },
            },
            baz: {
                // note the "items" for an array
                items: {
                    description: {
                        "ui:widget": "textarea"
                    }
                }
            },
            test: {
                "ui:widget": "select"
            }
        }
        this.onSubmit = ({formData}) => console.log("yay I'm valid!",formData);
        
        this.formlyConfig = {
            name: 'myFormly',
            fields: [
                {
                    key: 'otherFrameworkOrLibrary',
                    type: 'text',
                    data: {
                        label: 'What are you building with?',
                    }
                }
            ]
        }
    
    }


    logg(type) {
        console.log('*******',type);
    }



    onFormlyUpdate(model) {
        this.setState({model: model});
    }

    ErrorListTemplate(props) {
        const {errors} = props;
        return (
            <div>
                {errors.map((error, i) => {
                    return (
                        <li key={i}>
                            {error.stack}
                        </li>
                    );
                })}
            </div>
        );
    }

    
    componentWillMount() {
        this.props.eventActions.getEventForm();
    }
    

    render () {
        console.log('events edit -->', this.props);
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Event View Page</h1>
                    <span className='mainDescription'>Use this page to start from scratch and put your custom content</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
                {/* end: PAGE TITLE */}
                <div className="panel-body">
                    <div className="row">
                        <div className="col-mb-12">
                            <Tabs defaultActiveKey="1">
                                <TabPane tab="Events description" key="1">
                                    <ViewFormButtons actions={this.props.actions} eventId={this.props.params.eventID} />
                                    <EventForm loading={false} {...this.props} actions={this.props.eventActions} readonly={true}></EventForm>
                                </TabPane>
                                <TabPane tab="Victims and Perpetrators" key="2">
                                    <Victims olo="test"  {...this.props} actions={this.props.eventActions} />
                                </TabPane>
                                <TabPane tab="Sources" key="3"><Sources/></TabPane>
                                <TabPane tab="Interventions" key="4"><Interventions/></TabPane>
                                <TabPane tab="Chain of Events" key="5"><ChainOfEvents/></TabPane>
                                <TabPane tab="Documents" key="6"><Documents/></TabPane>
                                <TabPane tab="Audit log" key="7"><AuditLog/></TabPane>
                                <TabPane tab="Permissions" key="8"><Permissions/></TabPane>
                            </Tabs>
                        </div>
                    </div>
                </div>    
            </div>
        );
    }

}


const mapStateToProps = (state) => ({
    form         : state.eventForm.form,
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(actionCreators, dispatch),
    eventActions : bindActionCreators(eventActionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(eventsView);