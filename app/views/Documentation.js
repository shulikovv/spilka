import React from 'react';
import {Link} from 'react-router';
//const Grid = KendoGridReactWrapper.Grid;
import DayPicker from 'react-day-picker';
import { Upload, message, Button, Icon } from 'antd';
import AvatarEditor from 'react-avatar-editor';

const gridOptions = {
  dataSource: {
      type: "odata",
      transport: {
          read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Orders"
      },
      schema: {
          model: {
              fields: {
                  OrderID: { type: "number" },
                  Freight: { type: "number" },
                  ShipName: { type: "string" },
                  OrderDate: { type: "date" },
                  ShipCity: { type: "string" }
              }
          }
      },
      pageSize: 20,
      serverPaging: true,
      serverFiltering: true,
      serverSorting: true
  },
  height: 550,
  filterable: true,
  sortable: true,
  pageable: true,
  columns: [{
          field:"OrderID",
          filterable: false
      },
      "Freight",
      {
          field: "OrderDate",
          title: "Order Date",
          format: "{0:MM/dd/yyyy}"
      }, {
          field: "ShipName",
          title: "Ship Name"
      }, {
          field: "ShipCity",
          title: "Ship City"
      }
  ]
}

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return false;//isJPG && isLt2M;
}

class MyUpload extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }

        this.beforeUpload = (file) => {
            const isJPG = file.type === 'image/jpeg';
            if (!isJPG) {
              message.error('You can only upload JPG file!');
            }
            const isLt2M = file.size / 1024 / 1024 < 2;
            if (!isLt2M) {
              message.error('Image must smaller than 2MB!');
            }
            console.log('bu-1',file);
            getBase64(file, imageUrl => this.setState({ imageUrl }));
            console.log('bu-2', this.state);
            return false;//isJPG && isLt2M;
        }

        this.handleChange = (info) => {
            if (info.file.status === 'done') {
              // Get this url from response in real world.
              getBase64(info.file.originFileObj, imageUrl => this.setState({ imageUrl }));
            }
        }
    }


    render () {
        const imageUrl = this.state.imageUrl;
        const props = {
            name: 'file',
            action: '//jsonplaceholder.typicode.com/posts/',
            processData: false,
            headers: {
              authorization: 'authorization-text',
            },
            listType:"picture-card",
            beforeUpload: (file) => {
                return false;
              },
            onChange(info) {
              if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
              }
              if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
              } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
              }
            },
          };
        console.log('state is', this.state)
        return (<div>
            <Upload
                className="avatar-uploader"
                name="avatar"
                showUploadList={false}
                action="//jsonplaceholder.typicode.com/posts/"
                beforeUpload={this.beforeUpload}
                onChange={this.handleChange}

            >
                {
                imageUrl ?
                    <img src={imageUrl} alt="" className="avatar" /> :
                    <Icon type="plus" className="avatar-uploader-trigger" style={{display: 'table-cell',verticalAlign: 'middle'}} />
                }
            </Upload>
              </div>
        )
    }
}

export default class HomeView extends React.Component {
    
    render () {
        return (
            <div>
              {/* start: PAGE TITLE */}
              <section id='page-title'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <h1 className='mainTitle'>Документация</h1>
                    <span className='mainDescription'>Краткое описание работы с системой</span>
                  </div>
                  <ol className='breadcrumb'>
                    <li>
                      <span>Pages</span>
                    </li>
                    <li className='active'>
                      <span>Blank Page</span>
                    </li>
                  </ol>
                </div>
              </section>
              {/* end: PAGE TITLE */}
              Hello
              {/*<MyUpload></MyUpload>
              <Link to='/reports/rep1'> Rep test </Link>*/}
              {/*<Grid {...gridOptions}/>*/}
              {/* this.props.location.pathname */}
            </div>
        );
    }

}
