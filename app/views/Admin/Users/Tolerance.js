import React from 'react';
import {Link, browserHistory} from 'react-router';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Griddle from 'griddle-react';
import StructuredFilter from 'react-structured-filter';
import { Table, Icon, Button, Popconfirm, Modal, Alert } from 'antd';

import Form from "react-jsonschema-form";
import * as actionCreators from '../../../actions';
import Thesauri from '../../Thesauri';
import {findIndex} from 'lodash';

class Tolerance extends React.Component {
    constructor(props) {
        super(props);


    }

    componentWillMount() {
        this.props.admActions.loadUsersList();
        let index = findIndex(this.props.columns, {key: 'actions'});
        if (index === -1) {
            this.props.columns.push({
                title: 'Actions',
                key: 'actions',
                render: (text, record) => (
                    <span>
                    <a onClick={()=> this.props.admActions.viewTolerance(record.tolerance_id)}>View</a>
                    {/*<span className="ant-divider" />
                    <a onClick={()=> this.props.admActions.editUser(record.id)}>Edit</a>
                    <span className="ant-divider" />
                    <Popconfirm title="Sure to delete?" onConfirm={() => this.props.admActions.deleteUser(record.id)}>
                        <a>Delete</a>
                    </Popconfirm>*/}
                    </span>
                )
            })
        }
    }

    validate(formData, errors) {
        if (formData.password !== formData.password2) {
            errors.password2.addError("Passwords don't match");
        }
        return errors;
        }

    render () {
        console.log('))))))55',this.props);
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
            <style>{`.ant-modal-mask, .ant-modal-wrap { z-index: 1000000;}`}</style>
                {/* start: PAGE TITLE */}
                <section id='page-title'>
                    <div className='row'>
                        <div className='col-sm-8'>
                            <h1 className='mainTitle'>Tolerance levels Page</h1>
                            <span className='mainDescription'>Levels of access to documents in the system. To hide part of the information in the document.</span>
                        </div>
                    </div>
                </section>
            {/* end: PAGE TITLE */}
            <div className="panel-body">
                <div className="row">
                                <div className="col-md-12">
                                    <div style={{ marginBottom: 16 }}>
                                        <Button
                                            type="primary"
                                            icon="plus"
                                            onClick={()=> this.props.admActions.addTolerance()}
                                        >
                                            Add New Tolerance Level
                                        </Button>

                                    </div>
                                </div>
                            </div>
                <div className="row">
                    <div className="col-md-12">
                        <Table rowKey="tolerance_id" loading={this.props.loading}  rowSelection={rowSelection} dataSource={this.props.list} columns={this.props.columns} />
                    </div>
                </div>
                <Modal
                    title="View Tolerance"
                    visible={this.props.showViewTolModal}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.props.admActions.closeviewToleranceModal()}
                    >
                    <Form 
                        schema={this.props.viewFormSchema}
                        uiSchema={this.props.viewFormUiSchema} 
                        formData={this.props.viewFormData}
                        //validate={this.validate}
                        onSubmit={()=> this.props.admActions.closeviewToleranceModal()}
                        />
                </Modal>
                <Modal
                    title="Create new Tolerance level"
                    visible={this.props.showAddTolModal}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.props.admActions.closeaddToleranceModal()}
                    >
                    <Alert
                        message="Informational Notes"
                        description="Currently, the Tolerance Level Creation feature is disabled."
                        type="info"
                        showIcon
                    />
                </Modal>                                
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    list   : state.permission.tolerance.list,
    columns : state.permission.tolerance.columns,
    loading: false,//state.users.isLoading,
    showAddTolModal: state.permission.tolerance.showAddModal,
    //showEditUserModal: state.users.showEditUserModal,
    showViewTolModal: state.permission.tolerance.showViewModal,

    viewFormData: state.permission.tolerance.viewFormData,
    //addFormSchema: state.users.addFormSchema,
    //editFormSchema: state.users.editFormSchema,
    viewFormSchema: state.permission.tolerance.viewFormSchema,
    viewFormUiSchema: state.permission.tolerance.viewFormUiSchema,
});

const mapDispatchToProps = (dispatch) => ({
  admActions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Tolerance);