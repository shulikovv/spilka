import React from 'react';
import {Link, browserHistory} from 'react-router';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Griddle from 'griddle-react';
import StructuredFilter from 'react-structured-filter';
import { Table, Icon, Button, Popconfirm, Modal, Alert } from 'antd';

import Form from "react-jsonschema-form";
import * as actionCreators from '../../../actions';
import Thesauri from '../../Thesauri';
import {findIndex} from 'lodash';

class Roles extends React.Component {
    constructor(props) {
        super(props);


    }

    componentWillMount() {
        this.props.admActions.loadUsersList();
        let index = findIndex(this.props.columns, {key: 'actions'});
        if (index === -1) {
            this.props.columns.push({
                title: 'Actions',
                key: 'actions',
                render: (text, record) => (
                    <span>
                    <a onClick={()=> this.props.admActions.viewRole(record.role_id)}>View</a>
                    {/*<span className="ant-divider" />
                    <a onClick={()=> this.props.admActions.editUser(record.id)}>Edit</a>
                    <span className="ant-divider" />
                    <Popconfirm title="Sure to delete?" onConfirm={() => this.props.admActions.deleteUser(record.id)}>
                        <a>Delete</a>
                    </Popconfirm>*/}
                    </span>
                )
            })
        }
    }

    validate(formData, errors) {
        if (formData.password !== formData.password2) {
            errors.password2.addError("Passwords don't match");
        }
        return errors;
    }

    render () {
        console.log('))))))55',this.props);
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
                <style>{`.ant-modal-mask, .ant-modal-wrap { z-index: 1000000;}`}</style>
                {/* start: PAGE TITLE */}
                <section id='page-title'>
                    <div className='row'>
                        <div className='col-sm-8'>
                            <h1 className='mainTitle'>Roles of Users Page</h1>
                            <span className='mainDescription'>Use this page to manage users who can login into system</span>
                        </div>
                    </div>
                </section>
                {/* end: PAGE TITLE */}
                <div className="panel-body">
                    <div className="row">
                        <div className="col-md-12">
                            <div style={{ marginBottom: 16 }}>
                                <Button
                                    type="primary"
                                    icon="plus"
                                    onClick={()=> this.props.admActions.addRole()}
                                >
                                    Add New Role
                                </Button>

                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <Table rowKey="role_id" loading={this.props.loading}  rowSelection={rowSelection} dataSource={this.props.list} columns={this.props.columns} />
                        </div>
                    </div>
                <Modal
                    title="View Role"
                    visible={this.props.showViewModal}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.props.admActions.closeviewRoleModal()}
                    >
                    <Form 
                        schema={this.props.viewFormSchema}
                        uiSchema={this.props.viewFormUiSchema} 
                        formData={this.props.viewFormData}
                        //validate={this.validate}
                        onSubmit={()=> this.props.admActions.closeviewRoleModal()}
                        />
                </Modal>
                <Modal
                    title="Create new Role"
                    visible={this.props.showAddModal}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.props.admActions.closeaddRoleModal()}
                    >
                    <Alert
                        message="Informational Notes"
                        description="Currently, the Role Creation feature is disabled."
                        type="info"
                        showIcon
                    />
                </Modal> 
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    list   : state.permission.roles.list,
    columns : state.permission.roles.columns,
    loading: false,//state.users.isLoading,
    showAddModal: state.permission.roles.showAddModal,
    //showEditUserModal: state.users.showEditUserModal,
    showViewModal: state.permission.roles.showViewModal,
    viewFormData: state.permission.roles.viewFormData,
    //addFormSchema: state.users.addFormSchema,
    //editFormSchema: state.users.editFormSchema,
    viewFormSchema: state.permission.roles.viewFormSchema,
    viewFormUiSchema: state.permission.roles.viewFormUiSchema,
});

const mapDispatchToProps = (dispatch) => ({
    admActions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Roles);