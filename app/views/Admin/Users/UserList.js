import React from 'react';
import {Link, browserHistory} from 'react-router';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Griddle from 'griddle-react';
import StructuredFilter from 'react-structured-filter';
import { Table, Icon, Button, Popconfirm, Modal } from 'antd';
import 'antd/lib/table/style/css';

import Form from "react-jsonschema-form";
import * as actionCreators from '../../../actions';
import Thesauri from '../../Thesauri';
import {findIndex} from 'lodash';

class UsersList extends React.Component {
    constructor(props) {
        super(props);


    }

    componentWillMount() {
        this.props.admActions.loadUsersList();
        let index = findIndex(this.props.columns, {key: 'actions'});
        if (index === -1) {
            this.props.columns.push({
                title: 'Actions',
                key: 'actions',
                render: (text, record) => (
                    <span>
                    <a onClick={()=> this.props.admActions.viewUser(record.id)}>View</a>
                    <span className="ant-divider" />
                    <a onClick={()=> this.props.admActions.editUser(record.id)}>Edit</a>
                    <span className="ant-divider" />
                    <Popconfirm title="Sure to delete?" onConfirm={() => this.props.admActions.deleteUser(record.id)}>
                        <a>Delete</a>
                    </Popconfirm>
                    </span>
                )
            })
        }
    }

    validate(formData, errors) {
        if (formData.password !== formData.password2) {
            errors.password2.addError("Passwords don't match");
        }
        return errors;
        }

    render () {
        console.log('))))))55',this.props);
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
            <style>{`.ant-modal-mask, .ant-modal-wrap { z-index: 1000000  !important;}`}</style>
                {/* start: PAGE TITLE */}
                <section id='page-title'>
                    <div className='row'>
                        <div className='col-sm-8'>
                            <h1 className='mainTitle'>Users Page</h1>
                            <span className='mainDescription'>Use this page to manage users who can login into system</span>
                        </div>
                    </div>
                </section>
            {/* end: PAGE TITLE */}
            <div className="panel-body">
                <div className="row">
                                <div className="col-md-12">
                                    <div style={{ marginBottom: 16 }}>
                                        <Button
                                            type="primary"
                                            icon="plus"
                                            onClick={()=> this.props.admActions.addUser()}
                                        >
                                            Add New User
                                        </Button>

                                    </div>
                                </div>
                            </div>
                <div className="row">
                    <div className="col-md-12">
                        <Table rowKey="id" loading={this.props.loading}  rowSelection={rowSelection} dataSource={this.props.users_list} columns={this.props.columns} />
                    </div>
                </div>
                <Modal
                    title="Create new User"
                    visible={this.props.showAddUserModal}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.props.admActions.closeAddUserModal()}
                    >
                    <Form 
                        schema={this.props.addFormSchema} 
                        validate={this.validate}
                        onSubmit={({formData})=> this.props.admActions.submitAddUserModal(formData)}
                        />
                </Modal>
                <Modal
                    title="View User"
                    visible={this.props.showViewUserModal}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.props.admActions.closeViewUserModal()}
                    >
                    <Form 
                        schema={this.props.viewFormSchema}
                        uiSchema={this.props.viewFormUiSchema} 
                        formData={this.props.viewFormData}
                        validate={this.validate}
                        onSubmit={()=> this.props.admActions.closeViewUserModal()}
                        />
                </Modal>
                <Modal
                    title="Edit new User"
                    visible={this.props.showEditUserModal}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.props.admActions.closeEditUserModal()}
                    >
                    <Form 
                        schema={this.props.editFormSchema} 
                        formData={this.props.viewFormData}
                        onSubmit={({formData})=> this.props.admActions.submitEditUserModal(formData)}
                        />
                </Modal> 
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    users_list   : state.users.list,
    columns : state.users.listCollumn,
    loading: state.users.isLoading,
    showAddUserModal: state.users.showAddUserModal,
    showEditUserModal: state.users.showEditUserModal,
    showViewUserModal: state.users.showViewUserModal,
    viewFormData: state.users.viewFormData,
    addFormSchema: state.users.addFormSchema,
    editFormSchema: state.users.editFormSchema,
    viewFormSchema: state.users.viewFormSchema,
    viewFormUiSchema: state.users.viewFormUiSchema,
});

const mapDispatchToProps = (dispatch) => ({
  admActions : bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);