import React, {PropTypes, Component} from 'react';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete, DatePicker,Upload } from 'antd';
const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
import is from 'is_js';

import axios from 'axios';
//import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

const residences = [{
  value: 'zhejiang',
  label: 'Zhejiang',
  children: [{
    value: 'hangzhou',
    label: 'Hangzhou',
    children: [{
      value: 'xihu',
      label: 'West Lake',
    }],
  }],
}, {
  value: 'jiangsu',
  label: 'Jiangsu',
  children: [{
    value: 'nanjing',
    label: 'Nanjing',
    children: [{
      value: 'zhonghuamen',
      label: 'Zhong Hua Men',
    }],
  }],
}];

class RegistrationForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      confirmDirty: false,
      autoCompleteResult: [],
      formLayout: 'vertical',
      types: [],
    };

    this.handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
          const {fileList} = this.state;
          const formData = new FormData();
          fileList.forEach((file) => {
              console.log('file');
              formData.append('file', file);
          });
          for (let variable in values) {
            if (values.hasOwnProperty(variable) && is.existy(values[variable])) {
              formData.append(variable, values[variable]);
            }
          }

          this.setState({
              uploading: true,
          });

          axios.post('/api/document/store-document', formData, {
              headers: {
                  Authorization: 'Bearer ' + localStorage.getItem('token')
              }
          })
              .then((resp) => {
                  console.log('resp', resp);
                  if (resp.data.status == 'ok') {
                      console.log(resp.data);
                      this.setState({
                          fileList: [],
                          uploading: false,
                      });
                      //dispatch(personFormSavedOk());
                      //dispatch(push('/persons'));
                  } else if (resp.data.status == 'error') {
                      this.setState({
                          uploading: false,
                      });
                      console.log(resp.data.errors);
                      //dispatch(getErrorWhenAdd(resp.data.errors))
                  }
              })
        }
      });
    }
    this.handleConfirmBlur = (e) => {
      const value = e.target.value;
      this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }

  }

  componentWillMount() {
    axios.get('/api/thesaurus/16', {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token')
        }
    })
    .then((resp) => {
        console.log('resp', resp);
        if (resp.status==200) {
          this.setState({types:resp.data})
        }
    })
  }


  render() {
    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;
    const { formLayout } = this.state;
    const Types = this.state.types.map((item)=>{
       return <Option value={item.vocab_number} key={item.vocab_number}>{item.english}</Option>
    })
    const formItemLayout = formLayout === 'horizontal' ? {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    }:null;
    const tailFormItemLayout = formLayout === 'horizontal' ? {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    }: null;

    const props = {
        multiple: false,
        action: '',
        onRemove: (file) => {
            this.setState(({fileList}) => {
                const index = fileList.indexOf(file);
                const newFileList = fileList.slice();
                newFileList.splice(index, 1);
                return {
                    fileList: newFileList,
                };
            });
        },
        beforeUpload: (file) => {
            //formData = {...formData, file: file};
            this.setState(({fileList}) => ({
                fileList: [file],
            }));
            return false;
        },
        fileList: this.state.fileList,
    };

    return (
      <Form onSubmit={this.handleSubmit} layout={formLayout}>
        <fieldset>
            <FormItem
              {...formItemLayout}
              label={(
                <span>
                  Название документа&nbsp;
                  <Tooltip title="Заголовок документа">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              )}
              hasFeedback
            >
              {getFieldDecorator('title', {
                rules: [{ required: true, message: 'Пожалуйста введите заголовок документа!', whitespace: true }],
              })(
                <Input />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label={(
                <span>
                  Автор&nbsp;
                  <Tooltip title="Автор...">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              )}
              hasFeedback
            >
              {getFieldDecorator('creator', {
                rules: [{ required: false, message: 'Пожалуйста укажите автора документа!', whitespace: true }],
              })(
                <Input />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label={(
                <span>
                  Описание&nbsp;
                  <Tooltip title="Описание...">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              )}
              hasFeedback
            >
              {getFieldDecorator('description', {
                rules: [{ required: false, message: 'Пожалуйста опишите документ!', whitespace: true }],
              })(
                <TextArea />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label={(
                <span>
                  Дата создания&nbsp;
                  <Tooltip title="Дата...">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              )}
              hasFeedback
            >
              {getFieldDecorator('datecreated', {
                rules: [{ required: false, message: 'Пожалуйста укажите когда был создан документ!' }],
              })(
                <DatePicker
                  style={{ width: "100%" }}
                />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label={(
                <span>
                  Дата подтверждения&nbsp;
                  <Tooltip title="Дата...">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              )}
              hasFeedback
            >
              {getFieldDecorator('datesubmitted', {
                rules: [{ required: false, message: 'Пожалуйста укажите когда был подтвержден документ!' }],
              })(
                  <DatePicker
                    style={{ width: "100%" }}
                  />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label={(
                <span>
                  Тип&nbsp;
                  <Tooltip title="ТИп документа...">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              )}
              hasFeedback
            >
              {getFieldDecorator('type', {
                rules: [{ required: false, message: 'Пожалуйста укажите тип документа!', whitespace: true }],
              })(
                <Select
                  //defaultValue="lucy"
                  //onChange={handleChange}
                  >
                  {Types}
                </Select>
              )}
            </FormItem>
            {/*<FormItem
              {...formItemLayout}
              label={(
                <span>
                  Имя файла&nbsp;
                  <Tooltip title="Файл...">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              )}
              >
              {this.state.filename}
            </FormItem>*/}
            <FormItem {...formItemLayout}>
              <Upload {...props}>
                  <Button>
                      <Icon type="upload"/> Выберите файл
                  </Button>
              </Upload>
            </FormItem>






        <FormItem {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">Сохранить документ</Button>
        </FormItem>
      </fieldset>
      </Form>
    );
  }
}

export default Form.create({
  onFieldsChange(props, changedFields) {
  //props.onChange(changedFields);
},
  mapPropsToFields(props) {
    if (is.propertyDefined(props,'formData')) {
      let out = {};
      for (let key in props.formData) {
        out[key] = {
          value: props.formData[key]
        }
      }
      return out;
    }
},
})(RegistrationForm);
