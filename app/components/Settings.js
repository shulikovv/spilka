import React, {PropTypes, Component} from 'react'
import {Link} from 'react-router';

export default class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {mycss: 'css/my.css'};
    }
    render() {

        const {name} = this.props
        return (
            <div className="settings panel panel-default hidden-xs hidden-sm" id="settings">
                <button data-toggle-class="active" data-toggle-target="#settings" className="btn btn-default">
                    <i className="fa fa-spin fa-gear" />
                </button>
                <div className="panel-heading">
                    Style Selector
                </div>
                <div className="panel-body">
                    {/* start: FIXED HEADER */}
                    {/*<div className="setting-box clearfix">
                        <span className="setting-title pull-left"> Fixed header</span>
                        <span className="setting-switch pull-right">
                            <input type="checkbox" className="js-switch" id="fixed-header" />
                        </span>
                    </div>*/}
                    {/* end: FIXED HEADER */}
                    {/* start: FIXED SIDEBAR */}
                    {/*<div className="setting-box clearfix">
                        <span className="setting-title pull-left">Fixed sidebar</span>
                        <span className="setting-switch pull-right">
                            <input type="checkbox" className="js-switch" id="fixed-sidebar" />
                        </span>
                    </div>*/}
                    {/* end: FIXED SIDEBAR */}
                    {/* start: CLOSED SIDEBAR */}
                    {/*<div className="setting-box clearfix">
                        <span className="setting-title pull-left">Closed sidebar</span>
                        <span className="setting-switch pull-right">
                            <input type="checkbox" className="js-switch" id="closed-sidebar" />
                        </span>
                    </div>*/}
                    {/* end: CLOSED SIDEBAR */}
                    {/* start: FIXED FOOTER */}
                    {/*<div className="setting-box clearfix">
                        <span className="setting-title pull-left">Fixed footer</span>
                        <span className="setting-switch pull-right">
                            <input type="checkbox" className="js-switch" id="fixed-footer" />
                        </span>
                    </div>*/}
                    {/* end: FIXED FOOTER */}
                    {/* start: THEME SWITCHER */}
                    <div className="colors-row setting-box">
                        <div className="color-theme theme-1">
                            <div className="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" defaultValue="theme-1"  />
                                    <span className="ti-check" />
                                    <span className="split header"> <span className="color th-header" /> <span className="color th-collapse" /> </span>
                                    <span className="split"> <span className="color th-sidebar"><i className="element" /></span> <span className="color th-body" /> </span>
                                </label>
                            </div>
                        </div>
                        <div className="color-theme theme-2">
                            <div className="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" defaultValue="theme-2" />
                                    <span className="ti-check" />
                                    <span className="split header"> <span className="color th-header" /> <span className="color th-collapse" /> </span>
                                    <span className="split"> <span className="color th-sidebar"><i className="element" /></span> <span className="color th-body" /> </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className="colors-row setting-box">
                        <div className="color-theme theme-3">
                            <div className="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" defaultValue="theme-3" />
                                    <span className="ti-check" />
                                    <span className="split header"> <span className="color th-header" /> <span className="color th-collapse" /> </span>
                                    <span className="split"> <span className="color th-sidebar"><i className="element" /></span> <span className="color th-body" /> </span>
                                </label>
                            </div>
                        </div>
                        <div className="color-theme theme-4">
                            <div className="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" defaultValue="theme-4" />
                                    <span className="ti-check" />
                                    <span className="split header"> <span className="color th-header" /> <span className="color th-collapse" /> </span>
                                    <span className="split"> <span className="color th-sidebar"><i className="element" /></span> <span className="color th-body" /> </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className="colors-row setting-box">
                        <div className="color-theme theme-5">
                            <div className="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" defaultValue="theme-5" />
                                    <span className="ti-check" />
                                    <span className="split header"> <span className="color th-header" /> <span className="color th-collapse" /> </span>
                                    <span className="split"> <span className="color th-sidebar"><i className="element" /></span> <span className="color th-body" /> </span>
                                </label>
                            </div>
                        </div>
                        <div className="color-theme theme-6">
                            <div className="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" defaultValue="theme-6" />
                                    <span className="ti-check" />
                                    <span className="split header"> <span className="color th-header" /> <span className="color th-collapse" /> </span>
                                    <span className="split"> <span className="color th-sidebar"><i className="element" /></span> <span className="color th-body" /> </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    {/* end: THEME SWITCHER */}
                </div>
            </div>

        );
    }
}

/*Header.propTypes = {
    name: PropTypes.string.isRequired
};*/
