import React, {PropTypes, Component} from 'react'
import {Link} from 'react-router';
import Permissions from './Events/Permissions';
import {isCan} from '../utils/index';

export default class Sidebar extends Component {
    componentDidMount() {
        console.log('Sidebar mounted', this.props);
        //Main.init();
    }

    render() {
        console.log('getState', this.state, this.props);
        const {name} = this.props;
        let Menu = [];
        if (isCan('dashboard','menuview', this.props.user)) {
            Menu.push(
                <li key='dashboard' className={this.props.location.pathname == '/'?'active open':''}>
                <Link to="/">
                    <div className="item-content">
                        <div className="item-media">
                            <i className="ti-home" />
                        </div>
                        <div className="item-inner">
                            <span className="title"> Dashboard </span>
                        </div>
                    </div>
                </Link>
            </li>
            )
        }
        if (isCan('events','menuview', this.props.user)) {
            Menu.push(
                <li key='events' className={this.props.location.pathname == '/events'?'active open':''}>
                <Link to="/events">
                    <div className="item-content">
                        <div className="item-media">
                            <i className="ti-check-box" />
                        </div>
                        <div className="item-inner">
                            <span className="title"> Events </span>
                        </div>
                    </div>
                </Link>
            </li>
            )
        }

        if (isCan('person','menuview', this.props.user)) {
            Menu.push(<li key='person' className={this.props.location.pathname == '/persons'?'active open':''}>
            <Link to="/persons">
                <div className="item-content">
                    <div className="item-media">
                        <i className="ti-user" />
                    </div>
                    <div className="item-inner">
                        <span className="title"> Люди1 </span>
                    </div>
                </div>
            </Link>
        </li>);
        }
        if (isCan('organizations','menuview', this.props.user)) {
            Menu.push(<li key='organizations' className={this.props.location.pathname == '/organizations3'?'active open':''}>
            <Link to="/organizations3">
                <div className="item-content">
                    <div className="item-media">
                        <i className="ti-flag-alt" />
                    </div>
                    <div className="item-inner">
                        <span className="title"> Organizations</span>
                    </div>
                </div>
            </Link>
        </li>);
        }
        if (isCan('vechicles','menuview', this.props.user)) {
            Menu.push(<li key='vechicles' className={this.props.location.pathname == '/vechicles'?'active open':''}>
            <Link to="/vechicles">
                <div className="item-content">
                    <div className="item-media">
                        <i className="ti-truck" />
                    </div>
                    <div className="item-inner">
                        <span className="title"> Vechicles </span>
                    </div>
                </div>
            </Link>
        </li>);
        }
        if (isCan('documents','menuview', this.props.user)) {
            Menu.push(<li key='documets' className={this.props.location.pathname == '/documents'?'active open':''}>
            <Link to="/documents">
                <div className="item-content">
                    <div className="item-media">
                        <i className="ti-agenda" />
                    </div>
                    <div className="item-inner">
                        <span className="title"> Документы </span>
                    </div>
                </div>
            </Link>
        </li>);
        }
        if (isCan('admin','menuview', this.props.user)) {
            Menu.push(<li key='admin' className={this.props.location.pathname.indexOf('/admin') == 0 ?'active open':''}>
            <Link to="/admin">
                <div className="item-content">
                    <div className="item-media">
                        <i className="ti-settings" />
                    </div>
                    <div className="item-inner">
                        <span className="title"> Администрирование </span>
                    </div>
                </div>
            </Link>
                <ul className="sub-menu">
                    <li className={this.props.location.pathname == '/admin/thesauri'?'active':''}>
                        <Link to="/admin/thesauri">
                            <span className="title">Micro-thesauri</span>
                        </Link>
                    </li>
                    <li className={this.props.location.pathname.indexOf('/admin/user') == 0 ?'open':''}>
                        <Link to="/admin/user">
                            <span className="title">User management</span>
                        </Link>
                        <ul className="sub-menu">
                            <li className={this.props.location.pathname == '/admin/user/users'?'active':''}>
                                <Link to="/admin/user/users">
                                    Users
                                </Link>
                            </li>
                            <li className={this.props.location.pathname == '/admin/user/groups'?'active':''}>
                                <Link to="/admin/user/groups">
                                    Groups
                                </Link>
                            </li>
                            <li className={this.props.location.pathname == '/admin/user/roles'?'active':''}>
                                <Link to="/admin/user/roles">
                                    Roles
                                </Link>
                            </li>
                            <li className={this.props.location.pathname == '/admin/user/tolerance'?'active':''}>
                                <Link to="/admin/user/tolerance">
                                    Tolerance levels
                                </Link>
                            </li>
                            {/*<li className={this.props.location.pathname == '/permissions'?'active':''}>
                                <Link to="/permissions">
                                    Permissions
                                </Link>
                            </li>*/}
                        </ul>
                    </li>
                    {/*<li className={this.props.location.pathname == '/admin/language'?'active':''}>
                        <Link to="/admin/language">
                            <span className="title">Language</span>
                        </Link>
                    </li>
                    <li className={this.props.location.pathname == '/admin/system-config'?'active':''}>
                        <Link to="/admin/system-config">
                            <span className="title">System configuration</span>
                        </Link>
                    </li>
                    <li className={this.props.location.pathname == '/admin/printer-config'?'active':''}>
                        <Link to="/admin/printer-config">
                            <span className="title">Printer configuration</span>
                        </Link>
                    </li>
                    <li className={this.props.location.pathname == '/admin/dashboard-config'?'active':''}>
                        <Link to="/admin/dashboard-config">
                            <span className="title">Dashboard configuration</span>
                        </Link>
                    </li> */}
                </ul>
        </li>);
        }

        return (
        <div className="sidebar app-aside" id="sidebar">
            <div className="sidebar-container perfect-scrollbar">
                <nav>
                    {/* start: SEARCH FORM */}

                    {/* end: SEARCH FORM */}
                    {/* start: MAIN NAVIGATION MENU */}
                    <div className="navbar-title">
                        <span>Main Navigation</span>
                    </div>
                    <ul className="main-navigation-menu">
                        {Menu}
                    </ul>
                    {/* end: MAIN NAVIGATION MENU */}
                    {/* start: DOCUMENTATION BUTTON */}
                    <div className="wrapper">
                        <Link to="/documentation" className="button-o">
                            <i className="ti-help" />
                            <span>Документация</span>
                        </Link>
                    </div>
                    {/* end: DOCUMENTATION BUTTON */}
                </nav>
            </div>
        </div>

    );
    }
}

/*Header.propTypes = {
    name: PropTypes.string.isRequired
};*/
