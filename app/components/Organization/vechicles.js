import React, {PropTypes, Component} from 'react'
import { Table, Icon, Button, Popconfirm } from 'antd';
import {vechicleAdd} from '../../actions/VechicleActions';
import 'antd/lib/table/style/css';
import 'antd/lib/button/style/css';
import 'antd/lib/icon/style/css';
import 'antd/lib/popconfirm/style/css';

export default class Vechicles extends Component {
    constructor(props){
        super(props);
        this.dataSource = [
            {
                key: 1,
                vechicle_id: '1',
                vechicle_purpose: '',
                vechicle_brand: 'Mike',
                vechicle_color: 'red',
                vechicle_type: 'male',
                vechicle_model: '',
                vechicle_number: '',
            },
            ];
    
        this.columns = [
            {
              title: 'Purpose',
              dataIndex: 'vechicle_purpose',
            },
            {
              title: 'Brand',
              dataIndex: 'vechicle_brand',
            },
            {
              title: 'Color',
              dataIndex: 'vechicle_color',
            },
            {
              title: 'Type',
              dataIndex: 'vechicle_type',
            },
            {
              title: 'Model',
              dataIndex: 'vechicle_model',
            },
            {
              title: 'Number',
              dataIndex: 'vechicle_number',
            },
            {
                title: 'Actions',
                key: 'actions',
                render: (text, record) => (
                    <span>
                      <a onClick={()=> console.log(record.preson_id)}>View</a>
                      <span className="ant-divider" />
                      <a onClick={()=> console.log(record.preson_id)}>Edit</a>
                      <span className="ant-divider" />
                      <Popconfirm title="Sure to delete?" onConfirm={() => console.log(record.name, 'delete')}>
                        <a>Delete</a>
                      </Popconfirm>
                    </span>
                ),
                width: 250
            },
        ];
    
    }

    render() {
        //const {name} = this.props
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div className='ib user'>
                <div className="row">
                    <div className="col-md-12">
                        <div style={{ marginBottom: 16 }}>
                            <Button
                                type="primary"
                                icon="plus"
                                style={{ marginRight: 10 }}
                                onClick={()=> this.props.actions.vechicleAdd('civil')}
                            >
                                Add New civil Vechicle
                            </Button>
                            <Button
                                type="primary"
                                icon="plus"
                                style={{ marginRight: 10 }}
                                onClick={()=> this.props.actions.vechicleAdd('military')}
                            >
                                Add New military Vechicle
                            </Button>
                            <Button
                                type="primary"
                                icon="plus"
                            >
                                Add Vechicle from List
                            </Button>

                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Table rowSelection={rowSelection} dataSource={this.dataSource} columns={this.columns} />
                    </div>
                </div>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/