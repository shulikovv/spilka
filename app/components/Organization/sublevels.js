import React, {PropTypes, Component} from 'react'
import { Table, Icon, Button, Popconfirm } from 'antd';
import 'antd/lib/table/style/css';
import 'antd/lib/button/style/css';
import 'antd/lib/icon/style/css';
import 'antd/lib/popconfirm/style/css';

export default class addresses extends Component {

    constructor(props){
        super(props);
        this.dataSource = [
                {
                    id: 1,
                    title: 'Organization 1',
                    shortTitle: 'O1',
                    dateUpdated: '2014-04-04',
                    updatedBy: 'admin',
                    children: [
                        {
                            id: 3,
                            title: 'Department 1',
                            shortTitle: 'O1',
                            dateUpdated: '2014-04-04',
                            updatedBy: 'admin',
                        },
                        {
                            id: 4,
                            title: 'Department 2',
                            shortTitle: 'O1',
                            dateUpdated: '2014-04-04',
                            updatedBy: 'admin',
                            children: [

                            ]
                        },
                    ]
                },
                {
                    id: 2,
                    title: 'Organization 2',
                    shortTitle: 'O1',
                    dateUpdated: '2014-04-04',
                    updatedBy: 'admin',
                    children: [
                        {
                            id: 5,
                            title: 'Department 3',
                            shortTitle: 'O1',
                            dateUpdated: '2014-04-04',
                            updatedBy: 'admin',
                            children: [
                                {
                                    id: 6,
                                    title: 'subdepartment 1',
                                    shortTitle: 'O1',
                                    dateUpdated: '2014-04-04',
                                    updatedBy: 'admin',
                                    children: [
                                    ]
                                },
                            ]
                        },
                    ]
                },
            ];

        this.columns = [
            {
            title: 'Organization ID',
            dataIndex: 'id',
            render: (text, record) => <a onClick={()=>  console.log(record.id)}>{text}</a>
            },
            {
            title: 'Organization Title',
            dataIndex: 'title',
            render: (text, record) => <a onClick={()=>  console.log(record.id)}>{text}</a>
            },
            {
            title: 'Short Title',
            dataIndex: 'shortTitle',
            render: (text, record) => <a onClick={()=>  console.log(record.id)}>{text}</a>
            },
            {
                title: 'Actions',
                key: 'actions',
                render: (text, record) => (
                    <span>
                    <a onClick={()=>  this.props.actions.orgView(record.id)}>View</a>
                    <span className="ant-divider" />
                    <a onClick={()=> console.log(record.id)}>Edit</a>
                    <span className="ant-divider" />
                    <Popconfirm title="Sure to delete?" onConfirm={() => console.log(record.id, 'delete')}>
                        <a>Delete</a>
                    </Popconfirm>
                    </span>
                )
            },
        ];

    }




    render() {
        //const {name} = this.props
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div className='ib user'>
                <div className="row">
                    <div className="col-md-12">
                        <div style={{ marginBottom: 16 }}>
                            <Button
                                type="primary"
                                icon="plus"
                            >
                                Add Depertment
                            </Button>

                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Table rowKey='id' rowSelection={rowSelection} dataSource={this.dataSource} columns={this.columns} />
                    </div>
                </div>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/