import React, {PropTypes, Component} from 'react'
import Form from "react-jsonschema-form";
import { Spin , Button, Popconfirm} from 'antd';
import 'antd/lib/spin/style/css';
import 'antd/lib/button/style/css';
import 'antd/lib/popconfirm/style/css';

import {widgets} from "../../elements/form/widgets";


export default class EventForm extends Component {
    constructor(props) {
        super(props);
        this.logg = (mess) => console.log(mess);
        
        this.onSubmit = (formData) => console.log(formData);
        this.schema = {
            type: "object",
            properties: {
                "org_title": {
                    "title": "Organization Title",
                    "type": "string",
                },
                "org_abbreviation": {
                    "title": "Abbreviation",
                    "type": "string",
                },
                "org_parent": {
                    "title": "Parent Organization",
                    "type": "string",
                },
                "org_departments": {
                    "title": "Departments",
                    "type": "string",
                },
                "org_recognition": {
                    "title": "Recognition marks",
                    "type": "string",
                },
                "org_is_state": {
                    "title": "is State",
                    "type": "boolean",
                },
                "org_initial_date": {
                    "title": "Initial Date",
                    "type": "string",
                    "format": "date",
                },
                "org_description": {
                    "title": "Description",
                    "type": "string",
                    format: "textarea",
                },
                "org_features": {
                    "title": "features",
                    "type": "string",
                    format: "textarea"
                },
            }
        };
    
        this.uiSchema = {
            org_title : {
                 "ui:widget": "myCustomWidget",
                 "ui:options": {
                    backgroundColor: "red"
                }
            }
        };
    
        this.formDat = {};
    }


    

    render() {
        //const {name} = this.props
        return (
            <div>
                <Spin tip="Loading..." size="large" spinning={this.props.loading}>
                
                <Form schema={this.schema}
                    uiSchema={this.uiSchema}
                    onChange={this.logg("changed")}
                    onSubmit={this.onSubmit}
                    onError={this.logg("errors")}
                    liveValidate={true}
                    showErrorList={true}
                    formData={this.formData}
                    widgets={widgets}
                />
                </Spin>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/
