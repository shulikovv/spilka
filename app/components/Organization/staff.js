import React, {PropTypes, Component} from 'react'
import { Table, Icon, Button, Popconfirm } from 'antd';
import 'antd/lib/table/style/css';
import 'antd/lib/button/style/css';
import 'antd/lib/icon/style/css';
import 'antd/lib/popconfirm/style/css';

export default class addresses extends Component {

    constructor(props) {
        super(props);

        this.dataSource = [
            {
                key: 1,
                preson_id: '1',
                unit: '',
                name: 'Mike',
                birth: '2017-06-21',
                sex: 'male'
            },
            ];
    
        this.columns = [
            {
              title: 'Person Record Number1',
              dataIndex: 'preson_id',
              'width': 150  
            },
            {
              title: 'Counting unit',
              dataIndex: 'unit',
              'width': 250
            },
            {
              title: 'Name',
              dataIndex: 'name',
              width: 350
            },
            {
              title: 'Date of Birth',
              dataIndex: 'birth',
              width: 250
            },
            {
              title: 'Sex',
              dataIndex: 'sex',
              width: 250
            },
            {
                title: 'Actions',
                key: 'actions',
                render: (text, record) => (
                    <span>
                      <a onClick={()=> console.log(record.preson_id)}>View</a>
                      <span className="ant-divider" />
                      <a onClick={()=> console.log(record.preson_id)}>Edit</a>
                      <span className="ant-divider" />
                      <Popconfirm title="Sure to delete?" onConfirm={() => console.log(record.name, 'delete')}>
                        <a>Delete</a>
                      </Popconfirm>
                    </span>
                ),
                width: 250
            },
        ];
    }


    render() {
        //const {name} = this.props
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div className='ib user'>
                <div className="row">
                    <div className="col-md-12">
                        <div style={{ marginBottom: 16 }}>
                            <Button
                                type="primary"
                                icon="plus"
                                style={{ marginRight: 10 }}
                            >
                                Add New Person
                            </Button>
                            <Button
                                type="primary"
                                icon="plus"
                            >
                                Add Person from List
                            </Button>

                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Table rowSelection={rowSelection} dataSource={this.dataSource} columns={this.columns} />
                    </div>
                </div>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/