import React, {PropTypes, Component} from 'react'
import { Table, Icon, Button, Popconfirm } from 'antd';
import 'antd/lib/table/style/css';
import 'antd/lib/button/style/css';
import 'antd/lib/icon/style/css';
import 'antd/lib/popconfirm/style/css';

export default class addresses extends Component {

    constructor(props){
        super(props);
        this.dataSource = [];
        this.columns = [
        {
          title: 'Address Type',
          dataIndex: 'address_type'
        },
        {
          title: 'Address',
          dataIndex: 'address'
        },
        {
          title: 'Country',
          dataIndex: 'country'
        },
        {
          title: 'Phone',
          dataIndex: 'phone'
        },
        {
          title: 'Cellular',
          dataIndex: 'cellular'
        },
        {
          title: 'Fax',
          dataIndex: 'fax'
        },
        {
          title: 'Email',
          dataIndex: 'email'
        },
        {
          title: 'Website',
          dataIndex: 'website'
        },
        {
          title: 'Start Date',
          dataIndex: 'stsrt_date'
        },
        {
          title: 'End Date',
          dataIndex: 'end_date'
        },
        {
            title: 'Actions',
            key: 'actions',
            render: (text, record) => (
                <span>
                  <a onClick={()=> this.props.actions.viewPerson(record.preson_id)}>View</a>
                  <span className="ant-divider" />
                  <a onClick={()=> this.props.actions.editPerson(record.preson_id)}>Edit</a>
                  <span className="ant-divider" />
                  <Popconfirm title="Sure to delete?" onConfirm={() => console.log(record.name, 'delete')}>
                    <a>Delete</a>
                  </Popconfirm>
                </span>
            )
        },
    ];
    }

    render() {
        //const {name} = this.props
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div className='ib user'>
                <div className="row">
                    <div className="col-md-12">
                        <div style={{ marginBottom: 16 }}>
                            <Button
                                type="primary"
                                icon="plus"
                            >
                                Add Address
                            </Button>

                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Table rowSelection={rowSelection} dataSource={this.dataSource} columns={this.columns} />
                    </div>
                </div>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/