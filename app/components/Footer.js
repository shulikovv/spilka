import React, {PropTypes, Component} from 'react'
import {Link} from 'react-router';

export default class Footer extends Component {
    render() {

        const {name} = this.props
        return (
            <footer>
                <div className="footer-inner">
                    <div className="pull-left">
                        © <span className="current-year" /><span className="text-bold text-uppercase"> UHHRU</span>. <span>All rights reserved</span>
                    </div>
                    <div className="pull-right">
                        <span className="go-top"><i className="ti-angle-up" /></span>
                    </div>
                </div>
            </footer>
        );
    }
}

/*Header.propTypes = {
    name: PropTypes.string.isRequired
};*/
