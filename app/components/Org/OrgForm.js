import React, {PropTypes, Component} from 'react';
import { Table, Icon, Button, Popconfirm, Tree, Modal, Alert, Tabs, message, notification } from 'antd';
import 'antd/lib/message/style/css';
import Form from "react-jsonschema-form";

const TabPane = Tabs.TabPane;

import {widgets} from "../../elements/form/widgets";

export default class orgForm extends Component {
    constructor(props) {
        super(props);
        this.info = () => {      
            console.log('here');
            message.success('This function disabled now');
            notification.open({
            message: 'Notification Title',
            description: 'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
            duration: 0
        });
        }

        this.getTabContent = (item) => {
            switch(item.type) {
                case 'form':
                    return (<div>
                        <Button type="primary" style={{marginRight: 8}} onClick={()=> this.props.actions.editOrganization(item.form.data.organization_id)}>Edit</Button>
                        {/*<Button type="primary" style={{marginRight: 8}}>Print</Button>*/}
                        <Popconfirm title="Sure to delete?" onConfirm={this.info}>
                            <Button type="primary">Delete</Button>
                        </Popconfirm>
                        <Form schema={item.form.schema}
                        uiSchema={item.form.uiSchema}
                        formData={item.form.data}
                        widgets={widgets}
                        onSubmit={(data)=>console.log(data)}
                        >
                        <div></div>
                        </Form>
                        </div>)
                break;
                case 'table':
                    return (<Table rowKey="organization_id" dataSource={item.form.data} columns={item.form.columns} ></Table>)
                break;
                default:
                    return (<div></div>)
            }
        }
    }


    render() {
        return (<div>
                <Tabs defaultActiveKey="0" onChange={(e)=>console.log(e)}>
                    {this.props.form.tabs.map((item, index)=>{
                        return (<TabPane tab={item.title} key={index}>{this.getTabContent(item)}</TabPane>)
                    })}
                </Tabs>
            </div>)
    }
}