import React, {PropTypes, Component} from 'react';
import { Table, Icon, Button, Popconfirm, Tree, Modal, Alert, Tabs } from 'antd';
import 'antd/lib/tree/style/css';
import * as is from 'is_js';

const TreeNode = Tree.TreeNode;

export default class orgTree extends Component {

    constructor(props) {
        super(props);
        this.prepareTree = (tree) => {
            let out = tree.map((item)=>{
                if (item.children && item.children.length > 0) {
                    return <TreeNode title={item.organization_title} key={''+item.organization_id}>{this.prepareTree(item.children)}</TreeNode>
                } else {
                    return <TreeNode title={item.organization_title} key={''+item.organization_id}/>
                }
            })
            return out;
        }
    
        this.onSelect = (e) => {
            //this.props.actions.orgGetOrganization(e[0]);
            this.props.actions.getOrgForm(e[0]);
            this.props.actions.setCurrentOrg(e[0]);
            //console.log(this.props.actions);
            //this.props.actions.getOrgFormData(this.props.form, e);
            console.log('onSElect',e)
        }; 
    }

    componentWillUpdate(nextProps, nextState) {
        
    }

    componentWillMount() {
        console.log('//////////////--',this.props.actions)
        this.props.actions.getOrgForm(this.props.treeDefaultSelectedKeys);
    }



    render() {
        return (
            <div>
                <Button
                    type="primary"
                    icon="plus"
                    onClick={this.props.actions.addNewOrganization}
                >
                    Add New Organization
                </Button>
                <Tree
                    showLine
                    defaultExpandedKeys={[]}
                    defaultSelectedKeys={[this.props.treeDefaultSelectedKeys]}
                    onSelect={this.onSelect}
                >
                    {this.prepareTree(this.props.orgTreeData)}
                </Tree>
            </div>
        )
    }
}