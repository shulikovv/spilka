import React, {PropTypes, Component} from 'react'
import { Button, Popconfirm, Icon } from 'antd';


export default class ViewFormButtons extends Component {
    render () {
        return (
            <div>
                <Button type="primary" icon="edit" style={{marginRight: 8}} onClick={() => this.props.actions.eventEdit(this.props.eventId)}>Edit this Event</Button>
                <Button type="primary" icon="printer" style={{marginRight: 8}}>Print this Event</Button>
                <Popconfirm title="Sure to delete?" onConfirm={() => console.log('delete')}>
                    <Button type="primary" icon="delete">Delete this Event</Button>
                </Popconfirm>
            </div>
        );
    }
}