import React, {PropTypes, Component} from 'react'
import { DatePicker, LocaleProvider, Table, Tabs, Button, Icon} from 'antd';


const TabPane = Tabs.TabPane;

export default class Sources extends Component {

    constructor(props){
        super(props);
        this.dataSource = [
            {
                key: 1,
                date_of_source_material: '2017-05-23',
                person_name: 'Mike',
                source_connection_to_information: 'Victim him/herself',
            }, 
            {
                key: 2,
                date_of_source_material: '2017-05-23',
                person_name: 'Mike',
                source_connection_to_information: 'Victim him/herself',
            }, 
            ];
    
        this.columns = [
            {
                title: 'Date of Source Material',
                dataIndex: 'date_of_source_material',
            }, 
            {
                title: 'Source name',
                dataIndex: 'person_name',
                render: text => <a href="#">{text}</a>
            }, 
            {
                title: 'Information',
                dataIndex: 'source_connection_to_information',
                render: text => <a href="#">{text}</a>
            },
        ];
    }



    render() {
        //const {name} = this.props
        // rowSelection object indicates the need for row selection
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
                <div style={{marginBottom: 20}}>
                    <Button type="primary" icon="plus" style={{marginRight: 8}}>Add Source</Button>
                    <Button icon="delete" style={{marginRight: 8}}>Delete</Button>
                </div>
                <div>
                    <Table rowSelection={rowSelection} dataSource={this.dataSource} columns={this.columns} />
                </div>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/
