import React, {PropTypes, Component} from 'react'
import { DatePicker, LocaleProvider, Table, Tabs, Button, Icon} from 'antd';


const TabPane = Tabs.TabPane;

export default class Sources extends Component {

    constructor(props){
        super(props);
        this.dataSource = [
            {
                key: 1,
                entity: '2017-05-23',
                document_title: 'Mike',
                type: 'Victim him/herself',
                format: 'Victim him/herself',
            }, 
            ];
    
        this.columns = [
            {
                title: 'Document ID',
                dataIndex: 'key',
            }, 
            {
                title: 'Entity',
                dataIndex: 'entity',
                render: text => <a href="#">{text}</a>
            }, 
            {
                title: 'Document Title',
                dataIndex: 'document_title',
                render: text => <a href="#">{text}</a>
            },
            {
                title: 'Type',
                dataIndex: 'type',
                render: text => <a href="#">{text}</a>
            },
            {
                title: 'Format',
                dataIndex: 'format',
                render: text => <a href="#">{text}</a>
            },
            {
                title: 'Actions',
                render: (text, record) => (
                    <span>
                    </span>
                )
            }
        ];
    }


    render() {
        return (
            <div>
                <div>
                    <Table dataSource={this.dataSource} columns={this.columns} />
                </div>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/
