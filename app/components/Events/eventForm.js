import React, {PropTypes, Component} from 'react'
import Form from "react-jsonschema-form";
import { Spin , Button, Popconfirm} from 'antd';


import {widgets} from "../../elements/form/widgets";

export default class EventForm extends Component {

    constructor(props) {
        super(props);
        this.logg = (mess) => console.log(mess);
        
            this.onSubmit = (formData) => console.log(formData);
        
            this.schema = {
                type: "object",
                properties: {
                    "event_title": {
                        "title": "Event Title",
                        "type": "string",
                    },
                    "confidentiality": {
                        "title": "Confidentiality",
                        "type": "string",
                    },
                    "geographical_term": {
                        "title": "Geographical Term",
                        "type": "string",
                    },
                    "local_geographical_area": {
                        "title": "Local Geographical Area",
                        "type": "string",
                    },
                    "initial_date": {
                        "title": "Initial Date",
                        "type": "string",
                    },
                    "initial_date_type": {
                        "title": "Initial Date Type",
                        "type": "string",
                    },
                    "final_date": {
                        "title": "Final Date",
                        "type": "string",
                    },
                    "final_date_type": {
                        "title": "Final Date Type",
                        "type": "string",
                    },
                    "event_description": {
                        "title": "Event Description",
                        "type": "string",
                    },
                    "impact_of_event": {
                        "title": "Impact of Event",
                        "type": "string",
                    },
                    "remarks": {
                        "title": "Remarks",
                        "type": "string",
                    },
                    "violation_status": {
                        "title": "Violation Status",
                        "type": "string",
                    },
                    "violation_index": {
                        "title": "Violation Index",
                        "type": "string",
                    },
                    "rights_affected": {
                        "title": "Rights Affected",
                        "type": "string",
                    },
                    "huridocs_index_terms": {
                        "title": "HURIDOCS Index terms",
                        "type": "string",
                    },
                    "local_index": {
                        "title": "Local Index",
                        "type": "string",
                    },
                    "other_thesaurus": {
                        "title": "Other Thesaurus",
                        "type": "string",
                    },
                    "date_received": {
                        "title": "Date Received",
                        "type": "string",
                    },
                    "project_title": {
                        "title": "Project Title",
                        "type": "string",
                    },
                    "comments": {
                        "title": "Comments",
                        "type": "string",
                    },
                    "supporting_documents": {
                        "title": "Supporting Documents",
                        "type": "string",
                    },
                    "files": {
                        "title": "Files",
                        "type": "string",
                    },
                    "record_grouping": {
                        "title": "Record Grouping",
                        "type": "string",
                    },
                    "monitoring_status": {
                        "title": "Monitoring Status",
                        "type": "string",
                    },
                    "event_location": {
                        "title": "Location",
                        "type": "string",
                    },
                }
            };
        
            this.uiSchema = {};
        
            this.formDat = {};
        
    }


    setUiSchema(props) {
        this.uiSchema = this.props.form.uiSchema;
        if (props.readonly) {
            this.uiSchema = {
                "ui:readonly": true, 
                ...this.uiSchema
            }
        }
    }

    componentWillMount() {
        this.setUiSchema(this.props);
    }

    componentWillUpdate(nextProps, nextState) {
        this.setUiSchema(nextProps);
    }
    

    render() {
        //const {name} = this.props
        console.log('event form --->', this.props)
        return (
            <div>
                <Spin tip="Loading..." size="large" spinning={this.props.loading}>
                
                <Form 
                    schema={this.props.form.schema}
                    uiSchema={this.uiSchema}
                    onChange={this.logg("changed")}
                    onSubmit={this.onSubmit}
                    onError={this.logg("errors")}
                    liveValidate={true}
                    showErrorList={true}
                    widgets={widgets}
                    formData={this.formData}
                />
                </Spin>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/
