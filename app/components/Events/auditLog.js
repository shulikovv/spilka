import React, {PropTypes, Component} from 'react'
import { DatePicker, LocaleProvider, Table, Tabs, Button, Icon} from 'antd';
import person from '../../reducers/person';


const TabPane = Tabs.TabPane;

export default class Sources extends Component {

    constructor(props) {
        super(props);
        this.dataSource = [
            {
                key: 1,
                date_of_action: '2017-05-23',
                action: 'update',
                module: 'person',
                entity: 'person',
                record_number: 'HURI/person/33',
                username: 'admin',
            },
            {
                key: 2,
                date_of_action: '2017-05-23',
                action: 'delete',
                module: 'person',
                entity: 'person',
                record_number: 'HURI/person/33',
                username: 'admin',
            },
            ];
    
        this.columns = [
            {
                title: 'Timestamp',
                dataIndex: 'date_of_action',
            },
            {
                title: 'Action',
                dataIndex: 'action',
            },
            {
                title: 'Module',
                dataIndex: 'module',
            },
            {
                title: 'Entity',
                dataIndex: 'entity',
            },
            {
                title: 'Record No',
                dataIndex: 'record_number',
            },
            {
                title: 'User',
                dataIndex: 'username',
            },
        ];
    }



    render() {
        //const {name} = this.props
        // rowSelection object indicates the need for row selection
        return (
            <div>
                <div>
                    <Table dataSource={this.dataSource} columns={this.columns} />
                </div>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/
