import React, {PropTypes, Component} from 'react'
import { DatePicker, LocaleProvider, Table, Tabs, Button, Icon} from 'antd';


const TabPane = Tabs.TabPane;

export default class Sources extends Component {

    constructor(props) {
        super(props);
        this.dataSource = [
            {
                key: 1,
                date_of_intervention: '2017-05-23',
                person_name: 'Mike',
                type_of_intervention: 'Financial assistance',
            }, 
            {
                key: 2,
                date_of_intervention: '2017-05-23',
                person_name: 'Mike',
                type_of_intervention: 'Financial assistance',
            }, 
            ];
    
        this.columns = [
            {
                title: 'Date of Intervention',
                dataIndex: 'date_of_intervention',
            }, 
            {
                title: '	Intervening Party',
                dataIndex: 'person_name',
                render: text => <a href="#">{text}</a>
            }, 
            {
                title: 'Type of Intervention',
                dataIndex: 'type_of_intervention',
                render: text => <a href="#"><ul><li>text</li></ul></a>
            },
        ];
    
    }


    render() {
        //const {name} = this.props
        // rowSelection object indicates the need for row selection
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
                <div style={{marginBottom: 20}}>
                    <Button type="primary" icon="plus" style={{marginRight: 8}}>Add Intervention</Button>
                    <Button icon="delete" style={{marginRight: 8}}>Delete</Button>
                </div>
                <div>
                    <Table rowSelection={rowSelection} dataSource={this.dataSource} columns={this.columns} />
                </div>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/
