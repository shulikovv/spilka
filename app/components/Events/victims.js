import React, {PropTypes, Component} from 'react'
import { DatePicker, LocaleProvider, Table, Tabs, Button, Icon, message, notification} from 'antd';

const TabPane = Tabs.TabPane;

export default class Victims extends Component {
    constructor(props) {
        super(props);
        this.dataSource = [
            {
                key: 1,
                initial_date: '2017-05-23',
                person_name: 'Mike',
                type_of_act: 32,
                perpetrator_name: 'Tim',
                involvement: 'involvement'
            }, 
            {
                key: 2,
                initial_date: '2017-05-23',
                person_name: 'Mike',
                type_of_act: 32,
                perpetrator_name: 'Tim',
                involvement: 'involvement'
            }, 
            ];
    
        this.columns = [
            {
                title: 'Initial date',
                dataIndex: 'initial_date',
            }, 
            {
                title: 'Victim name',
                dataIndex: 'person_name',
                render: text => <a href="#">{text}</a>
            }, 
            {
                title: 'Type of Act',
                dataIndex: 'type_of_act',
                render: text => <a href="#">{text}</a>
            },
            {
                title: 'Perpetrator name(s)',
                dataIndex: 'perpetrator_name',
                render: text => <a href="#">{text}</a>
            },
            {
                title: 'Involvement',
                dataIndex: 'involvement',
                render: text => <a href="#">{text}</a>
            },
        ];
    
        this.columns2 = [
            {
                title: 'Victim',
                dataIndex: 'victim_name'
            },
        ];
    
        this.dataSource2 = [
            {
                id: 1,
                key: 1,
                victim_name: 'Ivan'
            }
        ];
    
        this.selectedRows0 = [];
    
        this.expandedRowRender = record => {
            console.log('rowKey',record, this.props);
            const columns = [
                {
                    title: 'Type of Act',
                    dataIndex: 'type_of_act'
                },
                {
                    title: 'Perpetrator',
                    dataIndex: 'perpetrator_name'
                },
                {
                    title: 'Involvement',
                    dataIndex: 'involvement'
                },
            ]
            const dataSource = [
                {
                    key: 1,
                    type_of_act: 'test',
                    perpetrator_name: 'Fg'
                }
            ]
            const expandedRowRender = record => {
                return (<div>sdsdsds</div>)
            }
            return (<div>
                <div className='row' style={{marginBottom:10}}>
                    <div className='col-md-12'>
                        <Button icon="plus" style={{marginRight: 8}}>Add Act</Button>
                        <Button icon="plus" style={{marginRight: 8}}>Add Perpetrator(s)</Button>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-12'>
                        <Table
                            dataSource={dataSource} 
                            columns={columns} 
                            expandedRowRender={expandedRowRender}
                            pagination={false}
                        ></Table>
                    </div>
                </div>
            </div>)
        }
    
    }

    render() {
        //const {name} = this.props
        // rowSelection object indicates the need for row selection
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
                this.selectedRows0 = selectedRows;
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        return (
            <div>
                <div style={{marginBottom: 20}}>
                    <Button type="primary" icon="plus" style={{marginRight: 8}}
                        onClick={()=> this.props.actions.addVictim(this.props.location.pathname)}
                    >Add Victum</Button>
                    <Button icon="plus" style={{marginRight: 8}} 
                        onClick={()=>{
                            //console.log('.....',this.selectedRows0, this.selectedRows0.length);
                            if (this.selectedRows0.length == 0) {
                                message.error('Please select some checkbox to perform action.');
                            }
                        }}
                    >Add Act</Button>
                    <Button icon="plus" style={{marginRight: 8}}
                        onClick={()=>{
                            //console.log('.....',this.selectedRows0, this.selectedRows0.length);
                            if (this.selectedRows0.length == 0) {
                                message.error('Please select some checkbox to perform action.');
                            }
                        }}
                    >Add Perpetrator(s)</Button>
                </div>
                <div>
                    <Table rowSelection={rowSelection} dataSource={this.dataSource} columns={this.columns} />
                </div>
                <div>
                    <Table 
                        //className="components-table-demo-nested"
                        rowSelection={rowSelection} 
                        dataSource={this.dataSource2} 
                        columns={this.columns2} 
                        expandedRowRender={this.expandedRowRender}
                    />
                </div>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/
