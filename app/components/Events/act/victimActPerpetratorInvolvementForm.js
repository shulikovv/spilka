import React, {PropTypes, Component} from 'react';
import { Steps, Button, message } from 'antd';


const Step = Steps.Step;

import Victim from './victim';
import Act from './act';
import Perpetrator from './perpetrator';
import Involvement from './involvement';




export default class victimActPerpetratorInvolvementForm extends Component {
    constructor(props) {
        super(props);
        

        this.state = {
            current: 0,
        };
    }
    componentWillMount() {

    }

    componentWillUpdate(nextProps, nextState) {
        console.log('...11................>>>',nextProps);
    }
    
    next() {
        const current = this.state.current + 1;
        this.setState({ current });
    }
    prev() {
        const current = this.state.current - 1;
        this.setState({ current });
    }
    render() {
        const { current } = this.state;
        this.steps = [
            {
                title: 'Victim',
                id: 'victim',
                content: (<Victim {...this.props} next={this.next.bind(this)} />),
            }, 
            {
                title: 'Act',
                id: 'act',
                content: (<Act {...this.props} next={this.next.bind(this)} prev={this.prev.bind(this)}/>),
            }, 
            {
                title: 'Perpetrator',
                id: 'perpetrator',
                content: (<Perpetrator {...this.props} next={this.next.bind(this)} />),
            }, 
            {
                title: 'Involvement',
                id: 'involvement',
                content: (<Involvement {...this.props} next={this.next.bind(this)}  prev={this.prev.bind(this)} />),
            }
        ];
        return (
            <div>
            <Steps current={current} style={{marginBottom: 10}}>
              {this.steps.map(item => <Step key={item.title} title={item.title} />)}
            </Steps>
            <div className="steps-content">{this.steps[this.state.current].content}</div>
            {/*<div className="steps-action">
                {
                    (this.steps[this.state.current].id === 'act'||this.steps[this.state.current].id === 'involvement')
                    &&
                    <Button  style={{ marginRight: 8 }} type="primary" onClick={() => message.success('Processing complete!')}>Save</Button>
                }
                {
                    this.state.current < this.steps.length - 1
                    &&
                    <Button type="primary" onClick={() => this.next()}>Next</Button>
                }
            </div>*/}
          </div>
        )
    }
}