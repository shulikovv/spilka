import React, {PropTypes, Component} from 'react';
import { Button, message, Modal, Input, Table } from 'antd';

import Form from "react-jsonschema-form";
import * as _ from 'lodash';


import {widgets} from "../../../elements/form/widgets";

class MyTable extends Component {
    
    constructor(props) {
        super(props);
        this.logg = (mess) => console.log(mess);
    }

    componentWillMount() {
        
    }
    
    closeFindInDb() {
        this.setState({searchModal1visible:false})
        console.log('gsdjhgsjhdgjsd', this.state);
    }

    componentDidMount () {
        const self = this;
        console.log('try table', this.state);
        $(document).ready(function () {
            $("#grid").kendoGrid({
                dataSource: {
                    data: self.props.personslist,
                    pageSize: 20
                },
                height: 550,
                groupable: true,
                sortable: true,
                filterable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                {
                    field: "person_id",
                    title: "Person #",
                }, {
                    field: "counting_unit",
                    title: "Counting unit"
                }, 
                {
                    field: "person_name",
                    title: "Name"
                }, 
                {
                    field: "date_of_birth",
                    title: "Date of birth"
                }, 
                {
                    field: "sex",
                },
                {
                    title: "Actions",
                    command: [
                        {
                            name: "Select",
                            click: (e) => {
                                var grid = $("#grid").data("kendoGrid");
                                var data = grid.dataItem($( e.currentTarget ).parent().parent());
                                
                                self.props.onSelect(data);
                            }
                        },
                    ]
                }
            ]
            });
        });
    }
    
    componentDidUpdate(prevProps, prevState) {
        
    }
    

    render() {
        return (<div>
            <div id="grid"></div>
        </div>)
    }
}


export default class Victim extends Component {
    constructor(props){
        super(props);
        console.log('-------------------------------->>>',props);
        this.state = {
            addModal1visible : false,
            searchModal1visible : false
        };
        this.logg = (mess) => console.log(mess);

        this.selectVictim = (data) => {
            this.props.actions.selectPresonForAct(data.person_id);
            this.setState({searchModal1visible:false})
        }
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('...................>>>',nextProps);
    }
    

    


    render(){
       const columns = [
            {
                title: 'Person #',
                dataIndex: 'person_id',
                key: 'person_id',
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                      <Input
                        ref={ele => this.searchInput = ele}
                        placeholder="Search name"
                        value={this.state.searchText}
                        onChange={this.onInputChange}
                        onPressEnter={this.onSearch}
                      />
                      <Button type="primary" onClick={this.onSearch}>Search</Button>
                    </div>
                  ),
            },
            {
                title: 'Counting unit',
                dataIndex: 'counting_unit'
            },
            {
                title: 'Name',
                dataIndex: 'person_name'
            },
            {
                title: 'Date of birth',
                dataIndex: 'date_of_birth'
            },
            {
                title: 'Sex',
                dataIndex: 'sex'
            },
            {
                title: 'Action'
            }
        ];
        return (<div>
            <style>{`.ant-modal-mask, .ant-modal-wrap { z-index: 1000000 !important;}`}</style>
                <div className='row'>
                    <div className='col-md-12'>
                        <Button onClick={() => this.props.actions.ReturnTo(this.props.actReturnPath)} style={{marginRight:5}} icon='close-circle-o'>Cancel</Button>
                        <Button onClick={()=> this.setState({addModal1visible:true})} style={{marginRight:5}} icon="plus">Add New</Button>
                        <Button onClick={()=> this.setState({searchModal1visible:true})} style={{marginRight:5}} icon="search">Search in database</Button>
                        <Button type="primary" disabled={(_.size(this.props.victim.data)==0)} onClick={() => this.props.next()} icon="right">Next</Button>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-12'>
                        <Form
                            schema={this.props.victim.victimForm.schema}
                            uiSchema={this.props.victim.victimForm.uiSchema}
                            widgets={widgets}
                            formData={this.props.victim.data}
                         />
                    </div>
                </div>
                <Modal
                    title="Add new person"
                    visible={this.state.addModal1visible}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.setState({addModal1visible:false})}
                    >
                    <Form 
                    schema={this.props.personForm.schema}
                    uiSchema={this.props.personForm.uiSchema}
                    onChange={this.logg("changed")}
                    onSubmit={this.onSubmit}
                    onError={this.logg("errors")}
                    liveValidate={true}
                    showErrorList={true}
                    widgets={widgets}
                    formData={this.formData}
                />
                </Modal>
                <Modal
                    title="Search in database"
                    visible={this.state.searchModal1visible}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.setState({searchModal1visible:false})}
                    width={1000}
                    >
                    <MyTable {...this.props} onSelect={this.selectVictim} />
                </Modal>
            </div>)
    }
}