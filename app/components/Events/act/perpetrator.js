import React, {PropTypes, Component} from 'react';
import { Button, message } from 'antd';


export default class Perpetrator extends Component {
    render(){
        return (<div>
                <div className='row'>
                    <div className='col-md-12'>
                        <Button onClick={() => this.props.next()} style={{marginRight:5}} icon='close-circle-o'>Add perpetrator later</Button>
                        <Button onClick={() => this.props.next()} style={{marginRight:5}} icon="plus">Add New</Button>
                        <Button onClick={()=> this.props.next()} style={{marginRight:5}} icon="search">Search in database</Button>
                        <Button type="primary" onClick={() => this.props.next()} icon="right">Next</Button>
                    </div>
                </div>
            </div>)
    }
}