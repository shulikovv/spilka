import React, {PropTypes, Component} from 'react';
import { Button, message } from 'antd';


export default class Involvement extends Component {
    render(){
        return (<div>
                <div className='row'>
                    <div className='col-md-12'>
                    <Button onClick={() => this.props.prev()} style={{marginRight:5}} icon='left'>Back</Button>
                        <Button onClick={() => this.props.next()} style={{marginRight:5}} icon='close-circle-o'>Cancel</Button>
                        <Button onClick={() => this.props.next()} style={{marginRight:5}} icon="plus">Save and add more Perpetrators</Button>
                        <Button type="primary" onClick={() => this.props.next()} icon="right">Finish</Button>
                    </div>
                </div>
            </div>)
    }
}