import React, {PropTypes, Component} from 'react';
import { Button, message } from 'antd';


export default class Victim extends Component {
    render(){
        return (<div>
                <div className='row'>
                    <div className='col-md-12'>
                        <Button onClick={() => this.props.prev()} style={{marginRight:5}} icon='left'>Back</Button>
                        <Button onClick={() => this.props.next()} style={{marginRight:5}} icon='close-circle-o'>Add perpetrator later</Button>
                        <Button onClick={() => this.props.next()} style={{marginRight:5}} icon="plus">Add Additional Details</Button>
                        <Button type="primary" onClick={() => this.props.next()} icon="right">Next</Button>
                    </div>
                </div>
            </div>)
    }
}
