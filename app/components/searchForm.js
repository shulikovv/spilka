import React, {PropTypes, Component} from 'react'

export default class SearchForm extends Component {
    render () {
        return (
            <div className="search-form">
                <Link to="/" className="s-open">
                    <i className="ti-search" />
                </Link>
                <form className="navbar-form" role="search">
                    <Link to="/" className="s-remove" target=".navbar-form">
                        <i className="ti-close" />
                    </Link>
                    <div className="form-group">
                        <input type="text" className="form-control" placeholder="Search..." />
                        <button className="btn search-button">
                            <i className="ti-search" />
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}
                    