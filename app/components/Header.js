import React, {PropTypes, Component} from 'react'
import {Link} from 'react-router';
import * as actionCreators from '../actions';
import {addEvent} from '../actions/EvensActions';
import {AddPerson} from '../actions/PersonActions';
import {isCan} from '../utils/index';

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.logOutClick = () => {
            console.log('Log out', this.props);
            this.props.actions.logoutUser();
        }
    
    }
    render() {

        const {name} = this.props
        return (
            <header className="navbar navbar-default navbar-static-top">
                {/* start: NAVBAR HEADER */}
                <div className="navbar-header">
                    <Link to="/" className="sidebar-mobile-toggler pull-left hidden-md hidden-lg" data-toggle-class="app-slide-off" data-toggle-target="#app" data-toggle-click-outside="#sidebar">
                        <i className="ti-align-justify" />
                    </Link>
                    <Link to="/" className="navbar-brand" >
                        <h2 style={{marginTop: 15}}>UHHRU </h2>
                    </Link>
                    <Link to="#" className="sidebar-toggler pull-right visible-md visible-lg" data-toggle-class="app-sidebar-closed" data-toggle-target="#app">
                        <i className="ti-align-justify" />
                    </Link>
                    <a className="pull-right menu-toggler visible-xs-block" id="menu-toggler" data-toggle="collapse" data-ahref=".navbar-collapse">
                        <span className="sr-only">Toggle navigation</span>
                        <i className="ti-view-grid" />
                    </a>
                </div>
                {/* end: NAVBAR HEADER */}
                {/* start: NAVBAR COLLAPSE */}
                <div className="navbar-collapse collapse">
                    <ul className="nav navbar-right" style={{height: 'auto'}}>
                        {/* start: MESSAGES DROPDOWN */}
                        {/* <li className="dropdown">
                            <a href className="dropdown-toggle" data-toggle="dropdown">
                                <span className="dot-badge partition-red" /> <i className="ti-comment" /> <span>MESSAGES</span>
                            </a>
                            <ul className="dropdown-menu dropdown-light dropdown-messages dropdown-large">
                                <li>
                                    <span className="dropdown-header"> Unread messages</span>
                                </li>
                                <li>
                                    <div className="drop-down-wrapper ps-container">
                                        <ul>
                                            <li className="unread">
                                                <a href="javascript:;" className="unread">
                                                    <div className="clearfix">
                                                        <div className="thread-image">
                                                            <img src="/images/avatar-2.jpg" alt />
                                                        </div>
                                                        <div className="thread-content">
                                                            <span className="author">Nicole Bell</span>
                                                            <span className="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
                                                            <span className="time"> Just Now</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;" className="unread">
                                                    <div className="clearfix">
                                                        <div className="thread-image">
                                                            <img src="/images/avatar-3.jpg" alt />
                                                        </div>
                                                        <div className="thread-content">
                                                            <span className="author">Steven Thompson</span>
                                                            <span className="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
                                                            <span className="time">8 hrs</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <div className="clearfix">
                                                        <div className="thread-image">
                                                            <img src="/images/avatar-5.jpg" alt />
                                                        </div>
                                                        <div className="thread-content">
                                                            <span className="author">Kenneth Ross</span>
                                                            <span className="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
                                                            <span className="time">14 hrs</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="view-all">
                                    <Link to="/">
                                        See All
                                    </Link>
                                </li>
                            </ul>
                        </li> */}
                        {/* end: MESSAGES DROPDOWN */}
                        {/* start: ACTIVITIES DROPDOWN */}
                        {/* <li className="dropdown">
                            <Link to="/" className="dropdown-toggle" data-toggle="dropdown">
                                <i className="ti-check-box" /> <span>ACTIVITIES</span>
                            </Link>
                            <ul className="dropdown-menu dropdown-light dropdown-messages dropdown-large">
                                <li>
                                    <span className="dropdown-header"> You have new notifications</span>
                                </li>
                                <li>
                                    <div className="drop-down-wrapper ps-container">
                                        <div className="list-group no-margin">
                                            <Link to='/' className="media list-group-item">
                                                <img className="img-circle" alt="..." src="/images/avatar-1.jpg" />
                                                <span className="media-body block no-margin"> Use awesome animate.css <small className="block text-grey">10 minutes ago</small> </span>
                                            </Link>
                                            <Link to='/' className="media list-group-item">
                                                <span className="media-body block no-margin"> 1.0 initial released <small className="block text-grey">1 hour ago</small> </span>
                                            </Link>
                                        </div>
                                    </div>
                                </li>
                                <li className="view-all">
                                    <Link to="/">
                                        See All
                                    </Link>
                                </li>
                            </ul>
                        </li> */}
                    {/* end: ACTIVITIES DROPDOWN */}
                        <li className="dropdown">
                            <a href className="dropdown-toggle" data-toggle="dropdown">
                                <i className="ti-plus" /> Добавить
                            </a>
                            <ul role="menu" className="dropdown-menu dropdown-light fadeInUpShort">
                                {(()=>{
                                    let Menu = [];
                                    if (isCan('events','create', this.props.user)) {
                                        Menu.push(<li key='add_events'>
                                    <a className="menu-toggler" onClick={()=> this.props.actions.addEvent()}>
                                        <i className="ti-plus" /> Add new Event
                                    </a>
                                </li>)
                                    }
                                    if (isCan('person','create', this.props.user)) {
                                        Menu.push(<li key='add_person'>
                                    <a className="menu-toggler" onClick={()=> this.props.actions.AddPerson()}>
                                        <i className="ti-plus" /> Добавить данные о человеке
                                    </a>
                                </li>)
                                    }
                                    if (isCan('documents','create', this.props.user)) {
                                        Menu.push(<li key='add_document'>
                                            <Link to="/document" className="menu-toggler">
                                                <i className="ti-plus" /> Add new Document
                                            </Link>
                                        </li>)
                                    }
                                    if (isCan('admin','usercreate', this.props.user)) {
                                        Menu.push(<li key='add_user'>
                                            <Link to="nolang" className="menu-toggler">
                                                <i className="ti-plus" /> Add new User
                                            </Link>
                                        </li>)
                                    }
                                    return Menu;
                                })()}
                                
                                
                                
                                
                            </ul>
                        </li>
                        {/* start: LANGUAGE SWITCHER */}
                        {/*<li className="dropdown">
                            <a href className="dropdown-toggle" data-toggle="dropdown">
                                <i className="ti-world" /> English
                            </a>
                            <ul role="menu" className="dropdown-menu dropdown-light fadeInUpShort">
                                <li>
                                    <Link to="nolang" className="menu-toggler">
                                        English
                                    </Link>
                                </li>
                                <li>
                                    <Link to="nolang" className="menu-toggler">
                                        Russian
                                    </Link>
                                </li>
                            </ul>
                        </li>*/}
                        {/* start: LANGUAGE SWITCHER */}
                        {/* start: USER OPTIONS DROPDOWN */}
                        <li className="dropdown current-user">
                            <a href className="dropdown-toggle" data-toggle="dropdown">
                                <img src="/images/User_Avatar-512.png" alt="Peter" /> <span className="username">{this.props.user.name} <i className="ti-angle-down" /></span>
                            </a>
                            <ul className="dropdown-menu dropdown-dark">
                                {/*<li>
                                    <Link to="/user/profile">
                                        My Profile
                                    </Link>
                                </li>*/}
                                <li>
                                    <a href="javascript:void(0);" onClick={this.logOutClick}>
                                        Выход
                                    </a>
                                </li>
                            </ul>
                        </li>
                        {/* end: USER OPTIONS DROPDOWN */}
                    </ul>
                    {/* start: MENU TOGGLER FOR MOBILE DEVICES */}
                    <div className="close-handle visible-xs-block menu-toggler dfg" data-toggle="collapse" href=".navbar-collapse">
                        <div className="arrow-left" />
                        <div className="arrow-right" />
                    </div>
                    {/* end: MENU TOGGLER FOR MOBILE DEVICES */}
                </div>
                {/*<a className="dropdown-off-sidebar sidebar-mobile-toggler hidden-md hidden-lg" data-toggle-class="app-offsidebar-open" data-toggle-target="#app" data-toggle-click-outside="#off-sidebar">
                    &nbsp;
                </a>
                <a className="dropdown-off-sidebar hidden-sm hidden-xs" data-toggle-class="app-offsidebar-open" data-toggle-target="#app" data-toggle-click-outside="#off-sidebar">
                    &nbsp;
                </a>*/}
                {/* end: NAVBAR COLLAPSE */}
            </header>
        );
    }
}

/*Header.propTypes = {
    name: PropTypes.string.isRequired
};*/
