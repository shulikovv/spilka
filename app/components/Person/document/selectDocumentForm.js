import React, {PropTypes, Component} from 'react';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete,DatePicker, Table } from 'antd';
const MonthPicker = DatePicker.MonthPicker;
const RangePicker = DatePicker.RangePicker;
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
import is from 'is_js';

import axios from 'axios';
//import MockAdapter from 'axios-mock-adapter';
import qs from 'qs';

//import Form from "react-jsonschema-form";

//import {widgets} from "../../../elements/form/widgets";

class searchDocForm extends Component {
  constructor (props) {
    super(props);
    this.state = {
      formLayout: 'horizontal',
      types: [],
    }
    this.handleSubmit = (e) => {
      console.log('submited');
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, formValues) => {
        const datecreated = formValues['datecreated'];
        const values = {
          ...formValues
        }
        if (is.array(datecreated)) {
          values.datecreated = [datecreated[0].format('YYYY-MM-DD'), datecreated[1].format('YYYY-MM-DD')]
        }
        this.props.handleSubmit(values);
      })
    }
  }

  componentWillMount() {
    axios.get('/api/thesaurus/16', {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token')
        }
    })
    .then((resp) => {
        console.log('resp', resp);
        if (resp.status==200) {
          this.setState({types:resp.data})
        }
    })
  }

  render() {
    const { formLayout } = this.state;
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched  } = this.props.form;
    const formItemLayout = formLayout === 'horizontal' ? {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    }:null;
    const tailFormItemLayout = formLayout === 'horizontal' ? {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    }: null;

    const Types = this.state.types.map((item)=>{
      return <Option value={item.vocab_number} key={item.vocab_number}>{item.english}</Option>
   })

    return (<div>
      <Form onSubmit={this.handleSubmit} layout={formLayout}>
        <FormItem
          {...formItemLayout}
          label="Title"
          hasFeedback
        >
          {getFieldDecorator('title', {
            rules: [],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Date created"
          hasFeedback
        >
          {getFieldDecorator('datecreated', {
            rules: [],
          })(
            <RangePicker
                getPopupContainer={()=>document.getElementById('select-ph')}
                popupStyle={{zIndex:10000000}}
             />
          )}
        </FormItem>
        <FormItem
              {...formItemLayout}
              label={(
                <span>
                  Тип&nbsp;
                  <Tooltip title="ТИп документа...">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              )}
              hasFeedback
            >
              {getFieldDecorator('type', {
                rules: [{ required: false, message: 'Пожалуйста укажите тип документа!', whitespace: true }],
              })(
                <Select
                  //defaultValue="lucy"
                  //onChange={handleChange}
                  getPopupContainer={()=>document.getElementById('select-ph')}
                  >
                  {Types}
                </Select>
              )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Format"
          hasFeedback
        >
          {getFieldDecorator('format', {
            rules: [],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" icon="search">Найти</Button>
        </FormItem>
      </Form>
    </div>)
  }
}

const SearchDocForm = Form.create()(searchDocForm);


export default class selectDocumentForm extends Component {
  constructor (props) {
    super(props);
    this.state = {
      confirmDirty: false,
      autoCompleteResult: [],
      formLayout: 'vertical',
      types: [],
      dataSource: [],
    };

    this.handleSubmit = (values) => {
      let params = {};
      for (let key in values) {
        if (is.existy(values[key])) {
          params[key]=values[key]
        }
      }
      axios.get('/api/document/find-documents', {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token')
        },
        params: params,
        paramsSerializer: function(params) {
          return qs.stringify(params, {arrayFormat: 'brackets'})
        },
    })
    .then((resp) => {
        console.log('resp', resp);
        if (resp.status==200) {
          this.setState({dataSource:resp.data})
          //console.log(resp.data);
        }
    })
      console.log(values);
    }

  }
  

  
  render() {

    const columns = [
      {
          title: 'Внутр №',
          dataIndex: 'doc_number',
          render: (text, record) => <a onClick={() => console.log(record.doc_number)}>{text}</a>
      },
      {
          title: 'Название',
          dataIndex: 'title',
          render: (text, record) => <a onClick={() => console.log(record.eventRecordNumber)}>{text}</a>
      },
      {
          title: 'Дата создания',
          dataIndex: 'date_created'
      },
      {
          title: 'Дата подтверждения',
          dataIndex: 'date_submited'
      },
      {
          title: 'Тип',
          dataIndex: 'type'
      },
      {
          title: 'Формат',
          dataIndex: 'format'
      },
    ];

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
          console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
      },
      getCheckboxProps: record => ({
          disabled: record.name === 'Disabled User',    // Column configuration not to be checked
      }),
    };

    return (<div>
      <SearchDocForm handleSubmit={this.handleSubmit}></SearchDocForm>
      <Table 
        rowKey='doc_id' 
        dataSource={this.state.dataSource} 
        columns={columns} 
        rowSelection={rowSelection} 
      />
      </div>)
  }
}