import React, {PropTypes, Component} from 'react';
import Form from "react-jsonschema-form";
import { Alert } from 'antd';
import {widgets} from "../../elements/form/widgets";

export default class personView extends Component {
    constructor(props) {
        super(props);
        this.state = {uiSchema:props.form.uiSchema};
        this.onSubmit = ({formData}) => {this.props.actions.personFormSubmit(formData)};
    }

    componentWillMount() {
        if (this.props.isEdit) {
            this.setState({uiSchema:{...this.state.uiSchema,"ui:readonly": true}});
        } 
    }

    componentWillUpdate(nextProps, nextState) {
        //this.state = {...this.state, uiSchema: {...this.state.uiSchema, ...nextProps.form.uiSchema}};
        console.log('fgfgfgfgfgfgfgfg',nextProps.errors);
        this.state = {...this.state, uiSchema: Object.assign(this.state.uiSchema, nextProps.form.uiSchema)};
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('........',this.props.errors);
    }
    

    render() {
        let submitButtons;
        if (this.props.isEdit) {
            submitButtons = <div></div>
        } else {
            submitButtons = <div>
            <button type="submit" className='btn btn-o btn-primary'>Сохранить</button>
          </div>
        }

        let Errors = [];
        for (let val in this.props.errors) {
            this.props.errors[val].forEach((err, idx)=>{
                Errors.push(<Alert 
                message={err}
                key={val+'_'+idx}
                style={{marginTop:5}}
                type="error"
                closable
                />)
            })
        }
        //console.log('Errors',out);

        return (
            <div>
                {Errors}
            <Form schema={this.props.form.schema}
                uiSchema={this.state.uiSchema}
                onChange={()=> console.log("changed")}
                onSubmit={this.onSubmit}
                onError={()=> console.log("errors")}
                liveValidate={true}
                showErrorList={true}
                widgets={widgets}
                formData={this.props.form.data}
            >
            {submitButtons}
            </Form>
            </div>
        );
    }
}