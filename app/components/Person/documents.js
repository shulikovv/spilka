import React, {PropTypes, Component} from 'react'
import { Table, Icon, Button, Popconfirm, Modal } from 'antd';
import {isCan} from '../../utils/index';

import BiographicForm from './biographic/biographicForm';
import SelectDocumentForm from './document/selectDocumentForm';

export default class biographic extends Component {
    constructor(props){
        console.log('biogr',props);
        super(props);
        this.state = {
            modal1SelectVisible : false,
            modal1AddVisible : false
        };
        this.dataSource = [];
        
        //props.actions.loadBio(props.form.data.person_id);
    }

    componentWillMount() {
        this.props.actions.getDocumentsList(this.props.form.data.person_id);
        //console.log('3333333333---',this.props.personView);//this.props.personView.bioforms.tableData
    }

    componentDidUpdate(prevProps, prevState) {
        //console.log('==========',this.props.personView.bioform)
       
    }
    

    render() {
        //const {name} = this.props
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        let Buttons = [];
        if (isCan('person','update',this.props.user)) {
            Buttons.push( <Button
                key='update'
                type="primary"
                icon="plus"
                style={{marginRight:'5px'}}
                onClick={()=> this.props.actions.openSelDocModal(this.props.form.data.person_id)}
            >
                Добавить документ из списка
            </Button>);
            Buttons.push( <Button
                key='create'
                type="primary"
                icon="plus"
                onClick={()=> this.props.actions.openAddDocModal(this.props.form.data.person_id)}
            >
                Добавить новый документ
            </Button>);
        }
        this.columns = [
           ...this.props.docs.table.columns,
            {
                title: 'Действия',
                key: 'actions',
                render: (text, record) => (
                    <span>
                  <a onClick={() => console.log(record.eventRecordNumber)}>Смотреть</a>
                  <span className="ant-divider"/>
                  <a onClick={() => console.log(record.eventRecordNumber)}>Редактировать</a>
                  <span className="ant-divider"/>
                  <Popconfirm title="Будет удалена связь с персоной, а не документ. Так?" onConfirm={() => console.log(record.doc_id, 'delete')}>
                    <a>Удалить</a>
                  </Popconfirm>
                </span>
                )
            },
        ];
        return (
            <div className='ib user'>
                <style>{`.ant-modal-mask, .ant-modal-wrap { z-index: 1000000 !important;}`}</style>
                <div className="row">
                    <div className="col-md-12">
                        <div style={{ marginBottom: 16 }}>
                           {Buttons}

                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Table rowKey='doc_id' rowSelection={rowSelection} dataSource={this.props.docs.table.data} columns={this.columns} />
                    </div>
                </div>
                <Modal
                    title="Create new Document for User"
                    visible={this.props.personView.documents.modaladdvisible}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.props.actions.closeAddDocModal()}
                    >
                    <BiographicForm form={this.props.bioform} actions={this.props.actions}></BiographicForm>
                </Modal>
                <Modal
                    title="Select Document for User"
                    visible={this.props.personView.documents.modalselectvisible}
                    //footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.props.actions.closeSelDocModal()}
                    width='90%'
                    >
                    <SelectDocumentForm {...this.props}></SelectDocumentForm>
                </Modal>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/