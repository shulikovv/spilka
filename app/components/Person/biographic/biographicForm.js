import React, {PropTypes, Component} from 'react';
import Form from "react-jsonschema-form";

import {widgets} from "../../../elements/form/widgets";

export default class biographicForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uiSchema:props.form.uiSchema,
            type: 'add'
        };
        this.onSubmit = ({formData}) => {
            console.log(formData, this.props);
            if (this.state.type == 'add') {
                console.log('addPerson', this.state.type);
                this.props.actions.addBioFormSubmit(formData,formData.person);
            }
            if (this.state.type == 'edit') {
                console.log('editPerson', this.state.type);
                this.props.actions.editBioFormSubmit(formData,formData.person);
            }

        };
    }

    componentWillReceiveProps(newProps) {    
        console.log('Component WILL RECIEVE PROPS!')
     }

     shouldComponentUpdate(newProps, newState) {
        console.log('Component SHOULD UPDATE!')
        return (this.state.type != newProps.form.type)
     }

    componentWillMount() {
        console.log('Component WILL MOUNT!');
        this.setState({type: this.props.form.type});
        if (this.props.form.type=='view') {
            //this.setState({uiSchema:{...this.state.uiSchema,"ui:readonly": true}});
        } 
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('Component WILL UPDATE!')
        this.state = {...this.state, uiSchema: Object.assign(this.state.uiSchema, nextProps.form.uiSchema)};
        if (nextProps.form.type=='view') {
            this.setState({uiSchema:{...this.state.uiSchema,"ui:readonly": true}, type: 'view'});
        } else {
            this.setState({uiSchema:{...this.state.uiSchema,"ui:readonly": false}, type: nextProps.form.type})
        }
    }

    

    render() {
        let submitButtons;
        console.log('isEdit', this.props.isEdit,this.props.form.data);
        if (this.props.form.type=='view') {
            submitButtons = <div></div>
        } else {
            submitButtons = null
        }
        return (
            <Form schema={this.props.form.schema}
                uiSchema={this.state.uiSchema}
                onChange={()=> console.log("changed")}
                onSubmit={this.onSubmit}
                onError={()=> console.log("errors", this.props.form.data)}
                //liveValidate={true}
                showErrorList={true}
                widgets={widgets}
                formData={this.props.form.data}
            >
            {submitButtons}
            </Form>
        );
    }
}