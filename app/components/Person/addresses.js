import React, {PropTypes, Component} from 'react'
import { Table, Icon, Button, Popconfirm, Modal } from 'antd';
import {isCan} from '../../utils/index';

import AddressForm from './address/addressForm';
export default class addresses extends Component {

    componentWillMount() {
        console.log('---addresses will mount');
        console.log(this.props.form.data);
        this.props.actions.loadAddresses( this.props.form.data.person_id);
    }


    constructor(props){
        super(props);
        this.state = {
        };
        this.dataSource = [];

        this.aForm = {
            schema: {},
            uiSchema: {},
            data: {}
        }
    
        this.onSubmitForm = ({formData}) => {
            this.props.actions.addAddressFormSubmit(formData, this.props.form.data.person_id)
        }
    
    
    }


    render() {
        //const {name} = this.props
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        let Buttons = [];
        if (isCan('person','update',this.props.user)) {
            Buttons.push(
                <Button
                    key='update'
                    type="primary"
                    icon="plus"
                    onClick={()=> this.props.actions.openAddAddressModal(this.props.form.data.person_id)}
                    >
                        Добавить адрес
                </Button>
            )
        }
        console.log('%%%%%%%%%%%%--',this.props.user,isCan('person','delete', this.props.user));
        const columns = [
            {
              title: 'Тип адреса',
              dataIndex: 'address_type_text'
            },
            {
              title: 'Адрес',
              dataIndex: 'address'
            },
            {
              title: 'Страна',
              dataIndex: 'country_text'
            },
            {
              title: 'Тел.',
              dataIndex: 'phone'
            },
            {
              title: 'Моб.',
              dataIndex: 'cellular'
            },
            {
              title: 'Факс',
              dataIndex: 'fax'
            },
            {
              title: 'Email',
              dataIndex: 'email'
            },
            {
              title: 'Website',
              dataIndex: 'website'
            },
            {
              title: 'с (дата)',
              dataIndex: 'start_date'
            },
            {
              title: 'по (дата)',
              dataIndex: 'end_date'
            },
            {
                title: 'Действия',
                key: 'actions',
                render: (text, record) => {
                    let Actions = [];
                    Actions.push(<span key='view'>
                        <a onClick={()=> this.props.actions.viewPersonAddress(record)}>Смотреть</a>
                        <span className="ant-divider" />
                    </span>)
                    if (isCan('person','update', this.props.user)) {
                        Actions.push(<span key='update'>
                            <a onClick={()=> this.props.actions.editPersonAddress(record)}>Редактировать</a>
                            <span className="ant-divider" />
                        </span>)
                    }
                    if (isCan('person','delete', this.props.user)) {
                        Actions.push(<Popconfirm key='delete' title="Точно удалить?" onConfirm={() => this.props.actions.deleteAddressRecord(record, this.props.form.data.person_id)}>
                        <a>Удалить</a>
                      </Popconfirm>);
                    }
                    console.log('Actions--',Actions);
                    return (
                        <span>
                          {Actions} 
                        </span>
                    )
                }
            },
        ];
        return (
            <div className='ib user'>
            <style>{`.ant-modal-mask, .ant-modal-wrap { z-index: 1000000 !important;}`}</style>
                <div className="row">
                    <div className="col-md-12">
                        <div style={{ marginBottom: 16 }}>
                            {Buttons}

                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Table rowKey='address_id' rowSelection={rowSelection} dataSource={this.props.personView.addresses.tableData} columns={columns} />
                    </div>
                </div>
                <Modal
                    title="Create new Address for User"
                    visible={this.props.personView.addresses.modalvisible}
                    footer={null}
                    onCancel={()=> this.props.actions.closeAddAddressModal()}
                    >
                    <AddressForm form={this.props.aform}  onSubmit = {this.onSubmitForm}  actions={this.props.actions}></AddressForm>
                </Modal>
                <Modal
                    title="View user Address"
                    visible={this.props.personView.addresses.modalviewvisible}
                    footer={null}
                    onCancel={()=> this.props.actions.closeViewAddressModal()}
                    >
                    <AddressForm form={this.props.aform}  onSubmit = {this.onSubmitForm} isEdit='false'  actions={this.props.actions}></AddressForm>
                </Modal>
                <Modal
                    title="Edit Address for User"
                    visible={this.props.personView.addresses.modaleditvisible}
                    footer={null}
                    onCancel={()=> this.props.actions.closeEditAddressModal()}
                    >
                    <AddressForm form={this.props.aform}  onSubmit = {this.onSubmitForm}  actions={this.props.actions}></AddressForm>
                </Modal>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/