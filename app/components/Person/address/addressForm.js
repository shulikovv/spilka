import React, {PropTypes, Component} from 'react';
import Form from "react-jsonschema-form";

import {widgets} from "../../../elements/form/widgets";

export default class addressForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uiSchema:props.form.uiSchema,
            type: 'add'
        };
        this.onSubmit = ({formData}) => {
            console.log(formData, this.props);
            if (this.state.type == 'add') {
                this.props.actions.addAddressFormSubmit(formData,formData.person_id);
            }
            if (this.state.type == 'edit') {
                this.props.actions.editAddressFormSubmit(formData,formData.person_id);
            }

        };
    }

    shouldComponentUpdate(newProps, newState) {
        return (this.state.type != newProps.form.type)
     }

    componentWillMount() {
        console.log('Component WILL MOUNT!', this.state);
        if (this.props.form.type=='view') {
            this.setState({uiSchema:{...this.state.uiSchema,"ui:readonly": true}, type: 'view'});
        } else {
            this.setState({uiSchema:{...this.state.uiSchema,"ui:readonly": false}, type: this.props.form.type})
        }
    }

    componentWillUpdate(nextProps, nextState) {
        this.state = {...this.state, uiSchema: Object.assign(this.state.uiSchema, nextProps.form.uiSchema)};
        console.log('=======>>',nextProps);
        if (nextProps.form.type=='view') {
            this.setState({uiSchema:{...this.state.uiSchema,"ui:readonly": true}, type: 'view'});
        } else {
            this.setState({uiSchema:{...this.state.uiSchema,"ui:readonly": false}, type: nextProps.form.type})
        }
    }

    

    render() {
        let submitButtons;
        console.log('isEdit', this.props.isEdit, this.props, this.state);
        if (this.props.form.type=='view') {
            submitButtons = <div></div>
        } else {
            submitButtons = null
        }
        return (
            <Form schema={this.props.form.schema}
                uiSchema={this.state.uiSchema}
                onChange={()=> console.log("changed")}
                onSubmit={this.onSubmit}
                onError={()=> console.log("errors")}
                //liveValidate={true}
                showErrorList={true}
                widgets={widgets}
                formData={this.props.form.data}
            >
            {submitButtons}
            </Form>
        );
    }
}