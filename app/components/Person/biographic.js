import React, {PropTypes, Component} from 'react'
import { Table, Icon, Button, Popconfirm, Modal } from 'antd';
import {isCan} from '../../utils/index';

import BiographicForm from './biographic/biographicForm';
export default class biographic extends Component {
    constructor(props){
        console.log('biogr',props);
        super(props);
        this.state = {
            modal1visible : false
        };
        this.dataSource = [];
        
    props.actions.loadBio(props.form.data.person_id);
    }

    componentWillMount() {
        //this.props.actions.loadBio(this.props.form.data.person_id);
        console.log('3333333333---',this.props.personView);//this.props.personView.bioforms.tableData
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('==========',this.props.personView.bioform)
       
    }
    

    render() {
        //const {name} = this.props
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User',    // Column configuration not to be checked
            }),
        };
        let Buttons = [];
        if (isCan('person','update',this.props.user)) {
            Buttons.push( <Button
                key='update'
                type="primary"
                icon="plus"
                onClick={()=> this.props.actions.openAddBioModal(this.props.form.data.person_id)}
            >
                Добавить данные биографии
            </Button>)
        }
        this.columns = [
            {
              title: 'Название',
              dataIndex: 'title'
            },
            {
              title: 'Описание',
              dataIndex: 'biographic_details'
            },
            {
              title: 'Тип отношений',
              dataIndex: 'relationship_type'
            },
            {
              title: 'Связь (человек)',
              dataIndex: 'related_person'
            },
            {
              title: 'с (дата)',
              dataIndex: 'initial_date'
            },
            {
              title: 'до (дата)',
              dataIndex: 'final_date'
            },
            {
                title: 'Действия',
                key: 'actions',
                render: (text, record) => {
                    let Actions = [];
                    Actions.push(<span key='view'>
                        <a onClick={()=> this.props.actions.viewPersonBiographic(record)}>Смотреть</a>
                        <span className="ant-divider" />
                    </span>)
                    if (isCan('person','update', this.props.user)) {
                        Actions.push(<span key='update'>
                            <a onClick={()=> this.props.actions.editPersonBiographic(record)}>Редактировать</a>
                            <span className="ant-divider" />
                        </span>)
                    }
                    if (isCan('person','delete', this.props.user)) {
                        Actions.push(<Popconfirm key='delete' title="Точно удалить?" onConfirm={() => this.props.actions.deleteBioRecord(record, this.props.form.data.person_id)}>
                        <a>Удалить</a>
                      </Popconfirm>);
                    }
                    return (
                        <span>
                          {Actions} 
                        </span>
                    )
                }
            },
        ];
        return (
            <div className='ib user'>
                <style>{`.ant-modal-mask, .ant-modal-wrap { z-index: 1000000 !important;}`}</style>
                <div className="row">
                    <div className="col-md-12">
                        <div style={{ marginBottom: 16 }}>
                           {Buttons}

                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Table rowSelection={rowSelection} dataSource={this.props.personView.bioform.tableData} columns={this.columns} />
                    </div>
                </div>
                <Modal
                    title="Create new Biographic for User"
                    visible={this.props.personView.bioform.modalvisible}
                    footer={null}
                    onOk={()=> console.log('OK')}
                    onCancel={()=> this.props.actions.closeAddBioModal()}
                    >
                    <BiographicForm form={this.props.bioform} actions={this.props.actions}></BiographicForm>
                </Modal>
            </div>
        )
    }
}

/*
User.propTypes = {
    name: PropTypes.string.isRequired
}*/