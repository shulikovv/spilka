import React, {PropTypes, Component} from 'react';
import { Table, Icon, Button, Popconfirm, Tree, Modal, Alert, Tabs, message, notification } from 'antd';

import Form from "react-jsonschema-form";

const TabPane = Tabs.TabPane;

import {widgets} from "../../elements/form/widgets";

import Addresses from './addresses';
import Biographic from './biographic';
import Documents from './documents';
import Roles from './roles';
import Audit from './audit';
import PersonForm from './PersonForm';
import {isCan} from '../../utils/index';

export default class personView extends Component {
    
    render() {
        let Buttons = [];
        if (isCan('person','update', this.props.user)) {
            Buttons.push(
                <Button key='update' type="primary" style={{marginRight: 8}} onClick={() => this.props.actions.editPerson(this.props.form.data.person_id)}>Редактировать</Button>
            )
        }
        if (isCan('person', 'delete', this.props.user)) {
            Buttons.push(
                <Popconfirm key='delete' title="Точно удалить?" onConfirm={() => console.log('delete')}>
                    <Button type="primary">Удалить</Button>
                </Popconfirm>
            )
        }
        return (<div>
                <Tabs defaultActiveKey="1" onChange={()=> console.log('dfg')}>
                    <TabPane tab="Информация" key="1">
                        {Buttons}
                        {/*<Button type="primary" style={{marginRight: 8}}>Print this Person</Button>*/}
                        
                        <PersonForm isEdit={true} {...this.props}></PersonForm>
                    </TabPane>
                    <TabPane tab="Адреса" key="2">
                        <Addresses {...this.props}></Addresses>
                    </TabPane>
                    <TabPane tab="Биография" key="3">
                        <Biographic {...this.props}></Biographic>
                    </TabPane>
                    <TabPane tab="Документы" key="4">
                        <Documents {...this.props}></Documents>
                    </TabPane>
                    <TabPane tab="Роли" key="5"><Roles {...this.props}></Roles></TabPane>
                    <TabPane tab="Аудит" key="6"><Audit></Audit></TabPane>
                </Tabs>
            </div>)
    }
}