import React, {PropTypes, Component} from 'react'
import Form from "react-jsonschema-form";
import { Spin , Button, Popconfirm} from 'antd';



export default class civilVehicleForm extends Component {

    constructor(props){
        super(props);
        this.logg = (mess) => console.log(mess);
        
            this.onSubmit = (formData) => console.log(formData);
            
            this.schema = {
                type: "object",
                properties: {
                    "vechicle_id": {
                        "type": "string",
                    },
                    "vechicle_role": {
                        "title": "Role",
                        "type": "string",
                    },
                    "vechicle_status": {
                        "title": "Status",
                        "type": "string",
                    },
                    "vechicle_color": {
                        "title": "Color",
                        "type": "string",
                    },
                    "vechicle_type": {
                        "title": "Type",
                        "type": "string",
                    },
                    "vechicle_kind": {
                        "title": "Kind",
                        "type": "string",
                    },
                    "vechicle_doors": {
                        "title": "Number of doors",
                        "type": "boolean",
                    },
                    "vechicle_own": {
                        "title": "Own",
                        "type": "string",
                    },
                    "vechicle_description": {
                        "title": "Description",
                        "type": "string",
                        format: "textarea",
                    },
                    "vechicle_features": {
                        "title": "Features",
                        "type": "string",
                        format: "textarea"
                    },
                }
            };
        
            this.uiSchema = {
                vechicle_id: {"ui:widget": "hidden"}
            };
        
            this.formDat = {};
    }



    render() {
        //const {name} = this.props
        return (
            <div>
                <Spin tip="Loading..." size="large" spinning={this.props.loading}>
                
                <Form schema={this.schema}
                    uiSchema={this.uiSchema}
                    onChange={this.logg("changed")}
                    onSubmit={this.onSubmit}
                    onError={this.logg("errors")}
                    liveValidate={true}
                    showErrorList={true}
                    formData={this.formData}
                />
                </Spin>
            </div>
        )
    }

}