<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Thesauri]].
 *
 * @see Thesauri
 */
class ThesauriQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Thesauri[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Thesauri|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
