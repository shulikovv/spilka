<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mt_vocab".
 *
 * @property integer $mt_index_no
 * @property string $vocab_number
 * @property string $huri_code
 * @property string $english
 * @property string $french
 * @property string $otherlanguages
 * @property integer $list_code
 * @property string $visible
 * @property integer $term_order
 * @property string $parent_vocab_number
 * @property integer $term_level
 */
class Thesaurus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mt_vocab';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mt_index_no', 'vocab_number', 'list_code', 'parent_vocab_number', 'term_level'], 'required'],
            [['mt_index_no', 'list_code', 'term_order', 'term_level'], 'integer'],
            [['english', 'french', 'otherlanguages'], 'string'],
            [['vocab_number', 'huri_code', 'parent_vocab_number'], 'string', 'max' => 14],
            [['visible'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mt_index_no' => Yii::t('app', 'Mt Index No'),
            'vocab_number' => Yii::t('app', 'Vocab Number'),
            'huri_code' => Yii::t('app', 'Huri Code'),
            'english' => Yii::t('app', 'English'),
            'french' => Yii::t('app', 'French'),
            'otherlanguages' => Yii::t('app', 'Otherlanguages'),
            'list_code' => Yii::t('app', 'List Code'),
            'visible' => Yii::t('app', 'Visible'),
            'term_order' => Yii::t('app', 'Term Order'),
            'parent_vocab_number' => Yii::t('app', 'Parent Vocab Number'),
            'term_level' => Yii::t('app', 'Term Level'),
        ];
    }

    /**
     * @inheritdoc
     * @return ThesaurusQuery the active query used by this AR class.
     */
    public static function find2()
    {
        return new ThesaurusQuery(get_called_class());
    }


    /**
     * @inheritdoc
     * @return ThesaurusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ThesaurusQuery(get_called_class());
    }
}
