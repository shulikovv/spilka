<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Thesaurus]].
 *
 * @see Thesaurus
 */
class ThesaurusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Thesaurus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Thesaurus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
