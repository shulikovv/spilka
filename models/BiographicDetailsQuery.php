<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[biographic_details]].
 *
 * @see biographic_details
 */
class BiographicDetailsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return biographic_details[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return biographic_details|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
