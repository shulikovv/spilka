<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "biographic_details".
 *
 * @property integer $biographic_details_id
 * @property string $biographic_details_record_number
 * @property string $title
 * @property string $person
 * @property string $related_person
 * @property string $confidentiality
 * @property string $type_of_relationship
 * @property string $initial_date
 * @property string $initial_date_type
 * @property string $final_date
 * @property string $final_date_type
 * @property string $education_and_training
 * @property string $employment
 * @property string $affiliation
 * @property string $position_in_organisation
 * @property string $rank
 * @property string $remarks
 * @property string $comments
 * @property integer $is_deleted
 * @property integer $is_confirmed
 */
class BiographicDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'biographic_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person'], 'required'],
            [['initial_date', 'final_date'], 'safe'],
            [['employment', 'rank', 'remarks', 'comments'], 'string'],
            [['is_deleted', 'is_confirmed'], 'integer'],
            [['biographic_details_record_number', 'person', 'related_person'], 'string', 'max' => 45],
            [['title'], 'string', 'max' => 2048],
            [['confidentiality'], 'string', 'max' => 1],
            [['type_of_relationship', 'initial_date_type', 'final_date_type', 'education_and_training'], 'string', 'max' => 14],
            [['affiliation', 'position_in_organisation'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'biographic_details_id' => Yii::t('app', 'Biographic Details ID'),
            'biographic_details_record_number' => Yii::t('app', 'Biographic Details Record Number'),
            'title' => Yii::t('app', 'Title'),
            'person' => Yii::t('app', 'Person'),
            'related_person' => Yii::t('app', 'Related Person'),
            'confidentiality' => Yii::t('app', 'Confidentiality'),
            'type_of_relationship' => Yii::t('app', 'Type Of Relationship'),
            'initial_date' => Yii::t('app', 'Initial Date'),
            'initial_date_type' => Yii::t('app', 'Initial Date Type'),
            'final_date' => Yii::t('app', 'Final Date'),
            'final_date_type' => Yii::t('app', 'Final Date Type'),
            'education_and_training' => Yii::t('app', 'Education And Training'),
            'employment' => Yii::t('app', 'Employment'),
            'affiliation' => Yii::t('app', 'Affiliation'),
            'position_in_organisation' => Yii::t('app', 'Position In Organisation'),
            'rank' => Yii::t('app', 'Rank'),
            'remarks' => Yii::t('app', 'Remarks'),
            'comments' => Yii::t('app', 'Comments'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'is_confirmed' => Yii::t('app', 'Is Confirmed'),
        ];
    }

    /**
     * @inheritdoc
     * @return BiographicDetailsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BiographicDetailsQuery(get_called_class());
    }

    public function load($params = null)
    {
        if ($params && is_array($params)) {
            foreach ($params as $key=>$val) {
                if ($key == 'confidentiality') {
                    $this->$key = $this->$key?1:0;
                } else {
                    $this->$key = $val;
                }
            }
        }
    }
}
