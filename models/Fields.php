<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fields".
 *
 * @property integer $field_id
 * @property string $entity
 * @property string $field_number
 * @property string $field_name
 * @property string $field_label
 * @property string $field_type
 * @property string $data_type
 * @property string $list_code
 * @property integer $is_multiple
 */
class Fields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity', 'field_number', 'field_name', 'field_label', 'field_type', 'data_type', 'list_code', 'is_multiple'], 'required'],
            [['is_multiple'], 'integer'],
            [['entity', 'field_name', 'field_label', 'field_type', 'data_type', 'list_code'], 'string', 'max' => 100],
            [['field_number'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'field_id' => Yii::t('app', 'Field ID'),
            'entity' => Yii::t('app', 'Entity'),
            'field_number' => Yii::t('app', 'Field Number'),
            'field_name' => Yii::t('app', 'Field Name'),
            'field_label' => Yii::t('app', 'Field Label'),
            'field_type' => Yii::t('app', 'Field Type'),
            'data_type' => Yii::t('app', 'Data Type'),
            'list_code' => Yii::t('app', 'List Code'),
            'is_multiple' => Yii::t('app', 'Is Multiple'),
        ];
    }
}
