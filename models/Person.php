<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person".
 *
 * @property integer $person_id
 * @property string $counting_unit
 * @property string $person_name
 * @property string $other_names
 * @property integer $org_id
 * @property integer $confidentiality
 * @property string $date_of_birth
 * @property string $place_of_birth
 * @property integer $sex
 * @property integer $sexual_orientation
 * @property string $identification_documents
 * @property string $civil_status
 * @property string $depedants
 * @property string $formal_education
 * @property string $other_training
 * @property string $helth
 * @property integer $deceased
 * @property string $medical_records
 * @property string $date_deceased
 * @property string $citizenship
 * @property string $remarks
 * @property string $physical_description
 */
class Person extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['counting_unit','person_name'], 'required'],
            [['org_id', 'confidentiality', 'sex', 'sexual_orientation', 'deceased'], 'integer'],
            [['identification_documents', 'depedants', 'other_training', 'helth', 'medical_records', 'remarks'], 'string'],
            [['counting_unit', 'date_of_birth', 'place_of_birth', 'civil_status', 'formal_education', 'date_deceased', 'citizenship'], 'string', 'max' => 14],
            [['person_name'], 'string', 'max' => 256],
            [['other_names'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_id' => Yii::t('app', 'Person ID'),
            'counting_unit' => Yii::t('app', 'Counting Unit'),
            'person_name' => Yii::t('app', 'Person Name'),
            'other_names' => Yii::t('app', 'Other Names'),
            'org_id' => Yii::t('app', 'Org ID'),
            'confidentiality' => Yii::t('app', 'Confidentiality'),
            'date_of_birth' => Yii::t('app', 'Date Of Birth'),
            'place_of_birth' => Yii::t('app', 'Place Of Birth'),
            'sex' => Yii::t('app', 'Sex'),
            'sexual_orientation' => Yii::t('app', 'Sexual Orientation'),
            'identification_documents' => Yii::t('app', 'Identification Documents'),
            'civil_status' => Yii::t('app', 'Civil Status'),
            'depedants' => Yii::t('app', 'Depedants'),
            'formal_education' => Yii::t('app', 'Formal Education'),
            'other_training' => Yii::t('app', 'Other Training'),
            'helth' => Yii::t('app', 'Helth'),
            'deceased' => Yii::t('app', 'Deceased'),
            'medical_records' => Yii::t('app', 'Medical Records'),
            'date_deceased' => Yii::t('app', 'Date Deceased'),
            'citizenship' => Yii::t('app', 'Citizenship'),
            'remarks' => Yii::t('app', 'Remarks'),
            'physical_description' => Yii::t('app', 'Physical Description'),
        ];
    }

    public function getCounting()
    {
        return $this->hasOne(Thesaurus::className(), ['huri_code' => 'counting_unit'])
        ->where(['mt_index_no'=>7]);
    }

    public function getSexstring()
    {
        return $this->hasOne(Thesaurus::className(), ['huri_code' => 'sex'])
        ->where(['mt_index_no'=>39]);
    }

    public function getDocument()
    {
        return $this->hasMany(Document::className(), ['doc_id' => 'person_id'])
            ->viaTable('person_to_doc', ['person_id' => 'person_id']);
    }

    /**
     * @inheritdoc
     * @return PersonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PersonQuery(get_called_class());
    }
}
