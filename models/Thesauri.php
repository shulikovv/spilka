<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mt_index".
 *
 * @property integer $no
 * @property string $term
 */
class Thesauri extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mt_index';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no'], 'required'],
            [['no'], 'integer'],
            [['term'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no' => Yii::t('app', 'No'),
            'term' => Yii::t('app', 'Term'),
        ];
    }

    /**
     * @inheritdoc
     * @return ThesauriQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ThesauriQuery(get_called_class());
    }
}
