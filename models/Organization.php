<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organization".
 *
 * @property integer $organization_id
 * @property integer $organization_parent
 * @property integer $organization_has_children
 * @property string $organization_title
 * @property string $organization_abb
 * @property string $organization_description
 * @property string $organization_features
 */
class Organization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_parent', 'organization_has_children', 'organization_title', 'organization_abb', 'organization_description', 'organization_features'], 'required'],
            [['organization_parent', 'organization_has_children'], 'integer'],
            [['organization_description', 'organization_features'], 'string'],
            [['organization_title'], 'string', 'max' => 1024],
            [['organization_abb'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'organization_id' => Yii::t('app', 'Organization ID'),
            'organization_parent' => Yii::t('app', 'Organization Parent'),
            'organization_has_children' => Yii::t('app', 'Organization Has Children'),
            'organization_title' => Yii::t('app', 'Organization Title'),
            'organization_abb' => Yii::t('app', 'Organization Abb'),
            'organization_description' => Yii::t('app', 'Organization Description'),
            'organization_features' => Yii::t('app', 'Organization Features'),
        ];
    }

    /**
     * @inheritdoc
     * @return OrganizationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrganizationQuery(get_called_class());
    }
}
