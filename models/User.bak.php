<?php

namespace app\models;

use Yii;
use Lcobucci\JWT\Signer\Hmac\Sha256;


class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];

    private static $key = 'testing';

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token , $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        $jwtToken = Yii::$app->jwt->getParser()->parse((string) $token);
        $data = Yii::$app->jwt->getValidationData();
        $signer = new Sha256();
        if ($jwtToken && $jwtToken->validate($data) && $jwtToken->verify($signer, self::$key)) {
            $accessToken = $jwtToken->getClaim('accessToken');
            foreach (self::$users as $user) {
                if ($user['accessToken'] === $accessToken) {
                    return new static($user);
                }
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function getJwtToken()
    {
        $signer = new Sha256();
        $token = Yii::$app->jwt->getBuilder()
            ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
            //->setNotBefore(time() + 60) // Configures the time before which the token cannot be accepted (nbf claim)
            ->setExpiration(time() + 3600) // Configures the expiration time of the token (exp claim)
            ->set('id', $this->id  )
            ->set('username', $this->username  )
            ->set('accessToken', $this->accessToken  )
            ->sign($signer, self::$key) // creates a signature using "testing" as key
            ->getToken(); // Retrieves the generated token
        return $token;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
