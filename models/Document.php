<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documents".
 *
 * @property integer $doc_id
 * @property string $title
 * @property string $creator
 * @property string $description
 * @property string $datecreated
 * @property string $datesubmitted
 * @property string $type
 * @property string $language
 * @property string $format
 * @property string $subject
 * @property string $url
 * @property string $file_name
 * @property string $real_name
 * @property string $ext
 *
 * @property PersonToDoc[] $personToDocs
 */
class Document extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datecreated', 'datesubmitted'], 'safe'],
            [['title', 'creator', 'description', 'url', 'real_name'], 'string', 'max' => 1024],
            [['type'], 'string', 'max' => 14],
            [['language', 'format', 'subject'], 'string', 'max' => 64],
            [['file_name'], 'string', 'max' => 256],
            [['ext'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'doc_id' => Yii::t('app', 'Doc ID'),
            'title' => Yii::t('app', 'Title'),
            'creator' => Yii::t('app', 'Creator'),
            'description' => Yii::t('app', 'Description'),
            'datecreated' => Yii::t('app', 'Datecreated'),
            'datesubmitted' => Yii::t('app', 'Datesubmitted'),
            'type' => Yii::t('app', 'Type'),
            'language' => Yii::t('app', 'Language'),
            'format' => Yii::t('app', 'Format'),
            'subject' => Yii::t('app', 'Subject'),
            'url' => Yii::t('app', 'Url'),
            'file_name' => Yii::t('app', 'File Name'),
            'real_name' => Yii::t('app', 'Real Name'),
            'ext' => Yii::t('app', 'Ext'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonToDocs()
    {
        return $this->hasMany(PersonToDoc::className(), ['doc_id' => 'doc_id']);
    }

    /**
     * @inheritdoc
     * @return DocumentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DocumentQuery(get_called_class());
    }
}
