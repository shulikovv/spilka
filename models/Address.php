<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $address_id
 * @property string $address_record_number
 * @property integer $person_id
 * @property string $address_type
 * @property string $address
 * @property string $country
 * @property string $phone
 * @property string $cellular
 * @property string $fax
 * @property string $email
 * @property string $web
 * @property string $start_date
 * @property string $end_date
 * @property integer $is_deleted
 * @property integer $is_confirmed
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id'], 'required'],
            [['person_id', 'is_deleted', 'is_confirmed'], 'integer'],
            [['address'], 'string'],
            [['start_date', 'end_date'], 'safe'],
            [['address_record_number'], 'string', 'max' => 45],
            [['address_type'], 'string', 'max' => 14],
            [['country', 'web'], 'string', 'max' => 200],
            [['phone', 'cellular', 'fax'], 'string', 'max' => 60],
            [['email'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'address_id' => Yii::t('app', 'Address ID'),
            'address_record_number' => Yii::t('app', 'Address Record Number'),
            'person_id' => Yii::t('app', 'Person ID'),
            'address_type' => Yii::t('app', 'Address Type'),
            'address' => Yii::t('app', 'Address'),
            'country' => Yii::t('app', 'Country'),
            'phone' => Yii::t('app', 'Phone'),
            'cellular' => Yii::t('app', 'Cellular'),
            'fax' => Yii::t('app', 'Fax'),
            'email' => Yii::t('app', 'Email'),
            'web' => Yii::t('app', 'Web'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'is_confirmed' => Yii::t('app', 'Is Confirmed'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressType()
    {
        return $this->hasOne(Thesaurus::className(), ['huri_code' => 'address_type'])
        ->where(['mt_index_no'=>40]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountrySt()
    {
        return $this->hasOne(Thesaurus::className(), ['huri_code' => 'country'])
        ->where(['mt_index_no'=>15]);
    }

    public function load($params = null)
    {
        if ($params && is_array($params)) {
            foreach ($params as $key=>$val) {
                $this->$key = $val;
            }
        }
    }

    /**
     * @inheritdoc
     * @return AddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AddressQuery(get_called_class());
    }
}
