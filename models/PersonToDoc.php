<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person_to_doc".
 *
 * @property integer $doc_id
 * @property integer $person_id
 * @property integer $linked_by
 * @property string $linked_date
 *
 * @property Documents $doc
 * @property Person $person
 * @property User $linkedBy
 */
class PersonToDoc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'person_to_doc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doc_id', 'person_id', 'linked_by'], 'required'],
            [['doc_id', 'person_id', 'linked_by'], 'integer'],
            [['linked_date'], 'safe'],
            [['doc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Documents::className(), 'targetAttribute' => ['doc_id' => 'doc_id']],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['person_id' => 'person_id']],
            [['linked_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['linked_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'doc_id' => 'Doc ID',
            'person_id' => 'Person ID',
            'linked_by' => 'Linked By',
            'linked_date' => 'Linked Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoc()
    {
        return $this->hasOne(Documents::className(), ['doc_id' => 'doc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['person_id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'linked_by']);
    }
}
