var path = require('path')
var webpack = require('webpack')
var NpmInstallPlugin = require('npm-install-webpack-plugin')
var autoprefixer = require('autoprefixer');
var precss = require('precss');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
//const BabiliPlugin = require('babili-webpack-plugin');

var svgoConfig = JSON.stringify({
    plugins: [
        {removeTitle: true},
        {convertColors: {shorthex: false}},
        {convertPathData: false}
    ]
});


module.exports = {
    //devtool: 'cheap-module-eval-source-map',
    //devtool: 'eval',
    entry: [
        //'webpack-hot-middleware/client',
        'babel-polyfill',
        './app/index'
    ],
    output: {
        path: path.join(__dirname, 'web/js'),
        filename: 'bundle.js',
        publicPath: '/web/js/'
    },
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        //new webpack.HotModuleReplacementPlugin(),
        new NpmInstallPlugin(),
        //new UglifyJSPlugin()
        //new BabiliPlugin(),
    ],
    module: {
        preLoaders: [
            {
                test: /\.js$/,
                loaders: ['eslint'],
                include: [
                    path.resolve(__dirname, "src"),
                ],
            }
        ],
        loaders: [
            {
                loaders: ['babel-loader'],
                include: [
                    path.resolve(__dirname, "app"),
                ],
                test: /\.js$/,
                plugins: ['transform-runtime'],
            },
            { test: /\.css$/,  loader: "style-loader!css-loader!postcss-loader" },
            {
                test: /\.less$/,
                loader: "style-loader!css-loader!less-loader"
            },
            { test: /\.gif$/, loader: "url-loader?mimetype=image/png" },
            { test: /\.woff(2)?(\?v=[0-9].[0-9].[0-9])?$/, loader: "url-loader?mimetype=application/font-woff" },
            { test: /\.(ttf|eot|svg)(\?v=[0-9].[0-9].[0-9])?$/, loader: "file-loader?name=[name].[ext]" },
        ]
    },
    postcss: function () {
        return [precss];
    }
}
