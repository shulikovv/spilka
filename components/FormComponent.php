<?php

namespace app\components;

use yii\base\Component;

use app\models\User;
use app\models\Organization;
use app\models\Thesauri;
use app\models\Thesaurus;
use app\models\Fields;

class FormComponent extends Component {

    public function mySuperMethod() {
        //ваш код
        //return ;
        echo "MyComponent here";
    }

    public function getMtAsOptions($mt) {
        $rows = Thesaurus::find()
            ->where(['mt_index_no'=>$mt])
            ->all();
        return $rows;
    }

    public function getOrgsAsOptions() {
        $select = ['organization_id', 'organization_title'];
        $org = Organization::find()
            ->select($select)
            ->where(['organization_parent'=>0])
            ->asArray()
            ->all();
        $out = $this->getOrgsTree($org, $select);
        return $out;
    }


    private function getOrgsTree($list, $select = '*')
    {
        $out = array();
        foreach ($list as $item) {
            $children = $this->getChildren($item['organization_id'], $select);
            if (is_array($children) && count($children)>0) {
                $item['children']= $this->getOrgsTree($children, $select);
            }
            $out[]=$item;
        }
        return $out;
    }

    private function getChildren($id, $select = '*')
    {
        $children = Organization::find()
            ->select($select)
            ->where(['organization_parent'=>$id])
            ->asArray()
            ->all();
        return $children;
    }


    public function getFormSchema($entity) {
        $fields = \app\models\Fields::find()
            ->where(['entity'=>$entity])
            ->orderBy('sort_order')
            ->all();
        $required = [];
        if ($entity == 'person_address') {
            $required = [
                'address_type',
                'address',
                'country',
                'start_date',

            ];
        }
        if ($entity == 'event') {
            $required = [
                'event_title',
            ];
        }
        $schema = [
            'type'=> 'object',
            'required'=> $required,
            'properties' => []
        ];
        foreach ($fields as $field) {
            switch ($field->field_type) {
                case 'number':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'number'
                    ];
                    break;
                case 'string':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'string'
                    ];
                    break;
                case 'user':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'user'
                    ];
                    break;
                case 'mt_tree':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'mt_tree',
                        'options' => $this->getMtAsOptions($field->list_code)
                    ];
                    break;
                case 'mt_tree_multi':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'mt_tree_multi',
                        'options' => $this->getMtAsOptions($field->list_code)
                    ];
                    break;
                case 'org_tree':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'org_tree',
                        'options' => $this->getOrgsAsOptions()
                    ];
                    break;
                case 'date':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'date'
                    ];
                    break;
                case 'checkbox':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'checkbox'
                    ];
                    break;
                case 'text':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'text'
                    ];
                    break;
                case 'upload':
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'upload'
                    ];
                    break;
                default:
                    $schema['properties'][$field->field_name] = [
                        'title' => $field->field_label,
                        'type' => 'string'//$field->field_type
                    ];

            }
            if ($field->is_hidden) {
                $schema['properties'][$field->field_name]['hidden']=true;
            }
        }
        return $schema;
    }

}
