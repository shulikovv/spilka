<?php

namespace app\actions;

//use Closure;
use Yii;
use yii\helpers\Inflector;


class TestAction extends \yii\base\Action
{

    public function run()
    {
        $collection = Yii::$app->mongodb->getCollection('customer');
        $collection->insert(['name' => 'John Smith', 'status' => 1]);
        return 'TestActions';
    }
}