'use strict';

var path = require('path');
var webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
var NpmInstallPlugin = require('npm-install-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
  entry: [
    'babel-polyfill', './app/index'
  ],
  output: {
    path: path.join(__dirname, 'web/js'),
    filename: 'bundle.js',
    publicPath: '/web/js/'
  },
  plugins: [
    new NpmInstallPlugin(),
    new webpack
      .optimize
      .OccurrenceOrderPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new webpack
      .optimize
      .UglifyJsPlugin({
        compress: {
          warnings: false,
          screw_ie8: true,
          conditionals: true,
          unused: true,
          comparisons: true,
          sequences: true,
          dead_code: true,
          evaluate: true,
          if_return: true,
          join_vars: true
        },
        output: {
          comments: false
        }
      }),
    new CompressionPlugin({
      asset: '[path].gz[query]', 
      algorithm: 'gzip', 
      test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/, 
      threshold: 10240, 
      minRatio: 0.8
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }, {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }, {
        test: /\.less$/,
        loader: 'style-loader!css-loader!less-loader'
      }, {
        test: /\.gif$/,
        loader: 'url-loader?mimetype=image/png'
      }, {
        test: /\.woff(2)?(\?v=[0-9].[0-9].[0-9])?$/,
        loader: 'url-loader?mimetype=application/font-woff'
      }, {
        test: /\.(ttf|eot|svg)(\?v=[0-9].[0-9].[0-9])?$/,
        loader: 'file-loader?name=[name].[ext]'
      }
    ]

  }
};
